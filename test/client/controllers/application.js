describe('ApplicationController', function () {

    var rootScope, scope;
    var ApplicationController, UserServiceMock, AUTH_EVENTS;

    beforeEach(module('statsGuy'));

    beforeEach(inject(function($rootScope, $controller, _AUTH_EVENTS_) {
        rootScope = $rootScope;
        AUTH_EVENTS = _AUTH_EVENTS_;
        scope = $rootScope.$new();

        UserServiceMock = {
            isAuthenticated: function () { return false },
            getCurrentId: function () { return 1 },
            profile: function (params, successCb) {
                successCb({
                    "profile": {
                        "user": {
                            "firstName": "Test",
                            "lastName": "Testington"
                        },
                        "identities": [{
                            "profile": {
                                "_json": {
                                    "email": "test@testington.com",
                                    "name": "Test Testington"
                                }
                            }
                        }],
                        "roles": [{
                            "name": "Support Analyst"
                        }]
                    }
                }, {});
            },
            userVoiceSSO: function (params, successCb) {
                successCb({
                    "data": {
                        "SSO": "1234"
                    }
                }, {});
            }
        };

        ApplicationController = $controller('ApplicationController', {
            $scope: scope,
            User: UserServiceMock,
        });
    }));

    it('should have a populated session if user is already authenticated', function () {
        rootScope.$broadcast(AUTH_EVENTS.loginSuccess);

        expect(scope.session.identity).not.toBe(null);
    });

    it('should not have a populated session if user is not authenticated', function () {
        expect(scope.session.identity).toBe(null);
    });

});
