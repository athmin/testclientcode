'use strict';

var chai = require('chai'),
    expect = chai.expect,
    mapping = require('../../../src/etl/mapping');

chai.use(require('chai-datetime'));

describe('etl mappers', function () {

    describe('team mapper', function () {

        it('should create a mapper for teams', function () {
            expect(mapping.create('ReportingTeam')).ok;
        });

        describe('mapping a row', function () {

            var row = {
                TEAMNAME: 'team name',
                TEAM_UUID: 'auuid',
                TEAMSEASON_UUID: 'teamseasonuuid'
            };

            beforeEach(function () {
                this.result = mapping.create('ReportingTeam').map(row);
            });

            it('should map the team name', function () {
                expect(this.result.name).to.equal('team name');
            });

            it('should map the team uuid', function () {
                expect(this.result.uuid).to.equal('auuid');
            });

            it('should map the team season uuid', function () {
                expect(this.result.teamSeasonUUID).to.equal('teamseasonuuid');
            });

        });

    });

    describe('game mapper', function () {

        it('should create a mapper for games', function () {
            expect(mapping.create('ReportingTeamGame')).ok;
        });

        describe('mapping a row', function () {

            var row = {
                GAME_UUID: 'gameuuid',
                DATE: '401954400',
                STARTTIME: '-63059025313',
                CITY: 'Calgary',
                GAMETYPE: 'Home'
            };

            beforeEach(function () {
                this.result = mapping.create('ReportingTeamGame').map(row);
            });

            it('should map the game uuid', function () {
                expect(this.result.uuid).to.equal('gameuuid');
            });

            it('should map the date', function () {
                expect(this.result.date).to.equalDate(new Date(2013, 8, 27, 20, 30));
            });

            it('should map the city', function () {
                expect(this.result.city).to.equal('Calgary');
            });

            it('should map the game type', function () {
                expect(this.result.type).to.equal('Home');
            });

        });

    });

    describe('game player mapper', function () {

        it('should create a mapper for players in a game', function () {
            expect(mapping.create('ReportingTeamPlayer')).ok;
        });

        describe('mapping a row', function () {

            var row = {
                FIRSTNAME: 'joe',
                LASTNAME: 'bob',
                JERSEYNUMBER: 12,
                POSITION_NAME: 'Forward'
            };

            beforeEach(function () {
                this.result = mapping.create('ReportingTeamPlayer').map(row);
            });

            it('should map the player name', function () {
                expect(this.result.name).to.equal('joe bob');
            });

            it('should map the date', function () {
                expect(this.result.jerseynumber).to.equal(12);
            });

            it('should map the city', function () {
                expect(this.result.position).to.equal('Forward');
            });

        });

    })


});
