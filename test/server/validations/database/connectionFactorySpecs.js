'use strict';

var chai = require('chai'),
    expect = chai.expect,
    sut = require('../../../../src/validations/database/connection'),
    sqlite3 = require('sqlite3').verbose();

describe('connection factory', function () {

    beforeEach(function () {
        this.connectionString = ':memory:';
    });

    it('should create a connection from a string', function () {
        expect(sut.create(this.connectionString)).to.be.ok;
    });

    it('should create a connection from an object', function () {
        expect(sut.create(new sqlite3.Database(this.connectionString))).to.be.ok;
    });
});