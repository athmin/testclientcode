'use strict';

var chai = require('chai'),
    expect = chai.expect,
    sqlite3 = require('sqlite3'),
    database = require('../../../../src/validations/database'),
    teamSeasonValidator = require('../../../../src/validations/database/teamseason'),
    gameValidator = require('../../../../src/validations/database/game'),
    app = require('../../../../src/server');

var teamSql = 'CREATE TABLE ZTEAM ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZPRIMARYTEAM INTEGER, ZICON INTEGER, ZASSOCIATION VARCHAR, ZDIVISION VARCHAR, ZGENDER VARCHAR, ZLEAGUE VARCHAR, ZLEVEL VARCHAR, ZSEASON VARCHAR, ZSEASONENDDATE VARCHAR, ZSEASONSTARTDATE VARCHAR, ZTEAMNAME VARCHAR, ZUUID VARCHAR );';
var gameSql = 'CREATE TABLE ZGAME ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZCURRENTTEAMSTRENGTH INTEGER, ZHOMEROSTER INTEGER, ZHOMETEAM INTEGER, ZSCOREKEEPER INTEGER, ZTIMEKEEPER INTEGER, ZVISITORROSTER INTEGER, ZVISITORTEAM INTEGER, ZDATE TIMESTAMP, ZENDTIME TIMESTAMP, ZSTARTTIME TIMESTAMP, ZARENA VARCHAR, ZCITY VARCHAR, ZCOMMENTS VARCHAR, ZCOUNTRY VARCHAR, ZDIVISION VARCHAR, ZGAMETYPE VARCHAR, ZLEAGUE VARCHAR, ZLEVEL VARCHAR, ZPROVINCE VARCHAR, ZREMARKS VARCHAR, ZUUID VARCHAR );';
var gameEventSql = 'CREATE TABLE ZGAMEEVENT ( Z_PK INTEGER PRIMARY KEY, Z_ENT INTEGER, Z_OPT INTEGER, ZPERIOD INTEGER, ZEVENT INTEGER, ZEVENTROLE INTEGER, Z8_EVENTROLE INTEGER, ZGAME INTEGER, ZGAMESTRENGTH INTEGER, ZGOALIE INTEGER, ZHOMEPLAYER INTEGER, ZRECORDEDBYTEAM INTEGER, ZTEAMSTRENGTH INTEGER, ZVISITORPLAYER INTEGER, ZEVENTTIME TIMESTAMP, ZLOCATIONX FLOAT, ZLOCATIONY FLOAT, ZCOLLECTOR VARCHAR, ZUUID VARCHAR );';

describe('database validation', function () {

    it('should validate a database', function () {
        //expect(database.validate(':memory:')).to.be.true;
    });

});

describe('teamseason validation', function () {

    beforeEach(function (done) {
        var self = this;
        this.db = new sqlite3.Database(':memory:');
        this.sut = teamSeasonValidator;
        this.db.serialize(function () {
            self.db.run(teamSql);
            done();
        });
    });

    afterEach(function () {
        this.db.close();
    });

    it('should fail when no teamseasons found', function (done) {
        this.sut.validate(this.db, '123', function (err, results) {
            if (err) return done(err);
            expect(results.length).to.equal(1);
            expect(results[0].message).to.equal('teamseason uuid not found in database');
            done();
        });
    });

    describe('teamseason found', function () {

        beforeEach(function (done) {
            var self = this;
            this.db.serialize(function () {
                self.db.run('insert into ZTEAM (Z_PK, ZUUID, ZPRIMARYTEAM) values (1, $uuid, 1)', {$uuid: '456'}, done);
            });

        });

        it('should fail when the incorrect teamseason (123) is found', function (done) {
            this.sut.validate(this.db, '123', function (err, results) {
                if (err) return done(err);
                expect(results.length).to.equal(1);
                expect(results[0].message).to.equal('teamseason uuid mismatch found in database. expected: 123, actual: 456');
                done();
            });
        });

        it('should pass when the correct teamseason (456) is found', function (done) {
            this.sut.validate(this.db, '456', function (err, results) {
                if (err) return done(err);
                expect(results.length).to.equal(0);
                done();
            });
        });
    });



});

describe('game validation', function () {
    beforeEach(function (done) {
        var self = this;
        this.db = new sqlite3.Database(':memory:');
        this.sut = gameValidator;
        this.db.serialize(function () {
            self.db.run(gameSql);
            self.db.run(gameEventSql);
            done();
        });
    });

    afterEach(function () {
        this.db.close();
    });

    describe('Reporting Games', function () {

        before(function (done) {
            app.models.ReportingGame.create([
                {teamSeasonUUID: '123', uuid: '1'},
                {teamSeasonUUID: '123', uuid: '2'},
                {teamSeasonUUID: '123', uuid: '3'},
                {teamSeasonUUID: '456', uuid: '4'}
            ], done);
        });

        after(function (done) {
            app.models.ReportingGame.destroyAll(done);
        });

        beforeEach(function (done) {
            var self = this;
            this.db.serialize(function () {
                self.db.run('insert into ZGAME (Z_PK, ZUUID) values (1, $uuid)', {$uuid: '2'}, done);
            });
        });

        it('should fail when a game is found in another team season', function (done) {
            this.sut.validate(this.db, '456', function (err, results) {
                if (err) return done(err);
                expect(results.length).to.equal(1);
                expect(results[0].message).to.equal('game uuid(s) found in existing team seasons. ["2"]');
                done();
            });
        });

        it('should pass when a game is not found in another team season', function (done) {
            this.sut.validate(this.db, '123', function (err, results) {
                if (err) return done(err);
                expect(results.length).to.equal(0);
                done();
            });
        });

    });

    describe('Reporting Game Events', function () {

        before(function (done) {
            app.models.GameEvent.create([
                {teamSeasonUUID: '123', uuid: '1'},
                {teamSeasonUUID: '123', uuid: '2'},
                {teamSeasonUUID: '123', uuid: '3'},
                {teamSeasonUUID: '456', uuid: '4'}
            ], done);
        });

        after(function (done) {
            app.models.GameEvent.destroyAll(done);
        });

        beforeEach(function (done) {
            var self = this;
            this.db.serialize(function () {
                self.db.run('insert into ZGAMEEVENT (Z_PK, ZUUID) values (1, $uuid)', {$uuid: '2'}, done);
            });
        });

        it('should fail when a game event is found in another team season', function (done) {
            this.sut.validate(this.db, '456', function (err, results) {
                if (err) return done(err);
                expect(results.length).to.equal(1);
                expect(results[0].message).to.equal('game event uuid(s) found in existing team seasons. ["2"]');
                done();
            });
        });

        it('should pass when a game is not found in another team season', function (done) {
            this.sut.validate(this.db, '123', function (err, results) {
                if (err) return done(err);
                expect(results.length).to.equal(0);
                done();
            });
        });

    });
});


