'use strict';

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var del = require('del');
var path = require('path');
var wiredep = require('wiredep').stream;

var paths = {
    styles: path.join(__dirname, 'src/public/styles'),
    bower: path.join(__dirname, 'src/public/bower_components'),
    tmp: path.join(__dirname, '.tmp'),
    dist: path.join(__dirname, 'dist'),
    test: path.join(__dirname, 'test')
};

gulp.task('wiredep', function () {
    return gulp.src('./src/public/index.html')
        .pipe(wiredep())
        .pipe(gulp.dest('./src/public/'));
});

gulp.task('test:ui', function (done) {
    karma.start({
        // configFile: path.join(__dirname, '/karma.conf.js'),
        // singleRun: true
    }, done);
});

gulp.task('test:server', function () {
    return gulp.src(path.join(paths.test, 'server/**/*.js'), {read: false})
        .pipe(plugins.mocha({reporter: 'spec'}));
});

gulp.task('less', function () {
    return gulp.src(path.join(paths.styles, 'application.less'))
        .pipe(plugins.sourcemaps.init({ loadMaps: true }))
        .pipe(plugins.less({ paths: [paths.bower] }))
        .pipe(plugins.minifyCss())
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(path.join(paths.tmp, 'styles')));
});

gulp.task('loopback:services', function () {
    return gulp.src('./src/server.js')
    .pipe(plugins.loopbackSdkAngular({
        ngModuleName: 'statsGuyServices.loopback'
    }))
    .pipe(plugins.rename('loopback.js'))
    .pipe(gulp.dest('./src/public/js/services'));
});

gulp.task('lint', function () {
    return gulp.src(['src/**/*.js', '!src/public/bower_components/**'])
        .pipe(plugins.jshint('.jshintrc'))
        .pipe(plugins.jshint.reporter('jshint-stylish'));
});

gulp.task('clean', function (cb) {
    del([paths.dist, paths.tmp], cb);
});

gulp.task('serve', function () {
    return plugins.nodemon({
        script: path.join(__dirname, 'src/server.js'),
        watch: 'src',
        ext: 'js',
        ignore: 'src/public',
        env: { 'NODE_ENV': 'development' }
    })
        .on('change', ['lint', 'loopback:services'])
        .on('restart', function () {
            console.log('restarted!');
        });
});

gulp.task('watch', function () {
    plugins.livereload.listen();
    gulp.watch('src/public/**/*.js', ['lint']);
    gulp.watch('src/public/**/*.less', ['less']);
    gulp.watch('test/**/*.js', ['test']);
    gulp.watch(['src/public/**', '!src/public/bower_components'], plugins.livereload.changed);
});

gulp.task('db:migrate', function () {
    var db = require('./src/bin/db');

    return db.migrate();
});

gulp.task('db:update', function () {
    var db = require('./src/bin/db');

    return db.update();
});

/**
 * Grouped tasks
 */

gulp.task('test', ['test:ui', 'test:server']);

gulp.task('default', ['wiredep', 'less', 'lint', 'test', 'watch', 'serve']);

gulp.task('compile', ['wiredep', 'less', 'loopback:services']);

gulp.task('heroku:production', ['clean', 'less', 'loopback:services'],
function () {
    gulp.src(['src/**', '!src/public/**'])
        .pipe(gulp.dest(paths.dist));

    gulp.src([
            'src/public/**',
            path.join(paths.tmp, '**/*.css'),
            '!src/public/**/*.js',
            '!src/public/bower_components/**',
            '!src/public/**/*.less',
            '!src/public/index.html']
    )
        .pipe(gulp.dest(path.join(paths.dist, 'public/')));

    gulp.src('src/public/index.html')
        .pipe(plugins.usemin({
            vendor: [plugins.uglify(), plugins.rev()],
            js: [plugins.rev()]
        }))
        .pipe(gulp.dest(path.join(paths.dist, 'public/')));
});
