"use strict";

module.exports = {
    port: process.env.PORT,
    apiExplorer: false,
    S3: {
        key: process.env.S3_KEY,
        secret: process.env.S3_SECRET,
        bucket: process.env.S3_BUCKET
    },
    appVersion: {
      hp1: process.env.HP1_VERSION,
      hp3: process.env.HP3_VERSION
    },
    trialLength: process.env.TRIAL_LENGTH,
    userVoiceDomain: 'ithiamsports',
    userVoiceKey: process.env.USER_VOICE_KEY,
    mailer: {
        recipient: process.env.MAILER_RECIPIENT
    },
    PayPalCred: {
      username: process.env.PAYPAL_USERNAME,
      password: process.env.PAYPAL_PASSWORD,
      signature: process.env.PAYPAL_SIGNATURE
    },
    PayPalSandBox: process.env.PAYPAL_SANDBOX,
    currency: process.env.CURRENCY
};
