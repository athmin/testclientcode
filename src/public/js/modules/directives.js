'use strict';

angular.module('statsGuyDirectives', [])

/**
 * As this file grows larger, it may be worth taking the time to refactor each section into
 * its' own controller. Check out modules/controllers.js or modules/services.js to see how other
 * components have been modularized
 */

/**
* Users
*/
.directive('userListItem', function () {
    return {
        restrict: 'A',
        templateUrl: 'views/users/list.item.tpl.html'
    };
})

.directive('userCreateForm', function () {
    return {
        restrict: 'A',
        templateUrl: 'views/users/form.tpl.html'
    };
})

/**
* Teams
*/
.directive('teamListItem', function () {
    return {
        restrict: 'A',
        templateUrl: 'views/teams/list.item.tpl.html'
    };
})

/**
* Reports
*/
.directive('reportsSidebar', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/sidebar.tpl.html'
    };
})

.directive('reportsTypeReport', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/directives/report.tpl.html'
    };
})

.directive('reportsTypeEvents', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/directives/events.tpl.html'
    };
})

.directive('reportsTypeFaceOff', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/directives/face-off.tpl.html'
    };
})

.directive('cellData', function () {
    return {
        restrict: 'A',
        templateUrl: 'views/reports/directives/cell-data.tpl.html',
        scope: {
            data: '@'
        }
    };
})

.directive('hideSidebarControl', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/directives/filters/hide-sidebar-control.tpl.html'
    };
})

.directive('selectReportControl', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/directives/filters/select-report-control.tpl.html'
    };
})

.directive('filterPeriodControl', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/directives/filters/filter-period-control.tpl.html'
    };
})

.directive('filterReportControl', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/directives/filters/filter-report-control.tpl.html'
    };
})

.directive('filterDisplayControl', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/directives/filters/filter-display-control.tpl.html'
    };
})

.directive('filterSpecialtyControl', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/reports/directives/filters/filter-specialty-control.tpl.html'
    };
});
