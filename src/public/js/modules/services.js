'use strict';

angular.module('statsGuyServices', [
    'statsGuyServices.alert',
    'statsGuyServices.authentication',
    'statsGuyServices.loopback',
    'statsGuyServices.country'
]);
