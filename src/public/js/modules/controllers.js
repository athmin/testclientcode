'use strict';

angular.module('statsGuyControllers', [
    'statsGuyControllers.alert',
    'statsGuyControllers.modal',
    'statsGuyControllers.fileUpload',
    'statsGuyControllers.application',
    'statsGuyControllers.report',
    'statsGuyControllers.season',
    'statsGuyControllers.team',
    'statsGuyControllers.user',
    'statsGuyControllers.log'
]);
