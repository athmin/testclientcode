'use strict';

angular.module('statsGuyFilters', [])

// This filter makes the assumption that the input will be in decimal form (i.e. 17% is 0.17).
.filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
        var result = $filter('number')(input * 100, decimals);
        if (result == 0) {
            return '0%';
        } else {
            return result + '%';
        }
    };
}]);
