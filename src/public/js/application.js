'use strict';

angular.module('statsGuy', [
    // Vendor
    'ngResource',
    'ngSanitize',
    'ngCookies',
    'ui.bootstrap',
    'ui.router',
    'ui.select',
    'angularFileUpload',
    'ngCsv',
    'btford.socket-io',

    // Application-specific
    'statsGuyControllers',
    'statsGuyDirectives',
    'statsGuyFilters',
    'statsGuyServices'
])

.constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
})

.constant('USER_ROLES', [
  { name: 'ISA', id: 1 },
  { name: 'User', id: 11}
  ])

.constant('TEAM_SEASON_ROLES', [
  { name: 'Coach', id: 21 },
  { name: 'Statistician', id: 31},
  { name: 'Owner', id: 41}
  ])

.config(['$stateProvider', '$urlRouterProvider',
function ($stateProvider, $urlRouterProvider) {

    // Routes

    $stateProvider
        .state('login', {
            url: '/login',
            containerClass: 'login',
            views: {
                'container@': {
                    templateUrl: 'views/login.tpl.html',
                    controller: 'LoginController'
                }
            }
        })
        .state('public', {
            url: '/public',
            containerClass: 'reports',
            views: {
                'container@': {
                    templateUrl: 'views/reports/index.tpl.html',
                    controller: 'ReportsController'
                }
            }
        })
        .state('reports', {
            url: '/',
            authenticate: true,
            containerClass: 'reports',
            views: {
                'container@': {
                    templateUrl: 'views/reports/index.tpl.html',
                    controller: 'ReportsController'
                }
            }
        })
        .state('teams', {
            url: '/subscriptions',
            authenticate: true,
            containerClass: 'teams',
            views: {
                'container@': {
                    templateUrl: 'views/teams/list.tpl.html',
                    controller: 'TeamListController'
                }
            }
        })
        .state('teams.create', {
            url: '/create',
            authenticate: true,
            containerClass: 'team-create',
            views: {
                'container@': {
                    templateUrl: 'views/teams/create.tpl.html',
                    controller: 'TeamCreateController'
                }
            }
        })
        .state('teams.upgrade', {
            url: '/upgrade/:id',
            authenticate: true,
            containerClass: 'team-upgrade',
            views: {
                'container@': {
                    templateUrl: 'views/teams/upgrade.tpl.html',
                    controller: 'TeamUpgradeController'
                }
            }
        })
        .state('teams.edit', {
            url: '/:id/edit',
            authenticate: true,
            containerClass: 'team-edit',
            views: {
                'container@': {
                    templateUrl: 'views/teams/edit.tpl.html',
                    controller: 'TeamEditController'
                }
            }
        })
        .state('seasons', {
            url: '/seasons',
            authenticate: true,
            containerClass: 'seasons'
        })
        .state('seasons.create', {
            url: '/create',
            authenticate: true,
            containerClass: 'season-create',
            views: {
                'container@': {
                    templateUrl: 'views/seasons/create.tpl.html',
                    controller: 'SeasonCreateController'
                }
            }
        })
        .state('seasons.edit', {
            url: '/:id/edit',
            authenticate: true,
            containerClass: 'season-edit',
            views: {
                'container@': {
                    templateUrl: 'views/seasons/edit.tpl.html',
                    controller: 'SeasonEditController'
                }
            }
        })
        .state('users', {
            url: '/users',
            authenticate: true,
            containerClass: 'users',
            views: {
                'container@': {
                    templateUrl: 'views/users/list.tpl.html',
                    controller: 'UsersController'
                }
            }
        })
        .state('logs', {
            url: '/logs',
            authenticate: true,
            containerClass: 'logs',
            views: {
                'container@': {
                    templateUrl: 'views/logs/list.tpl.html',
                    controller: 'LogsController'
                }
            }
        })
        .state('account', {
            url: '/',
            authenticate: true,
            containerClass: 'account',
            views: {
                'container@': {
                    templateUrl: 'views/reports/index.tpl.html'
                    //controller: 'ContactNumberController'
                }
            }
        })
        .state('account.new', {
            url: 'account/new',
            authenticate: true,
            containerClass: 'account',
            views: {
                'container@': {
                    templateUrl: 'views/account/new.tpl.html',
                    controller: 'ContactNumberController'
                }
            }
        });

    $urlRouterProvider.otherwise('/');

}])

/**
 * Enable CORS
 */
.config(
    ['$httpProvider',
    function ($httpProvider) {

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

}])

/**
 * State/location event behaviour
 */
.run(
    ['$rootScope', '$location', 'AUTH_EVENTS', 'User',
    function ($rootScope, $location, AUTH_EVENTS, User) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

        // Redirect requests from /login to / if the user is authenticated
        if( toState.name === "root.login" && User.isAuthenticated() ) {
            $location.path( "/" );
        }

        // Redirect to /login if the user is not authenticated
        if($location.path() !== '/public'){
          if( (typeof(toParams.authenticate) === "undefined" || toParams.authenticate) && !User.isAuthenticated() ) {
              $location.path( "/login" );
          }
        }

    });

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

        // Change the active class (to be added to the body element)
        $rootScope.containerClass = toState.containerClass;

    });

}])

.factory('SocketIO', function (socketFactory) {
  return socketFactory();
});
