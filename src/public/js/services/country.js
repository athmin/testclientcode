'use strict';

angular.module('statsGuyServices.country', ['ngResource'])
.service('Country', ['$resource', function ($resource) {

    return {
      all: function () {
        return $resource('/api/countries/all').query();
      },
      provinces: function () {

        // only return Canadian provinces for now
        return $resource('/api/countries/CAN/regions').query();

      }
    }

  }]);