'use strict';

angular.module('statsGuyServices.authentication', [])

.factory('OAuth',
['$rootScope', '$window', '$interval', '$cookies', '$cookieStore', 'User', 'LoopBackAuth', 'AUTH_EVENTS',
function ($rootScope, $window, $interval, $cookies, $cookieStore, User, LoopBackAuth, AUTH_EVENTS) {

    var OAuthService = {};

    // Place all authentication provider URLs in here
    OAuthService.providers = {
        google: '/auth/google'
        // e.g. linkedin: '/auth/linkedin'
    };

    /**
     * Opens a login window and begins OAuth process. A timer checks to see if our cookies have been set,
     * and populates the relevant data into our services
     */
    OAuthService.login = function () {
        var loginWindow = $window.open('/auth/google', 'Example', 'width=600, height=600, top=' +
        ($window.screen.height - 600) / 2 + ', left=' + ($window.screen.width - 600) / 2);

        // Continually check the window for
        var pollTimer = $interval(function () {
            try {
                // Make sure the cookie's been set before we extract it's data
                if ($cookies.access_token && $cookies.userId) {
                    $interval.cancel(pollTimer);

                    var userId = $cookies.userId,
                        accessToken = $cookies.access_token;

                    LoopBackAuth.setUser(accessToken, userId, {});
                    LoopBackAuth.save();

                    loginWindow.close();
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                }
            } catch (e) {
                console.log(e);
                $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                loginWindow.close();
            }
        }, 500);
    };

    /**
     * Log out of the server session and clear all auth-related client-side data
     */
    OAuthService.logout = function () {
        User.logout(function (value, responseHeader) {
            // Clear user info from our cookies
            $cookieStore.remove("access_token");
            $cookieStore.remove("userId");

            // Clear the user from our Auth service
            LoopBackAuth.clearUser();
            LoopBackAuth.save();

            $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
        });
    };

    return OAuthService;

}])

/**
 * Catch all 401 (unauthenticated) responses and emit the appropriate event
 */
.factory('OAuthHttpResponseInterceptor',
['$q', '$rootScope', 'AUTH_EVENTS',
function ($q, $rootScope, AUTH_EVENTS) {

    return {
        response: function(response){
            if (response.status === 401) {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
            }
            return response || $q.when(response);
        },
        responseError: function(rejection) {
            if (rejection.status === 401) {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
            }
            return $q.reject(rejection);
        }
    };

}])

.config(
['$httpProvider',
function ($httpProvider) {

    $httpProvider.interceptors.push('OAuthHttpResponseInterceptor');

}]);
