'use strict';

angular.module('statsGuyServices.alert', [])

.factory('Alert',
    ['$timeout',
    function ($timeout) {

    var Alert = {};

    Alert.alerts = [
        // e.g. { type: 'danger', message: 'Snap, son! Something bad happened.' }
    ];

    /**
     * @param {String} type         'success', 'info','danger', or 'warning'
     * @param {String} message
     */
    Alert.add = function (type, message) {
        var alert = this.alerts.push({ type: type, msg: message });

        $timeout(function() {
            this.alerts.splice(this.alerts.indexOf(alert), 1);
        }.bind(this), 5000);
    };

    Alert.persistent = function (type, message) {
        this.alerts.push({ type: type, msg: message });
    };

    Alert.close = function (index) {
        this.alerts.splice(index, 1);
    };

    Alert.closeAll = function (index) {
        this.alerts.length = 0;
    };

    Alert.handleError = function (err) {
        Alert.add('danger', err.data.error.message);
    };

    return Alert;

}]);
