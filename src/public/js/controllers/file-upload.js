'use strict';

angular.module('statsGuyControllers.fileUpload', [])

.controller('FileUploadController',
['$scope', '$modalInstance', '$upload', 'teamSeason', 'Alert',
function ($scope, $modalInstance, $upload, teamSeason, Alert) {

    $scope.heading = "Upload to: "+ teamSeason.name;
    $scope.percentCompleted = 0;

    var selected = [];
    $scope.onFileSelect = function ($files) {
        selected = $files;
    };

    $scope.ok = function () {

        //$files: an array of files selected, each file has name, size, and type.
        for (var i = 0; i < selected.length; i++) {
            var file = selected[i];
            $scope.upload = $upload.upload({
                url: '/api/team/backup', //upload.php script, node.js route, or servlet url
                //method: 'POST' or 'PUT',
                //headers: {'header-key': 'header-value'},
                //withCredentials: true,
                data: {teamSeason: teamSeason.uuid},
                file: file // or list of files ($files) for html5 only
                //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
                // customize file formData name ('Content-Disposition'), server side file variable name.
                //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file'
                // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
                //formDataAppender: function(formData, key, val){}
            }).progress(function(evt) {
                // Todo: implement streaming uploads on the back-end
                $scope.percentCompleted = parseInt(100.0 * (evt.loaded / evt.total));
            }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                //Alert.close();
                //Alert.add('success', 'The Team-Season database was successfully pushed to the server');
                $modalInstance.close(true);
            }).error(function() {
                // file did not upload successfully
                Alert.persistent('warning', 'Error - The Team-Season database could not be pushed to the server');
                $modalInstance.close(true);
            });
            //.error(...)
            //.then(success, error, progress);
            // access or attach event listeners to the underlying XMLHttpRequest.
            //.xhr(function(xhr){xhr.upload.addEventListener(...)})
        }

    };

    $scope.cancel = function () {
        $modalInstance.dismiss(false);
    };

    $scope.selected = function (season) {
        $scope.selectedModel = season;
    };

}]);
