'use strict';

angular.module('statsGuyControllers.alert', [])

.controller('AlertsController',
['$scope', 'Alert',
function ($scope, Alert) {

    $scope.alerts = Alert.alerts;
    $scope.closeAlert = function (index) {
        Alert.close(index);
    };

}]);
