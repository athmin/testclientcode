'use strict';

angular.module('statsGuyControllers.season', [])

.controller('SeasonCreateController',
['$scope', '$state', 'Season', 'Alert', 'App',
function ($scope, $state, Season, Alert, App) {

    $scope.newSeason = {
      active: 'no'
    };
    $scope.selected = {
      app: {}
    };
    $scope.apps = App.find({ filter: { where: { active: 'yes' } } }, function (results) {
      $scope.selected.app = results[0];
    });
    $scope.activeOptions = ['yes', 'no'];

    $scope.create = function () {
        App.seasons.create({ id: $scope.selected.app.id }, $scope.newSeason)
        .$promise
        .then(function (data) {
            $scope.newSeason = {};
            $state.go('teams');
            Alert.add('success', 'The season was successfully created');
        }, function (err) {
            Alert.add('danger', err.data.error.message);
        });
    };

    $scope.cancel = function () {
        $scope.newSeason = {};
        $state.go('teams');
    };

}])

.controller('SeasonSelectController',
['$scope', '$modalInstance', 'Season',
function ($scope, $modalInstance, Season) {

    $scope.heading = "Select Season";
    $scope.seasons = Season.find();

    $scope.ok = function () {
        if ($scope.selectedModel) {
            $modalInstance.close($scope.selectedModel);
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss(false);
    };

    $scope.selected = function (season) {
        $scope.selectedModel = season;
    };
}]);
