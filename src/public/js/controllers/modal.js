'use strict';

angular.module('statsGuyControllers.modal', [])

.controller('ModalConfirm',
['$scope', '$modalInstance', 'item',
function($scope, $modalInstance, item) {

    $scope.heading = "Confirm";

    if(item[0].type === 'User'){
      $scope.message = "Are you sure you want to delete: "+ item[0].type+ " - " +item[1].email;
    }
    else{
      $scope.message = "Are you sure you want to delete: "+ item[0].type+ " - " +item[1].name;
    }

    $scope.ok = function () {
        $modalInstance.close(true);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}])

.controller('ModalCreatTeamNoPlayers',
['$scope', '$modalInstance',
function($scope, $modalInstance) {

    $scope.heading = "Create new Team";
    $scope.message = "Are you sure you want to create your new team database with no members?";

    $scope.ok = function () {
        $modalInstance.close(true);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);
