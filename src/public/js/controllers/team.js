'use strict';

angular.module('statsGuyControllers.team', [])

.constant('APPS', {
  'hp1': 1,
  'hp3': 2
})

.controller('TeamListController',
['$scope', 'Team', '$modal', 'TeamSeason', 'User', 'Alert', 'TEAM_SEASON_ROLES', 'APPS',
function ($scope, Team, $modal, TeamSeason, User, Alert, TEAM_SEASON_ROLES, APPS) {

    $scope.teams = Team.find({
        filter: {
            include: {
                relation: 'teamSeasons',
                scope: {
                    include: [{
                        relation: 'season',
                        scope: {
                            fields: ['name']
                        }
                    },{
                        relation: 'userRoles',
                        scope: {
                          fields: ['roleId'],
                          where: {
                            userId: User.getCurrentId()
                          },
                        },
                    }]
                }
            }
        }
    }, function(teams){
      _.each((teams), function(team){
        _.each((team.teamSeasons), function(teamSeason){
            if(teamSeason.userRoles[0] !== undefined){
              var role = TEAM_SEASON_ROLES;
              role = $.grep(role,
                         function(o,i) { return o.id !== teamSeason.userRoles[0].roleId; },
                         true);
              if(role.length !== 0){
                teamSeason.userRoles[0].name = role[0].name;
              }else{
                teamSeason.userRoles[0].name = "";
              }
            }
            else if(teamSeason.userRoles.length ===  0){
              teamSeason.userRoles = [{0:{name: ""}}];
              teamSeason.userRoles[0].name = "";
            }
        });
      });
    });

    $scope.editedTeamSeason = undefined;

    var accessibleTeamSeasons = [];
    var accessibleTeams = [];

    User.findOne({
        filter: {
            where: {
              id: User.getCurrentId()
            },
            include: [{
                relation: 'teamSeasons'
            }]
        }
    })
    .$promise
    .then(function (user) {
      _.each((user.teamSeasons), function(teamSeason){
          accessibleTeamSeasons.push(teamSeason.id);
          accessibleTeams.push(teamSeason.teamId);
      });
    });

    $scope.users = [];

    function getUsers () {
        User.find({
        })
        .$promise
        .then(function (result) {
            $scope.users = result;
        });
    }

    getUsers();

    $scope.addSeason = function (team) {

        var modal = $modal.open({
            templateUrl: 'views/partials/modal-select-season.tpl.html',
            controller: 'SeasonSelectController'
        });

        modal.result.then(function (selected) {
            Team.teamSeasons.create({id: team.id}, {seasonId: selected.id, teamId: team.id}, function (data) {
                TeamSeason.findOne({filter: {where: {id: data.id}, include: {
                    relation: 'season',
                    scope: {
                        fields: ['name']
                    }
                }}}, function (result) {
                    team.teamSeasons.push(result);
                });
            }, Alert.handleError);
        });
    };

    $scope.deleteTeamSeasonConfirm = function (teamSeason) {
      var modal = $modal.open({
          templateUrl: 'views/partials/modal-confirm.tpl.html',
          controller: 'ModalConfirm',
          resolve: {
            item: function () {
              var array = [{type: 'Team Season'}, teamSeason]
              return array;
            }
         }
      });

      // Display a confirmation modal
      modal.result.then(function (confirmation) {
          if (confirmation === true) {
            $scope.deleteTeamSeason(teamSeason)
          }
      });
    };

    $scope.deleteTeamSeason = function (teamSeason) {

        _.each(($scope.users), function(user){
            User.teamSeasons.unlink({
                id: user.id,
                fk: teamSeason.id
            }, function (teamSeason) {
                _.remove(user.teamSeasons, {id: teamSeason.id});
               }, Alert.handleError);
        });

        TeamSeason.destroyById({id: teamSeason.id}, function () {
            var team = _.find($scope.teams, {id: teamSeason.teamId});
            _.remove(team.teamSeasons, {id: teamSeason.id});
        }, Alert.handleError);
    };

    $scope.deleteTeam = function (team) {
      var modal = $modal.open({
          templateUrl: 'views/partials/modal-confirm.tpl.html',
          controller: 'ModalConfirm',
          resolve: {
            item: function () {
              var array = [{type: 'Team'}, team]
              return array;
            }
         }
      });

      // Display a confirmation modal
      modal.result.then(function (confirmation) {
          if (confirmation === true) {

            _.each((team.teamSeasons), function(teamSeason){
                $scope.deleteTeamSeason(teamSeason);
            });

            Team.destroyById({ id: team.id }, function () {
                _.remove($scope.teams, { id: team.id });
                Alert.add('success', 'The team was successfully deleted');
            });
          }
      });
    };

    $scope.upload = function (teamSeason) {

        var modal = $modal.open({
            templateUrl: 'views/partials/modal-file-upload.tpl.html',
            controller: 'FileUploadController',
            resolve: {
                teamSeason: function () {
                    return teamSeason;
                }
            }
        });

        modal.result.then(function (succcess) {

        });
    };

    $scope.pull = function (teamSeason) {

        window.open('/api/team/backup/last?teamSeason=' + teamSeason.uuid, '_blank', '');

    };

    $scope.toggleEditTeamSeason = function (teamSeason) {
        $scope.editedTeamSeason = teamSeason;
    };

    $scope.cancelEditTeamSeason = function () {
        $scope.editedTeamSeason = undefined;
    };

    $scope.submitEditTeamSeason = function () {
        if ($scope.editedTeamSeason !== undefined) {
            TeamSeason.update({
                where: { id: $scope.editedTeamSeason.id }
            }, {
                name: $scope.editedTeamSeason.name
            }, function (teamSeason) {
                Alert.add('success', 'The Team Season has been updated successfully');

                $scope.editedTeamSeason = undefined;
            }, Alert.handleError);
        }
    };

    $scope.isEditingTeamSeason = function (teamSeason) {
        if ($scope.editedTeamSeason !== undefined) {
            return $scope.editedTeamSeason.id === teamSeason.id;
        }
    };

    $scope.accessibleTeamSeasons = function(value) {
      return (accessibleTeamSeasons.indexOf(value.id) !== -1);
    };

    $scope.accessibleTeams = function(value) {
      return (accessibleTeams.indexOf(value.id) !== -1);
    };

}])

.controller('TeamUpgradeController',
  ['$scope', '$state', '$stateParams', 'Team', 'Season', 'TeamSeason', 'User', 'Country',
  function ($scope, $state, $stateParams, Team, Season, TeamSeason, User, Country) {


    $scope.selected = {};

    $scope.countries = Country.all();
    $scope.provinces = Country.provinces();

    $scope.season = {};
    $scope.seasonExpired = false;
    $scope.price = {};

    $scope.teamSeason = TeamSeason.findById({
      id: $stateParams.id
    }, function (teamSeason) {
      $scope.season = Season.findById({
        id: teamSeason.seasonId
      }, function (season) {
        $scope.season = season;

        $scope.effectiveDate = effectiveDate(season.startDate);
        $scope.expiryDate = season.endDate;

        $scope.price = Season.price({
          seasonId: season.id
        });

        if (new Date() > new Date(season.endDate)) {
          $scope.seasonExpired = true;
        }
      });
    });

    $scope.cancel = function () {
        $state.go('teams');
    };

    // Returns the passed effective date if its in the future, otherwise returns
    // today's date
    function effectiveDate (date) {
      if (new Date(date) > new Date()) {
        return date;
      } else {
        return new Date();
      }
    }

  }
])

.controller('TeamCreateController',
['$scope','$controller', '$state', 'Team', 'TeamSeason', 'Alert','Season', 'MasterSeason', 'User','$modal', 'Country', 'App', 'APPS',
function ($scope, $controller, $state, Team, TeamSeason, Alert, Season, MasterSeason, User, $modal, Country, App, APPS) {

    var TeamListController = $scope.$new();
    $controller('TeamListController', { $scope: TeamListController });

    $scope.newTeam = {};
    $scope.newTeam.players = [];
    $scope.newPlayer = {};
    $scope.selected = {
      hp1: {},
      hp3: {},
      masterSeason: {},
      subscription: 'hp3'
    };
    $scope.prices = {
      hp1: {
        fullPrice: 0,
        payable: 0,
        discountedPercentage: 0
      },
      hp3: {
        fullPrice: 0,
        payable: 0,
        discountedPercentage: 0
      }
    },
    $scope.masterSeasons = MasterSeason.find({
      filter: {
        where: {
          active: 'yes'
        },
        include: ['seasons']
      }
    }, onMasterSeasonChange);
    $scope.seasons = {};
    $scope.countries = Country.all();
    $scope.provinces = Country.provinces();

    App.find({ filter: { where: { active: 'yes' } } }, function (results) {
      $scope.apps = _.object(_.map(results, function (result) {
        return [result.appTag, result]
      }));
    });

    // Update Child Seasons on MasterSeason change
    $scope.$watch('selected.masterSeason', function () {
      onMasterSeasonChange($scope.selected.masterSeason);
    });

    function onMasterSeasonChange (result) {
      var isArray = result.constructor === Array,
          seasons = isArray ? result[0].seasons : result.seasons;

      // Load child seasons (to load in product boxes) and convert projected
      // expiry date based on the start date
      var hp1Season = _.filter(seasons, { appId: APPS['hp1'] })[0];
      var hp3Season = _.filter(seasons, { appId: APPS['hp3'] })[0];

      if (hp1Season) $scope.selected.hp1.season = hp1Season;
      if (hp3Season) $scope.selected.hp3.season = hp3Season;

      // Select first result if we're passed an array
      if (isArray) $scope.selected.masterSeason = result[0];
      $scope.selected.masterSeason.monthLength = monthDifference(effectiveDate($scope.selected.masterSeason.startDate), $scope.selected.masterSeason.endDate);

      // Calculate prices per season
      $scope.getPrices();
    }

    $scope.trialExpiryDate = new Date();
    $scope.trialLength = Season.trialLength();
    $scope.currentTime = new Date();

    $scope.admin = {
      isDemo: false
    };

    $scope.$watch('admin.isDemo', function () {
      if ($scope.admin.isDemo) {
        $scope.selected.country = true;
        $scope.selected.region = true;
      } else {
        delete $scope.selected.country;
        delete $scope.selected.region;
      }
      // Get the trial expiry date from the server
      if ($scope.selected[$scope.selected.subscription].season) {
        $scope.trialExpiryDate = Season.trialExpiry({
          seasonId: $scope.selected[$scope.selected.subscription].season.id
        });
      }
    });

    // Opens the 'Learn More' modal
    $scope.learnMore = function () {
      var modal = $modal.open({
          templateUrl: 'views/partials/help/plans.tpl.html',
          controller: ['$scope', '$modalInstance', function ($scope, $modalInstance) {
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
          }]
      });
    }

    function monthDifference (startDate, endDate) {
      var months;
      var startDate = new Date(startDate),
          endDate = new Date(endDate);

      months = (endDate.getFullYear() - startDate.getFullYear()) * 12;
      months -= startDate.getMonth();
      months += endDate.getMonth() + 1;

      return months <= 0 ? 0 : months;
    }

    // Returns the passed effective date if its in the future, otherwise returns
    // today's date
    function effectiveDate (date) {
      if (new Date(date) > new Date()) {
        return date;
      } else {
        return new Date();
      }
    }

    $scope.monthDifference = monthDifference;
    $scope.effectiveDate = effectiveDate;

    // Gets the season prices and discounts from API
    $scope.getPrices = function () {
      if ($scope.selected.hp1.season) {
        Season.price({ seasonId: $scope.selected.hp1.season.id }, function (result) {
          $scope.prices.hp1.fullPrice = result.fullPrice;
          $scope.prices.hp1.payable = result.payable;
          $scope.prices.hp1.discountedPercentage = result.discountedPercentage;
        });
      }

      if ($scope.selected.hp3.season) {
        Season.price({ seasonId: $scope.selected.hp3.season.id }, function (result) {
          $scope.prices.hp3.fullPrice = result.fullPrice;
          $scope.prices.hp3.payable = result.payable;
          $scope.prices.hp3.discountedPercentage = result.discountedPercentage;
        });
      }
    };

    $scope.jerseyNumbers = function(start, end) {
        var result = [];
        for (var i = start; i <= end; i++) {
            result.push(i);
        }
        return result;
    };

    var playerId = 0;

    $scope.create = function () {
      Team.create({}, $scope.newTeam)
      .$promise
      .then(function (data) {
          $scope.newTeam = {};
          $state.go('teams');
          Alert.add('success', 'The team was successfully created');
      }, function (err) {
          Alert.add('danger', err.data.error.message);
      });
    };

    $scope.confirmCreateTeam = function () {

      if($scope.newTeam.players.length === 0){
        var modal = $modal.open({
            templateUrl: 'views/partials/modal-confirm.tpl.html',
            controller: 'ModalCreatTeamNoPlayers'
        });

        modal.result.then(function (confirmation) {
            if (confirmation === true) {
              $scope.createNewTeam();
            }
        });

      }else{
        $scope.createNewTeam();
      }
    };

    $scope.createNewTeam = function () {

      $scope.$modalInstance = $modal.open({
          templateUrl: 'views/partials/modal-loading.tpl.html',
          controller: 'TeamCreateController',
          backdrop: 'static',
          keyboard: false
      });
      var user = User.getCurrentId();


      Team.create({}, $scope.newTeam)
      .$promise
      .then(function (team) {

          var selected = $scope.selected[$scope.selected.subscription].season;
          Team.teamSeasons.create({id: team.id}, {seasonId: selected.id, teamId: team.id}, function (data) {
              TeamSeason.findOne({filter: {where: {id: data.id}, include: {
                  relation: 'season',
                  scope: {
                      fields: ['name']
                  }
              }}}, function (result) {
                  if(team.teamSeasons === undefined){
                    team.teamSeasons = [];
                    team.teamSeasons.push(result);
                  }
                  else{
                    team.teamSeasons.push(result);
                  }

                  User.teamSeasons.link({
                      id: user,
                      fk: result.id
                    }, {
                        roleId: 41
                      }, function (teamSeason) {
                  }, Alert.handleError);

                  $scope.createNewTeamDatabase(team, result);
              });
          }, Alert.handleError);

      }, function (err) {
          Alert.add('danger', err.data.error.message);
      });
    };

    $scope.createNewTeamDatabase = function (team, tSeason) {

      var season = angular.toJson($scope.selected.season);
      $scope.newTeam.season = JSON.parse(season);

      var teamSeason = angular.toJson(tSeason);
      $scope.newTeam.teamSeason = JSON.parse(teamSeason);

      var data = $scope.newTeam;

      $.ajax({
        type: 'POST',
        url: 'api/team/create?sport=Hockey',
        data: {data:data}
      })
      .done(function( msg ) {
        $scope.$modalInstance.close();
        $state.go('reports');
        Alert.add('success', 'Your new team database was succesfully created!');
      })
      .fail(function( msg ) {
        $scope.$modalInstance.close();
        Alert.add('warning', 'Sorry, something went wrong. Try signing out and back in again.');
        $state.go('teams.create');
        $scope.deleteTeam(team);
      });
    };

    $scope.deleteTeam = function (team) {

        _.each((team.teamSeasons), function(teamSeason){
            TeamListController.deleteTeamSeason(teamSeason);
        });

        Team.destroyById({ id: team.id }, function () {
            _.remove($scope.teams, { id: team.id });
        });
    };

    $scope.cancel = function () {
        $scope.newTeam = {};
        $state.go('teams');
    };

    $scope.addPlayer = function () {

        var players = $scope.newTeam.players;

        players.push({
          firstName: $scope.newPlayer.firstName,
          lastName: $scope.newPlayer.lastName,
          position: $scope.newPlayer.position,
          role: $scope.newPlayer.role,
          zJerseyNumber: $scope.newPlayer.jerseyNumber,
          id: playerId
        });

        $scope.newTeam.players = players;
        $scope.newPlayer.firstName = "";
        $scope.newPlayer.lastName = "";
        $scope.newPlayer.position = "";
        $scope.newPlayer.role = "";
        $scope.newPlayer.jerseyNumber = "";

        playerId++;
    };

    $scope.removePlayer = function (id) {
        var players = $scope.newTeam.players;
        players = $.grep(players,
                   function(o,i) { return o.id === id; },
                   true);

        $scope.newTeam.players = players;
    };

}])

.controller('TeamEditController',
['$scope', '$state', '$stateParams', 'Team', 'Alert',
function ($scope, $state, $stateParams, Team, Alert) {

    $scope.editedTeam = {};

    function getTeam() {
        Team.findById({ id: $stateParams.id })
        .$promise
        .then(function (data) {
            $scope.editedTeam = data;
        });
    }
    getTeam();

    $scope.edit = function () {
        Team.update({
            where: { id: $stateParams.id }
        }, $scope.editedTeam)
        .$promise
        .then(function (data) {
            $scope.editedTeam = {};
            $state.go('teams');
            Alert.add('success', 'The team was successfully updated');
        }, function (err) {
            Alert.add('danger', err.data.error.message);
        });
    };

    $scope.cancel = function () {
        $scope.editedTeam = {};
        $state.go('teams');
    };

}]);
