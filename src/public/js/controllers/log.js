'use strict';

angular.module('statsGuyControllers.log', [])
.constant('moment', window.moment)
.controller('LogsController', ['$scope', 'Log', 'moment', function ($scope, Log, moment) {

    $scope.reload = function () {
        $scope.logs = Log.find({ filter: {
            order: 'timestamp DESC'
        }});
    };

    $scope.clearLogs = function () {
        Log.clear().$promise.then($scope.reload());
    };

    $scope.reload();

    $scope.logsForCsv = function () {
        return _.map($scope.logs, function (log) {
            return {level: log.level, message: log.message, timestamp: log.timestamp};
        });
    };

    $scope.severities = [
        {id: '', label: 'All'},
        {id: 'info', label: 'Info'},
        {id: 'error', label: 'Error'}
    ];

    $scope.dateRanges = [
        {id: moment().subtract(10,'year'), label: 'All Time'},
        {id: moment().subtract(14400,'seconds'), label: 'Last 4 Hours'},
        {id: moment().subtract(1,'day'), label: 'Last Day'},
        {id: moment().subtract(1,'week'), label: 'Last Week'},
        {id: moment().subtract(2,'week'), label: 'Last 2 Weeks'},
        {id: moment().subtract(1,'month'), label: 'Last 1 Month'},
        {id: moment().subtract(3,'month'), label: 'Last 3 Months'}
    ];

    $scope.searchSeverity = $scope.severities[0];
    $scope.searchDate = $scope.dateRanges[0];

}])

.filter('isAfter', function() {
  return function(items, dateAfter) {
    return items.filter(function(item){
      return moment(item.timestamp).isAfter(dateAfter);
    })
  }
});
