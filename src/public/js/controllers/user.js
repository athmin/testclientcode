'use strict';

angular.module('statsGuyControllers.user', [])

.controller('UsersController',
['$scope', '$modal', '$filter', 'USER_ROLES', 'TEAM_SEASON_ROLES', 'User', 'UserIdentity', 'RoleMapping', 'Alert', 'TeamSeason',
function ($scope, $modal, $filter, USER_ROLES, TEAM_SEASON_ROLES, User, UserIdentity, RoleMapping, Alert, TeamSeason) {

    $scope.validations = {
        email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        phone: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
    };

    $scope.ROLES = USER_ROLES;
    $scope.users = [];

    $scope.newUser = {};
    $scope.editedUser = {};
    $scope.editedUserId = null;

    $scope.isAddingUser = false;

    function getUsers () {
        User.find({
            filter: {
                include: [{
                    relation: 'roles'}, {
                      relation: 'teamSeasons',
                      scope: {
                          include: {
                              relation: 'userRoles',
                          }
                      }
                }]
            }
        }, function(users){
          _.each((users), function(user){
            _.each((user.teamSeasons), function(teamSeason){
                if(teamSeason.userRoles[0] !== undefined){
                  for(var i in teamSeason.userRoles){
                    if(teamSeason.userRoles[i].userId === user.id){
                      var role = TEAM_SEASON_ROLES;
                      var roleId = teamSeason.userRoles[i].roleId;
                      role = $.grep(role,
                                 function(o,i) { return o.id !== roleId; },
                                 true);
                      if(role.length !== 0){
                        teamSeason.userRoles[i].name = role[0].name;
                      }else{
                        teamSeason.userRoles[i].name = "";
                      }
                      teamSeason.userRoles = teamSeason.userRoles[i];
                      break;
                    }
                  }
                }
                else if(teamSeason.userRoles.length ===  0){
                  teamSeason.userRoles = [{0:{name: ""}}];
                  teamSeason.userRoles[0].name = "";
                }
            });
          });
          $scope.users = users;
        });
    }

    getUsers();

    /**
    * Submit CREATE for newUser
    */
    $scope.submitCreate = function () {

      if($scope.newUser.contactNumber === undefined ){
          $scope.newUser.contactNumber = "";
      }

      $scope.newUser.contactNumber = $scope.newUser.contactNumber.replace(/[^\d\+]/g,"");

        User.create({}, {
            username: '',
            password: 'default',
            firstName: $scope.newUser.firstName,
            lastName: $scope.newUser.lastName,
            email: $scope.newUser.email,
            contactNumber: $scope.newUser.contactNumber,
            role: $scope.newUser.role,
        }, function (user, userHeaders) {

            RoleMapping.create({}, {
              principalType: "USER",
              principalId: user.id,
              roleId: $scope.newUser.role.id
            }, function(role){
            }, Alert.handleError);

            Alert.add('success', 'The user was successfully created');

            $scope.newUser = {};
            $scope.isAddingUser = false;
            $scope.users.push(user);
            getUsers();
        }, Alert.handleError);
    };

    /**
    * Submit UPDATE for the editedUser
    */
    $scope.submitEdit = function (user) {

      var id = user.id;
      $scope.editedUser.contactNumber = $scope.editedUser.contactNumber.replace(/[^\d\+]/g,"");

        User.update({
            where: { id: id }
        }, {
            firstName: $scope.editedUser.firstName,
            lastName: $scope.editedUser.lastName,
            email: $scope.editedUser.email,
            contactNumber: $scope.editedUser.contactNumber
        }, function (userEdit, userHeaders) {

            if(user.roles.length === 0){
                RoleMapping.create({}, {
                  principalType: "USER",
                  principalId: user.id,
                  roleId: $scope.editedUser.role.id
                }, function(role){
                }, Alert.handleError);
            }
            else{
            RoleMapping.update({
              where: { principalId: id }
                }, {
                  roleId: $scope.editedUser.role.id
                }, function(role){
              }, Alert.handleError);
            }

            Alert.add('success', 'The user was successfully updated');

            $scope.editedUser = {};
            $scope.editedUserId = null;
            getUsers();
        }, Alert.handleError);
    };

    /**
    * Add a team-season relation to the specified user
    */
    $scope.grantTeamSeason = function (user) {
        var modal = $modal.open({
            templateUrl: 'views/partials/modal-select-team-season.tpl.html',
            controller: 'TeamSeasonSelectController'
        });

        modal.result.then(function (selected) {
            User.teamSeasons.link({
                id: user.id,
                fk: selected.id,
            }, {
                roleId: selected.role.id
              }, function (teamSeason) {
                Alert.add('success', 'Access to the selected Team Season has been granted to the user');
                getUsers();
            }, Alert.handleError);
        });
    };

    /**
    * Revoke access to a user's team-season
    */
    $scope.revokeTeamSeason = function (userId, foreignKey) {
        User.teamSeasons.unlink({
            id: userId,
            fk: foreignKey
        }, function (teamSeason) {
            var user = _.find($scope.users, {id: userId});
            _.remove(user.teamSeasons, {id: foreignKey});

            Alert.add('success', 'Access to the selected Team Season has been revoked from the user');
        }, Alert.handleError);
    };

    /**
    * Toggles a form in the appropriate user row
    */
    $scope.toggleEdit = function (user) {

        var userRole = $.grep($scope.ROLES,
          function(e){
            if(user.roles.length === 0){
              return e.id === 2;
            }else{
              return e.id === user.roles[0].id;
            }
        });

        $scope.editedUser = {
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            contactNumber: user.contactNumber,
            role : userRole[0]
        };
        $scope.editedUserId = user.id;
    };

    /**
    * Delete a user instance
    */
    $scope.delete = function (user) {
        var modal = $modal.open({
            templateUrl: 'views/partials/modal-confirm.tpl.html',
            controller: 'ModalConfirm',
            resolve: {
              item: function () {
                var array = [{type: 'User'}, user]
                return array;
              }
           }
        });
        var id = user.id;
        var teamSeasons = user.teamSeasons;

        // Display a confirmation modal
        modal.result.then(function (confirmation) {
            if (confirmation === true) {

              _.each((teamSeasons), function(teamSeason){
                  User.teamSeasons.unlink({
                      id: id,
                      fk: teamSeason.id
                  }, function (teamSeason) {
                      _.remove(user.teamSeasons, {id: teamSeason.id});
                     }, Alert.handleError);
              });

                // Delete all user identities
                User.userIdentities.destroyAll({
                    id: id
                }, function (identity, identHeaders) {
                    // Delete all the user roles
                    User.roles.destroyById({
                        id: id
                    }, function (roles, userHeaders) {
                        // Delete the user
                        User.deleteById({
                            id: id
                        }, function (user, userHeaders) {
                            Alert.add('success', 'The user was successfully deleted');
                            getUsers();
                        }, Alert.handleError);
                    }, Alert.handleError);
                }, Alert.handleError);
            }
        });
    };

    $scope.isEditing = function (id) {
        return ($scope.editedUserId === id);
    };

    $scope.AddingUser = function () {
        $scope.isAddingUser = true;
    };

    $scope.cancelForm = function () {
        $scope.newUser = {};
        $scope.isAddingUser = false;
    };

    $scope.cancelEdit = function () {
        $scope.editedUser = {};
        $scope.editedUserId = null;
    };

}])

.controller('ContactNumberController',
['$scope', 'OAuth', 'User', 'Alert','$state','RoleMapping',
function ($scope, OAuth, User, Alert, $state, RoleMapping) {

    User.getCurrent(function(user) {

      $scope.email = user.email;
      $scope.contactNumber = {
        valid: /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/
      };

        $scope.continue = function () {
            $scope.session.isAuthenticated = true;

            var number = $scope.contactNumber.value;

            if(number === undefined){
                number = "";

                User.update({
                      where: { id: user.id, uuid: user.uuid }
                  }, {
                      contactNumber: number,
                  }, function (user, userHeaders) {
                });

                RoleMapping.create({}, {
                  principalType: "USER",
                  principalId: user.id,
                  roleId: 11
                }, function(role){
                }, Alert.handleError);

                $state.go('teams');
            }
            else{
                number = number.replace(/[^\d\+]/g,"");

                User.update({
                      where: { id: user.id, uuid: user.uuid }
                  }, {
                      contactNumber: number,
                  }, function (user, userHeaders) {
                      Alert.add('success', 'Thanks!  Your contact number was successfully entered.');
                });

                RoleMapping.create({}, {
                  principalType: "USER",
                  principalId: user.id,
                  roleId: 11
                }, function(role){
                }, Alert.handleError);

                $state.go('teams');
          }
        };

        $scope.cancel = function () {
          $scope.session.isAuthenticated = true;
          User.update({
              where: { id: user.id, uuid: user.uuid }
          }, {
              contactNumber: "",
          }, function (user, userHeaders) {
          }, Alert.handleError);

            $state.go('reports');
        };

        $scope.deleteAcc = function () {

            var id = user.id;
            // Delete all user identities
            User.userIdentities.destroyAll({
                id: id
            }, function (identity, identHeaders) {}, Alert.handleError);

            User.deleteById({
                id: id
            }, function (user, userHeaders) {

            }, Alert.handleError);

            $scope.logout = OAuth.logout;

          };
    });
}])

.controller('UserNavigationController',
['$scope', 'OAuth', 'User',
function ($scope, OAuth, User) {

    $scope.logout = OAuth.logout;

}])

.controller('TeamSeasonSelectController',
['$scope', '$modalInstance', 'Team', 'TEAM_SEASON_ROLES',
function ($scope, $modalInstance, Team, TEAM_SEASON_ROLES) {

    $scope.ROLES = TEAM_SEASON_ROLES;
    $scope.selectedRole = {};

    $scope.heading = "Select Team Season";
    $scope.teams = Team.find({
        filter: {
            include: ['teamSeasons']
        }
    });

    $scope.ok = function () {
        if ($scope.selectedTeamSeason) {
            $scope.selectedTeamSeason.role = $scope.selectedRole.role;
            $modalInstance.close($scope.selectedTeamSeason);
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss(false);
    };

    $scope.selectTeam = function (team) {
        $scope.selectedTeam = team;
        $scope.teamSeasons = team.teamSeasons;
    };

    $scope.selectTeamSeason = function (teamSeason) {
        $scope.selectedTeamSeason = teamSeason;
    };
}])

.filter('tel', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (country + " (" + city + ") " + number).trim();
    };
});
