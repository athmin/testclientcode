'use strict';

angular.module('statsGuyControllers.application', [])

.controller('ApplicationController',
['$scope', '$rootScope', '$window', '$state', 'AUTH_EVENTS', 'User', '$q', '$modal', 'TeamSeason', 'Alert', 'SocketIO',
function($scope, $rootScope, $window, $state, AUTH_EVENTS, User, $q, $modal, TeamSeason, Alert, SocketIO) {

    var defaults = {
        session: {
            identity: null,
            role: null,
            isAuthenticated: false
        }
    };

    $scope.copyright = {
      year: new Date().getFullYear()
    };

    $scope.help = {
      login: function () {
        var modal = $modal.open({
            templateUrl: 'views/partials/help/login.tpl.html',
            controller: ['$scope', '$modalInstance', function ($scope, $modalInstance) {
              $scope.cancel = function () {
                  $modalInstance.dismiss('cancel');
              };
            }]
        });
      }
    };

    $scope.session = Object.create(defaults.session);

    function endSession () {
        Alert.closeAll();
        $scope.session = Object.create(defaults.session);

        $state.go('login');
    }

    function loadUserProfile () {

        var deferred = $q.defer();

        User.profile({
            id: User.getCurrentId()
        }, function (data) {
            $scope.session.identity = {
                firstName: data.profile.user.firstName,
                lastName: data.profile.user.lastName
            };
            if (data.profile.roles[0]) {
                $scope.session.role = data.profile.roles[0].name;
            }
            $scope.session.isAuthenticated = true;

            deferred.resolve(data);

            if(data.profile.user.contactNumber === "none"){
              $scope.session.isAuthenticated = false;
            }
        });

        return deferred.promise;
    }

    function checkSyncInProgress(){
      User.teamSeasons({
          id: User.getCurrentId()
      }, function (teamSeasons) {

       var syncYes = $.grep(teamSeasons,
                   function(o,i) { return o.syncInProgress !== 'yes'; },
                   true);

       var syncFailed = $.grep(teamSeasons,
                  function(o,i) { return o.syncInProgress !== 'failed'; },
                  true);

        Alert.closeAll();

        _.each((syncYes), function(teamSeason){
            Alert.persistent('info', 'INFO - A data import for '+teamSeason.name+' is currently underway. The report data for this team will not be accessible until the import has been completed.');
        });

        _.each((syncFailed), function(teamSeason){
            Alert.persistent('warning', 'Warning - The last database sync for '+teamSeason.name+' failed, please try again or contact support.');
        });
      });
    }

    var socket = SocketIO;
    socket.on('syncInProgress', function () {
      function checkAuth(){
        if ( User.isAuthenticated() ) {
            checkSyncInProgress()
        }else{
          setTimeout(function(){checkAuth()}, 1000);
        }
      }
      checkAuth();
    });

    // Bind authentication events to application session
    // Start a session
    $scope.$on(AUTH_EVENTS.loginSuccess, function(event) {
        loadUserProfile().then(function () {
          checkSyncInProgress();

          User.getCurrent(function(user) {
              var contactNumber = user.contactNumber;

              if(contactNumber !== "none"){
                  $state.go('reports');
              }
              else{
                $state.go('account.new');
              }
           });
        });
    });

    // End a session
    $scope.$on(AUTH_EVENTS.logoutSuccess,       endSession);
    $scope.$on(AUTH_EVENTS.notAuthenticated,    endSession);
    $scope.$on(AUTH_EVENTS.sessionTimeout,      endSession);

    // Do a check on application load
    if ( User.isAuthenticated() ) {
        loadUserProfile();
        checkSyncInProgress()
    }

    /**
     * Check browser version to see if user is IE
     */
    $scope.isIE = false;
    (function isIE() {
      var ua = window.navigator.userAgent;
      var msie = ua.indexOf("MSIE ");

      if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        $scope.isIE = true;
      }
    })();

}])

/**
* Login Controller
*/
.controller('LoginController',
['$scope', 'OAuth',
function($scope, OAuth) {

    $scope.login = OAuth.login;

}]);
