'use strict';

angular.module('statsGuyControllers.report', [])

.controller('ReportsController',
['$scope', '$window', '$state', '$timeout', '$filter', 'User', 'ReportingTeam', 'ReportingGame', 'ReportingGamePlayer', 'GameEvent', 'TeamSeason',
function($scope, $window, $state, $timeout, $filter, User, ReportingTeam, ReportingGame, ReportingGamePlayer, GameEvent, TeamSeason) {

    /**
     * Controller scope variables
     */

    // Common UI stuff
    $scope.currentDate = Date.now();
    $scope.sidebarExpanded = true;

    // The values and settings of each reporting filter
    $scope.filters = {
        period:         5,              // 1, 2, 3, 4 or 5(all)
        report:         'report',       // report, events, or faceOff
        display:        'actual',       // actual, percentage
        render:         'players',      // players, or intensityPlot
        specialtyTeam:  'all',          // EvenManpower, PowerPlayManpower, ShortHandManpower, or all
        player:         undefined,      // Player [Object]

        events: {
            Goal:             true,
            Save:             true,
            Miss:             true,
            Blocked:          true,
            'Give-Away':      true,
            'Take-Away':      true,
            'Hit':            true,
            'Fly-By':         true,
            'Defensive Block':true,
            'Battle (+)':     true,
            'Battle (-)':     true,
            'Mid-Lane Drive': true,
            '2ndPuck(+)':     true,
            '2ndPuck(-)':     true,
            'Blueline (+)':   true,
            'Blueline (-)':   true,
            'Pass (+)':       true,
            'Pass (-)':       true,
            'Penalty (taken)':true,
            'Penalty (drawn)':true
        }
    };

    // The indexes of our selected report (team & game)
    $scope.indexes = {
        team: 0,
        games: [],
        HP1: false
    };

    // This is basically the document that the report interface understands
    $scope.report = defaultReport();

    // Anything that gets displayed in the report goes in here
    function defaultReport () {
        return {
            games: [],
            players: [],
            events: [],

            faceoffDots: [],
            faceoffLegend: [0, 0, 0, 0, 0, 0, 0, 0, 0],
            faceoffEvents: [],
            faceoffAggregates: [],

            filtered: {
                faceoffEvents: [],
                events: []
            },

            totals: {
                players: {},
                goalies: {}
            },

            percent: {
                players: [],
                totals: {
                    players: {},
                    goalies: {}
                }
            }
        };
    }

    // An empty event (data point for table totals aggregate)
    function defaultEvent () {
        return {
            period: 0,
            custom: {
                'Give-Away': 0,
                'Take-Away': 0,
                'Hit': 0,
                'Fly-By': 0,
                'Defensive Block': 0,
                'Battle (+)': 0,
                'Battle (-)': 0,
                'Mid-Lane Drive': 0,
                '2ndPuck(+)': 0,
                '2ndPuck(-)': 0,
                'Blueline (+)': 0,
                'Blueline (-)': 0,
                'Pass (+)': 0,
                'Pass (-)': 0,
                'Penalty (taken)': 0,
                'Penalty (drawn)': 0
            },
            faceoff: {
                loss: 0,
                possession: 0,
                tie: 0,
                win: 0,
                total: 0
            },
            plusminus: 0,
            points: {
                assists: 0,
                goal: 0,
                total: 0
            },
            shots: {
                block: 0,
                goal: 0,
                miss: 0,
                shot: 0,
                total: 0
            }
        };
    }

    // An empty faceoff dot (data point for our charts)
    function defaultFaceoffDot () {
        return {
            wins: 0,
            losses: 0,
            ties: 0
        };
    }

    /**
    * Canvas stuff
    */
    var VIEWPORT_WIDTH      = 650,
        VIEWPORT_HEIGHT     = 277,

        POINT_INTENSITY     = 0.6,
        POINT_RADIUS        = 20,
        POINT_BLUR          = 30,

        NUM_FACEOFF_DOTS = 9;

    var PIECHART_CONFIG = {
        segmentShowStroke : true,
        segmentStrokeColor : "#555",
        segmentStrokeWidth : 1,
        animationSteps: 20,
        animateScale: true,
        animationEasing: 'easeOutQuad'

    };

    var DEFAULT_FONT_STYLE = {
        font: '12px Arial',
        fill: 'white',
        stroke: 'black',
        strokeThickness: 2
    };

    var COLORS = {
        faceoffs: {
            tie: '#555555',
            team: {
                wins: '#185ed8',
                losses: '#ffffff'
            },
            opposition: {
                wins: '#5ed025',
                losses: '#ffffff'
            }
        }
    };

    var stage = new PIXI.Stage();
    var renderer = new PIXI.autoDetectRenderer(
        VIEWPORT_WIDTH,
        VIEWPORT_HEIGHT,
        {
            transparent: true,
            view: $window.document.getElementById('renderer')
        }
    );

    var heatmap = simpleheat('heatmap');
    heatmap.radius(POINT_RADIUS, POINT_BLUR);

    // These are the canvases we render our charts on
    var faceoffDisplays = {
        team: [],
        opposition: []
    };

    for (var i = 0; i < NUM_FACEOFF_DOTS; i++) {
        faceoffDisplays.team.push(
            $window.document.getElementById('team-dot-' + (i + 1)).getContext("2d")
        );
        faceoffDisplays.opposition.push(
            $window.document.getElementById('opposition-dot-' + (i + 1)).getContext("2d")
        );
    }

    var sprites = {
        home: {
            Goal:       PIXI.Texture.fromImage('/images/home/bullseye_33cc00_24.png'),
            Save:       PIXI.Texture.fromImage('/images/home/dot-circle-o_33cc00_24.png'),
            Miss:       PIXI.Texture.fromImage('/images/home/circle-o_0c39ca_24.png'),
            Blocked:    PIXI.Texture.fromImage('/images/home/circle_0c39ca_24.png'),
            'Give-Away': PIXI.Texture.fromImage('/images/home/sign-out_e80000_24.png'),
            'Take-Away': PIXI.Texture.fromImage('/images/home/sign-in_33cc00_24.png'),
            'Hit': PIXI.Texture.fromImage('/images/home/star_6b048e_24.png'),
            'Fly-By': PIXI.Texture.fromImage('/images/home/star-o_6b048e_24.png'),
            'Defensive Block': PIXI.Texture.fromImage('/images/home/times-circle_0c39ca_24.png'),
            'Battle (+)': PIXI.Texture.fromImage('/images/home/plus-square_33cc00_24.png'),
            'Battle (-)': PIXI.Texture.fromImage('/images/home/minus-square_e80000_24.png'),
            'Mid-Lane Drive': PIXI.Texture.fromImage('/images/home/reply_ffff00_24.png'),
            '2ndPuck(+)': PIXI.Texture.fromImage('/images/home/search-plus_33cc00_24.png'),
            '2ndPuck(-)': PIXI.Texture.fromImage('/images/home/search-minus_e80000_24.png'),
            'Blueline (+)': PIXI.Texture.fromImage('/images/home/plus_33cc00_24.png'),
            'Blueline (-)': PIXI.Texture.fromImage('/images/home/minus_e80000_24.png'),
            'Pass (+)': PIXI.Texture.fromImage('/images/home/arrows-alt_2fc605_24.png'),
            'Pass (-)': PIXI.Texture.fromImage('/images/home/arrows-alt_df0003_24.png'),
            'Penalty (taken)': PIXI.Texture.fromImage('/images/home/gavel_df0003_24.png'),
            'Penalty (drawn)': PIXI.Texture.fromImage('/images/home/gavel_2fc605_24.png')

        },
        opposition: {
            Goal:       PIXI.Texture.fromImage('/images/opposition/bullseye_b0b0b0_24.png'),
            Save:       PIXI.Texture.fromImage('/images/opposition/dot-circle-o_b0b0b0_24.png'),
            Miss:       PIXI.Texture.fromImage('/images/opposition/circle-o_b0b0b0_24.png'),
            Blocked:    PIXI.Texture.fromImage('/images/opposition/circle_b0b0b0_24.png'),
            'Give-Away': PIXI.Texture.fromImage('/images/opposition/sign-out_b0b0b0_24.png'),
            'Take-Away': PIXI.Texture.fromImage('/images/opposition/sign-in_b0b0b0_24.png'),
            'Hit': PIXI.Texture.fromImage('/images/opposition/star_b0b0b0_24.png'),
            'Fly-By': PIXI.Texture.fromImage('/images/opposition/star-o_b0b0b0_24.png'),
            'Defensive Block': PIXI.Texture.fromImage('/images/opposition/times-circle_b0b0b0_24.png'),
            'Battle (+)': PIXI.Texture.fromImage('/images/opposition/plus-square_b0b0b0_24.png'),
            'Battle (-)': PIXI.Texture.fromImage('/images/opposition/minus-square_b0b0b0_24.png'),
            'Mid-Lane Drive': PIXI.Texture.fromImage('/images/opposition/reply_b0b0b0_24.png'),
            '2ndPuck(+)': PIXI.Texture.fromImage('/images/opposition/search-plus_b0b0b0_24.png'),
            '2ndPuck(-)': PIXI.Texture.fromImage('/images/opposition/search-minus_b0b0b0_24.png'),
            'Blueline (+)': PIXI.Texture.fromImage('/images/opposition/plus_b0b0b0_24.png'),
            'Blueline (-)': PIXI.Texture.fromImage('/images/opposition/minus_b0b0b0_24.png'),
            'Pass (+)': PIXI.Texture.fromImage('/images/opposition/arrows-alt_b0b0b0_24.png'),
            'Pass (-)': PIXI.Texture.fromImage('/images/opposition/arrows-alt_b0b0b0_24.png'),
            'Penalty (taken)': PIXI.Texture.fromImage('/images/opposition/gavel_b0b0b0_24.png'),
            'Penalty (drawn)': PIXI.Texture.fromImage('/images/opposition/gavel_b0b0b0_24.png')
        }
    };

    // Start the render loop
    requestAnimationFrame(frame);

    /**
    * The report's render loop
    */
    function frame () {
        requestAnimationFrame(frame);
        renderer.render(stage);
    }

    /**
     * Update the heatmap canvas view
     */
    function updateHeatmap () {
        var events = $scope.report.filtered.events;

        heatmap.clear();

        for (var i = 0; i < events.length; i++) {
            var position = transposePosition(events[i]);

            heatmap.add([
                Math.floor(position.x * VIEWPORT_WIDTH),
                Math.floor(position.y * VIEWPORT_HEIGHT),
                POINT_INTENSITY
            ]);
        }

        heatmap.draw();
    }

    /**
     * Update the faceoff rink display canvas. Face off dots are numbered 1-9, from top to bottom,
     * ending at the very center of the rink
     */
    function updateFaceoffDisplay() {
      var charts = [];

        for (var i = 0; i < NUM_FACEOFF_DOTS; i++) {
            var team = $scope.report.faceoffAggregates[i].team,
                opposition = $scope.report.faceoffAggregates[i].opposition;

            // Populate the chart data
            var teamChartData = [
                { value: team.wins, color: COLORS.faceoffs.team.wins, label: "Wins" },
                { value: team.ties, color: COLORS.faceoffs.tie, label: "Ties" },
                { value: team.losses, color: COLORS.faceoffs.team.losses, label: "Losses" }
            ];

            var oppositionChartData = [
                { value: opposition.wins, color: COLORS.faceoffs.opposition.wins, label: "Opposition Wins" },
                { value: opposition.losses, color: COLORS.faceoffs.opposition.losses, label: "Opposition Losses" }
            ];

            // Can't resize through HTML style or CSS, so we set size explicitly here
            faceoffDisplays.team[i].canvas.width = $scope.indexes.HP1 ? 120 : 80;
            faceoffDisplays.team[i].canvas.height = $scope.indexes.HP1 ? 120 : 80;

            faceoffDisplays.opposition[i].canvas.height = 120;
            faceoffDisplays.opposition[i].canvas.height = 120;

            // Create the chart objects
            charts.push(new Chart(faceoffDisplays.team[i]).Pie( teamChartData, PIECHART_CONFIG ));
            if (!$scope.indexes.HP1) {
              charts.push(new Chart(faceoffDisplays.opposition[i]).Pie( oppositionChartData, PIECHART_CONFIG ));
            }
        }
    }

    /**
     * Update the standard event viewport canvas
     */
    function updateViewport () {
        var events = $scope.report.filtered.events;

        if ($scope.indexes.HP1) {
          events = _.findByValues(events, 'name', ['Goal', 'Save']);
        }

        stage.removeChildren();

        for (var i = 0; i < events.length; i++) {
            processEventIcon(events[i]);
            processJerseyNumbers(events[i]);
        }
    }

    /**
    *  Process what type of icons players should have displayed on rink
    */
    function processEventIcon(events){
        var sprite,
            selectedTeam = $scope.teams[$scope.indexes.team].uuid;
        // Render home and opposition events using different sets of sprites
        if (events.players.length) {
            for (var i = 0; i < events.players.length; i++) {
                if ((events.players[i].role !== "Goalie") && ((events.players[i].teamUUID === selectedTeam))) {
                    sprite = new PIXI.Sprite(sprites.home[events.name]);
                    break;
                }
                else {
                    sprite = new PIXI.Sprite(sprites.opposition[events.name]);
                }
            }
        }
        else{
            sprite = new PIXI.Sprite(sprites.opposition[events.name]);
        }
        positionRinkElement(events, sprite);
    }

    /**
    *  Process what players should have jersey numbers displayed on rink
    */
    function processJerseyNumbers(events){
        var label,
            selectedTeam = $scope.teams[$scope.indexes.team].uuid;
        // Render the jersey number if the event is associated to a player (who is not a goalie)
        if (events.players.length) {
            for (var i = 0; i < events.players.length; i++) {
                if ((events.players[i].role !== "Goalie") && ((events.players[i].teamUUID === selectedTeam))) {
                    var player = _.find($scope.report.players, { 'name': events.players[i].name }),
                        jerseyNumber;

                    if (player !== undefined) {
                        jerseyNumber = player.jerseynumber;

                        label = new PIXI.Text(jerseyNumber, DEFAULT_FONT_STYLE);
                        positionRinkElement(events, label);
                    }
                }
            }
        }
    }

    /**
    *  Position the Event Icon/Label on the rink
    */
    function positionRinkElement(events, element){
      var position = transposePosition(events);
      element.anchor.x = 0.5;
      element.anchor.y = 0.5;
      element.position.x = Math.floor(position.x * VIEWPORT_WIDTH);
      element.position.y = Math.floor(position.y * VIEWPORT_HEIGHT);
      stage.addChild(element);
    }

    /**
    * Show/hide the filters sidebar
    */
    $scope.toggleSidebar = function () {
        $scope.sidebarExpanded = !$scope.sidebarExpanded;
    };

   /**
    * Filters the games based on what period they occur in
    *
    * @param {[Integer]} period   The game period to filter by (defaults to actual scope value)
    */
    $scope.filterByPeriod = function (period) {
        period = period || $scope.filters.period;

        if (period == 5) {
            $scope.report.filtered.events = $scope.report.events;
            $scope.report.filtered.faceoffEvents = $scope.report.faceoffEvents;
        } else if (period > 0 && period < 5) {
            $scope.report.filtered.events = _.filter($scope.report.filtered.events, {
                'period': period
            });
            $scope.report.filtered.faceoffEvents = _.filter($scope.report.faceoffEvents, {
                'period': period
            });
        }
    };

    /**
    * Filters the report data based on which event type is chosen (Blocked, Goal, etc..)
    */
    $scope.filterByEventType = function () {
        var result = [];

        for (var key in $scope.filters.events) {
            if($scope.filters.events[key] === true) {
                result.push.apply(result, _.filter($scope.report.filtered.events, {
                    'name': key
                }));
            }
        }

        $scope.report.filtered.events = result;
    };

    /**
     * Filters the report data based on which specialty team is chosen (EvenManpower, PowerPlayManpower, etc..)
     */
    $scope.filterBySpecialtyTeam = function () {
        if ($scope.filters.specialtyTeam !== 'all') {
            $scope.report.filtered.events = _.filter($scope.report.filtered.events, {
                'specialtyTeam': $scope.filters.specialtyTeam
            });
            $scope.report.filtered.faceoffEvents = _.filter($scope.report.filtered.faceoffEvents, {
                'specialtyTeam': $scope.filters.specialtyTeam
            });
        }
    };

    /**
     * [filterByPlayer description]
     * @param {[type]} player [description]
     */
    $scope.filterByPlayer = function (player) {
        if (player) {
            $scope.filters.player = player;
            var playerEvents = _.find($scope.report.players, { 'name': player.name }).events;
            var playerFaceoffEvents= _.filter($scope.report.filtered.faceoffEvents, function (event) {
                var foundPlayer = _.find(event.players, { name: player.name });
                return foundPlayer !== undefined;
            });

            if ($scope.filters.period === 5) {
                $scope.report.filtered.events = playerEvents;
                $scope.report.filtered.faceoffEvents = playerFaceoffEvents;
            } else {
                $scope.report.filtered.faceoffEvents = _.filter(playerFaceoffEvents, { 'period': $scope.filters.period });
                $scope.report.filtered.events = _.filter(playerEvents, { 'period': $scope.filters.period });
            }
        }
    };

    $scope.togglePlayerSelection = function (player) {
        if ($scope.filters.player !== player && $scope.filters.player !== undefined) {
            $scope.processFilters({ player: player });
        } else if ($scope.filters.player !== undefined) {
            $scope.filters.player = undefined;
            $scope.processFilters();
        } else {
            $scope.processFilters({ player: player });
        }
    };

    $scope.clearPlayerSelection = function (player) {
        if ($scope.filters.player !== undefined) {
            $scope.filters.player = undefined;
        }
    };

    /**
    * Recalculate the reporting data. This function is used whenever a game selection
    * select option is clicked (i.e. when a user selects a new game or set of games to report on)
    */
    $scope.refreshData = function () {
        $scope.report = defaultReport(); // Reset the document
        var gameUUIDs = [];

        var selector = $scope.indexes.HP1 ? 'select.single-select' : 'select.multi-select';
        $scope.indexes.games = $(selector).val();

        mapGamesToReport(gameUUIDs);
        mapEventsToReport(gameUUIDs);

        $scope.clearPlayerSelection($scope.filters.player);
        $scope.processFilters();
    };

    $scope.$watch('indexes.team', function (value) {
      if ($scope.teams) {
        var teamSeason = _.filter($scope.teamSeasons, { uuid: $scope.teams[value].uuid });
        if (teamSeason.length > 0) {
          $scope.indexes.HP1 = teamSeason[0].appTag === 'HP1' ? true : false;
          $scope.report = defaultReport();
          $scope.indexes.games = [];
        }
      }
    });

    /**
    * Run through all the filters using the passed object, or use the scope values as default
    *
    * @param {[Object]} filters        e.g. {
    *                                  		period: 1,
    *                                  		specialtyTeam: 'EvenStrength',
    *                                  		player: [Object]
    *                                  	}
    */
    $scope.processFilters = function (filters) {
        filters = filters || {};

        var options = {
            period: filters.period || $scope.filters.period,
            specialtyTeam: filters.specialtyTeam || $scope.filters.specialtyTeam,
            player: filters.player || $scope.filters.player
        };

        remap(function () {
            calculateTableTotals();

            $scope.filterByPeriod(options.period);
            $scope.filterByPlayer(options.player);
            $scope.filterBySpecialtyTeam();
            $scope.filterByEventType();

            calculateFaceOffs();
            updateHeatmap();
            updateFaceoffDisplay();
            updateViewport();

            $timeout(function (){
              updateTableRows('table-faceoffs');
              updateTableRows('table-events');
            });

        });
    };

    /**
     * Extracts the player report table into CSV format ready for export
     *
     * @param  {[String]} selector    CSS selector for table containing player data
     * @return {[String]}             A string containing comma-delimited CSV data
     */
    $scope.playerReportToCSV = function (selector) {
      var $table = $(selector);

      // Parse table header
      var headers = ["Player", "Games Played"];

      _.each($( $table.find('thead tr')[1] ).find('th'), function (header) {
          if ($(header).text() !== '') {
              headers.push($(header).text());
          }
      });

      headers.splice(5, 0, "+/-");

      // Parse table body
      var content = [];
      content.push(headers);

      _.each($table.find('tbody tr'), function (row) {
          var $row = $(row);
          var playerName = $( $row.find('td')[0] ).text();

          var contentRow = [];
          contentRow.push(playerName);

          _.each($row.find('td:gt(0)'), function (cell) {
              var $cell = $(cell);

              if ($cell.attr('data') !== undefined) {
                  contentRow.push($cell.attr('data'));
              }
          });

          content.push(contentRow);
      });

      // Remove 'Games Played' column if we're only reporting on one game
      if ($scope.indexes.games.length === 1) {
        _.each(content, function (player, index) {
          player.splice(1, 1);
        });
      }

      // Convert JSON objects to CSV
      var csvData = '';

      _.each(content, function(row, index){
         var line = row.join(",");
         csvData += index < content.length ? line + "\n" : line;
      });

      return csvData;
    }

    /**
     * Extracts the goalie report table into CSV format ready for export
     *
     * @param  {[String]} selector    CSS selector for table containing player data
     * @return {[String]}             A string containing comma-delimited CSV data
     */
    $scope.goalieReportToCSV = function (selector) {
      var $table = $(selector);

      // Parse table header
      var headers = [];

      _.each($( $table.find('thead tr')[0] ).find('th'), function (header) {
          if ($(header).text() !== '') {
              headers.push($(header).text());
          }
      });

      // Parse table body
      var content = [];
      content.push(headers);

      _.each($table.find('tbody tr'), function (row) {
          var $row = $(row);
          var playerName = $( $row.find('td')[0] ).text();

          var contentRow = [];
          contentRow.push(playerName);

          _.each($row.find('td:gt(0)'), function (cell) {
              var $cell = $(cell);

              if ($cell.attr('data') !== undefined) {
                contentRow.push($cell.attr('data'));
              }
          });

          content.push(contentRow);
      });

      // Remove 'Games Played' column if we're only reporting on one game
      if ($scope.indexes.games.length === 1) {
        _.each(content, function (goalie, index) {
          goalie.splice(1, 1);
        });
      }

      // Convert JSON objects to CSV
      var csvData = '';

      _.each(content, function(row, index){
         var line = row.join(",");
         csvData += index < content.length ? line + "\n" : line;
      });

      return csvData;
    }

    /**
     * Exports a CSV (spreadsheet) of the tabular report with a filename
     * similar to '150231 Opponent Name.csv'
     *
     * @param selector [String]         CSS selector for table to be converted
     *                                  into CSV
     */
    $scope.exportCSV = function () {
        var reportDate = $scope.teams[$scope.indexes.team].games[$scope.indexes.games[0]].date,
            filename = $scope.teams[$scope.indexes.team].games[$scope.indexes.games[0]].rival + ' ' +
                        $filter('date')(reportDate, 'yyyy-MM-d hh-mm-ssa') + '.csv';

        var playerReport = $scope.playerReportToCSV('#player-csv-table');
        var goalieReport = $scope.goalieReportToCSV('#goalie-csv-table');

        var csvData = playerReport + '\n\n' + goalieReport;

        var csvFile = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csvData);

        // Trigger a file download
        var hiddenElement = document.createElement('a');
        hiddenElement.href = csvFile;
        hiddenElement.target = '_blank';
        hiddenElement.download = filename;
        var event = document.createEvent("MouseEvents");
          event.initMouseEvent(
                  "click", true, false, window, 0, 0, 0, 0, 0
                  , false, false, false, false, 0, null
          );
        hiddenElement.dispatchEvent(event);
    }

    /**
     * Aggregates faceoff totals for each faceoff dot and updates $scope.report.faceoffDots accordingly.
     * Each index corresponds to its respective faceoff dot ID (1-9)
     */
    function calculateFaceOffs () {
        $scope.report.faceoffDots = [];
        $scope.report.faceoffLegend = [0, 0, 0, 0, 0, 0, 0, 0, 0];

        var dots = _.groupBy($scope.report.filtered.faceoffEvents, function (event) {
            var dot = transposeFaceOff(event);
            return dot;
        });

        for (var i = 1; i <= NUM_FACEOFF_DOTS; i++) {
            if (dots[i]) {
                $scope.report.faceoffDots[i] = _.countBy(dots[i], function (dot) {
                    return dot.name;
                });
            }
        }

        // Aggregate legend labels
        for(var j = 0; j < $scope.report.faceoffDots.length; j++) {
            for (var property in $scope.report.faceoffDots[j + 1]) {
                if ($scope.report.faceoffDots[j + 1].hasOwnProperty(property)) {
                    $scope.report.faceoffLegend[j] = $scope.report.faceoffLegend[j] + $scope.report.faceoffDots[j + 1][property];
                }
            }
        }

        aggregateFaceoffs();
    }

    /**
     * Creates a scope variable with the sums of every column in the report table, as well as
     * the percentage values
     */
    function calculateTableTotals () {
        var aggregated = defaultEvent();
        aggregated.period = $scope.filters.period;

        var players = $filter('filter')($scope.report.players, { position: '!Goalie' }),
            goalies = $filter('filter')($scope.report.players, { position: 'Goalie' });

        var playerPeriods = [], // These arrays will contain every user's period object to later be
            goaliePeriods = []; // aggregated using aggregatePeriods()

        // Calculate player totals
        for (var i = 0; i < players.length; i++) {
            if (players[i].periods !== undefined) {
                var playerIterator = players[i].periods[$scope.filters.period - 1];
                playerPeriods.push(playerIterator);
            }
        }

        // Calculate goalie totals
        for (var j = 0; j < goalies.length; j++) {
            if (goalies[j].periods !== undefined) {
                var goalieIterator = goalies[j].periods[$scope.filters.period - 1];
                goaliePeriods.push(goalieIterator);
            }
        }

        $scope.report.totals.players = aggregatePeriods(playerPeriods);
        $scope.report.percent.totals.players = aggregatePeriods(playerPeriods, true);

        // Eliminate event table columns that have a total value of 0 and hide the corrresponding buttons
        for(var key in $scope.report.totals.players.custom) {
            var value = $scope.report.totals.players.custom[key];
            if((value === 0) ){
              $scope.filters.events[key] = false;
              hideEventButtons(key);
            }
            if((value !== 0)){
              $scope.filters.events[key] = true;
              showEventButtons(key);
            }
        }
        for(var key2 in $scope.report.totals.players.shots) {
            var value2 = $scope.report.totals.players.shots[key2];

            if(key2 === 'block'){key2 = 'Blocked';}
            if(key2 === 'shot'){key2 = 'Save';}
            if(key2 === 'miss'){key2 = 'Miss';}
            if(key2 === 'goal'){key2 = 'Goal';}

            if(value2 === 0){
              $scope.filters.events[key2] = false;
              hideEventButtons(key2);
            }
            if((value2 !== 0)){
              $scope.filters.events[key2] = true;
              showEventButtons(key2);
            }
        }
       // end of code to eliminate event table columns

        $scope.report.totals.goalies = aggregatePeriods(goaliePeriods);
        $scope.report.percent.totals.goalies = aggregatePeriods(goaliePeriods, true);
    }

    /**
     * function to dynamically hide the event buttons
     */
    function hideEventButtons(key){
       var filter1 = 'filters.events.'+key,
           filter2 = 'filters.events['+"'"+key+"'"+']';

       $('.btn-group-filter-rink-events label').each(function(){
         if($(this).attr('ng-model') === filter1 || $(this).attr('ng-model') === filter2){
            $(this).filter(function(){
             return $(this).attr('ng-model') === filter1 || $(this).attr('ng-model') === filter2; }).addClass('ng-hide');
         }
       });
    }

    /**
     * function to dynamically show the event buttons
     */
    function showEventButtons(key){
      $('.btn-group-filter-rink-events').removeClass('ng-hide');

       var filter1 = 'filters.events.'+key,
           filter2 = 'filters.events['+"'"+key+"'"+']';

       $('.btn-group-filter-rink-events label').each(function(){
         if($(this).attr('ng-model') === filter1 || $(this).attr('ng-model') === filter2){
            $(this).filter(function(){
             return $(this).attr('ng-model') === filter1 || $(this).attr('ng-model') === filter2; }).removeClass('ng-hide');

             if($(this).hasClass('ng-dirty') === true && $(this).hasClass('active') === false && $(this).hasClass('ng-valid-parse') === true){
                $(this).filter(function(){
                 $scope.filters.events[key] = false;
                 return $(this).hasClass('ng-dirty') === true && $(this).hasClass('active') === false; }).removeClass('ng-hide');
             }
         }
       });
    }

    /**
     * Hides rows from the events table when all cells are blank
     */
    function updateTableRows(table){
        $("#"+table+"-Message").addClass('ng-hide');
        var rows = $('.'+table+' tbody tr');

        for(var i = 0; i < rows.length - 1; i++) {
            $(rows[i]).removeClass("ng-hide");
            var cells = rows[i].querySelectorAll('td.ng-binding');
            var flag = true;
            for(var j = 1; j < cells.length; j++) {

                 if ((($(cells[j]).html().trim() !== '0') && !$(cells[j]).hasClass("ng-hide")) &&
                      ($(cells[j]).html().trim()  !== '' && !$(cells[j]).hasClass("ng-hide")) &&
                      ($(cells[j]).html().trim()  !== '-' && !$(cells[j]).hasClass("ng-hide")) &&
                      ($(cells[j]).html().trim()  !== '0%' && !$(cells[j]).hasClass("ng-hide"))) {
                     flag = false;
                 }
                 if ($(cells[j]).html() === '' && !$(cells[j]).hasClass("ng-hide")) {
                     $(cells[j]).html('-');
                 }
            }

            if(flag && !$(rows[i]).hasClass('active')) {
                $(rows[i]).addClass('ng-hide');
            }
        }
        $('.'+table).removeClass('ng-hide');

        if ($('.'+ table +' tbody tr.ng-hide').length == rows.length - 1 && rows.length > 0) {
          $('.'+ table).addClass('ng-hide');
          $("#"+table+"-Message").removeClass('ng-hide');
        }
    }

    /**
     * Takes a set of event aggregates and returns a percentage value in decimal form (e.g. 0.15)
     *
     * @param {[Object]} period     Period object with all the aggregates to calculate
     */
    function calculatePercentages (period) {
        var result = {};

        for (var property in period) {
            if (period.hasOwnProperty(property)) {

                if (property === 'shots') {
                    result[property] = {
                        block: period[property].block / period[property].total,
                        shot: period[property].shot / period[property].total,
                        miss: period[property].miss / period[property].total,
                        goal: period[property].goal / period[property].total
                    };
                } else if (property === 'faceoff') {
                    result[property] = {
                        win: period[property].win / period[property].total,
                        loss: period[property].loss / period[property].total,
                        tie: period[property].tie / period[property].total,
                        possession: period[property].possession / period[property].total
                    };
                }

            }
        }

        return result;
    }

    /**
    * Transposes an event's position on the canvas depending on which side of the rink the team
    * played on, as well as the period that's being rendered
    *
    * @param {[Object]} event
    * @param {[Object]} sprite
    *
    * @returns {[Object]}      An object with X and Y coordinates (e.g. { x: 0.54344, y: 0.12345 })
    */
    function transposePosition (event, sprite) {
        var ozoneLeftFlag,
            posX, posY,
            posXAll, posYAll,
            posXTranspose, posYTranspose,
            posXTransposeAll, posYTransposeAll;

        // When multiple games are selected, use the first selected game's Ozone Left Flag to determine
        // the defense/offense zones, and only return the transposed positions
        //
        // Return the regular positions if only one game is selected
        if ($scope.report.games.length > 1) {
            ozoneLeftFlag = $scope.report.games[0].ozoneLeftFlag;
            var isLeftFlagged = (_.find($scope.report.games, { "uuid": event.gameUUID }).ozoneLeftFlag === 1);

            posX                = isLeftFlagged ? event.position.xTranspose : event.position.x;
            posY                = isLeftFlagged ? event.position.yTranspose : event.position.y;
            posXAll             = isLeftFlagged ? event.position.xTransposeAll : event.position.xAll;
            posYAll             = isLeftFlagged ? event.position.yTransposeAll : event.position.yAll;
            posXTranspose       = isLeftFlagged ? event.position.x : event.position.xTranspose;
            posYTranspose       = isLeftFlagged ? event.position.y : event.position.yTranspose;
            posXTransposeAll    = isLeftFlagged ? event.position.xAll : event.position.xTransposeAll;
            posYTransposeAll    = isLeftFlagged ? event.position.yAll : event.position.yTransposeAll;
        } else if ($scope.report.games.length === 1) {
            ozoneLeftFlag = $scope.report.games[0].ozoneLeftFlag;

            posX                = event.position.x;
            posY                = event.position.y;
            posXAll             = event.position.xAll;
            posYAll             = event.position.yAll;
            posXTranspose       = event.position.xTranspose;
            posYTranspose       = event.position.yTranspose;
            posXTransposeAll    = event.position.xTransposeAll;
            posYTransposeAll    = event.position.yTransposeAll;
        }

        // Return a different coordinate depending on the Ozone flag and period selected
        if (ozoneLeftFlag === 0 || (ozoneLeftFlag === 1 && $scope.report.games.length > 1)) {
            $('#rink-legend').attr('src', '/images/rink-legend-ozone-right.png');
            if ($scope.filters.period === 5) {
                return { x: posXAll, y: posYAll };
            } else if ($scope.filters.period === 2) {
                $('#rink-legend').attr('src', '/images/rink-legend-ozone-left.png');
                return { x: posX, y: posY };
            } else {
                return { x: posX, y: posY };
            }
        } else if (ozoneLeftFlag === 1) {
            $('#rink-legend').attr('src', '/images/rink-legend-ozone-left.png');
            if ($scope.filters.period === 5) {
                return { x: posXAll, y: posYAll };
            } else if ($scope.filters.period === 2) {
                $('#rink-legend').attr('src', '/images/rink-legend-ozone-right.png');
                return { x: posX, y: posY };
            } else {
                return { x: posX, y: posY };
            }
        }
    }

    /**
    * Transposes an event's position on the canvas depending on which side of the rink the team
    * played on, as well as the period that's being rendered
    *
    * @param {[Object]} event
    * @param {[Object]} sprite
    *
    * @returns {[Object]}      An object with X and Y coordinates (e.g. { x: 0.54344, y: 0.12345 })
    */
    function transposeFaceOff (event) {
        var ozoneLeftFlag,
            dot, dotAll,
            dotTranspose, dotTransposeAll;

        // When multiple games are selected, use the first selected game's Ozone Left Flag to determine
        // the defense/offense zones, and only return the transposed positions
        //
        // Return the regular positions if only one game is selected
        if ($scope.report.games.length > 1) {
            ozoneLeftFlag = $scope.report.games[0].ozoneLeftFlag;
            var isLeftFlagged = (_.find($scope.report.games, { "uuid": event.gameUUID }).ozoneLeftFlag === 1);

            dot             = isLeftFlagged ? event.faceOffPosition.dotTranspose : event.faceOffPosition.dot;
            dotTranspose    = isLeftFlagged ? event.faceOffPosition.dot : event.faceOffPosition.dotTranspose;
            dotAll          = isLeftFlagged ? event.faceOffPosition.dotTransposeAll : event.faceOffPosition.dotAll;
            dotTransposeAll = isLeftFlagged ? event.faceOffPosition.dotAll : event.faceOffPosition.dotTransposeAll;
        } else if ($scope.report.games.length === 1) {
            ozoneLeftFlag = $scope.report.games[0].ozoneLeftFlag;

            dot                 = event.faceOffPosition.dot;
            dotAll              = event.faceOffPosition.dotAll;
            dotTranspose        = event.faceOffPosition.dotTranspose;
            dotTransposeAll     = event.faceOffPosition.dotTransposeAll;
        }

        // Return a different coordinate depending on the Ozone flag and period selected
        if (ozoneLeftFlag === 0) {
            $('#rink-legend').attr('src', '/images/rink-legend-ozone-right.png');
            if ($scope.filters.period === 5) {
                return dotAll;
            } else if ($scope.filters.period === 2) {
                $('#rink-legend').attr('src', '/images/rink-legend-ozone-left.png');
                return dot;
            } else {
                return dot;
            }
        } else if (ozoneLeftFlag === 1) {
            $('#rink-legend').attr('src', '/images/rink-legend-ozone-left.png');
            if ($scope.filters.period === 5) {
                return dotAll;
            } else if ($scope.filters.period === 2) {
                $('#rink-legend').attr('src', '/images/rink-legend-ozone-right.png');
                return dot;
            } else {
                return dot;
            }
        }
    }

    /**
    * [calculateGames description]
    */
    function mapGamesToReport (gameUUIDs) {
        var games = [];

        if ($scope.indexes.games !== null) {
          if ($scope.indexes.HP1) {
            var game = $scope.teams[$scope.indexes.team].games[$scope.indexes.games];
            var matchedGame = _.find($scope.games, {'uuid': game.uuid});
            if (matchedGame) {
                game.id = matchedGame.id;

                games.push(game);
                gameUUIDs.push(game.uuid);
            }
          } else {
            for(var i = 0; i < $scope.indexes.games.length; i++) {
                var game = $scope.teams[$scope.indexes.team].games[$scope.indexes.games[i]];
                var matchedGame = _.find($scope.games, {'uuid': game.uuid});
                if (matchedGame) {
                    game.id = matchedGame.id;

                    games.push(game);
                    gameUUIDs.push(game.uuid);
                }
            }
          }

          var newGameUUIDs = _.map(games, _.partialRight(_.pick, 'id'));
          $scope.report.gameUUIDs = _.map(newGameUUIDs, function (game) {
              Object.defineProperty(game, 'reportingGameId',
                  Object.getOwnPropertyDescriptor(game, 'id'));
              delete game['id'];

              return game;
          });
          $scope.report.games = games;
        }
    }

    /**
    * [calculateEvents description]
    */
    function mapEventsToReport (gameUUIDs) {
        if ($scope.report.games.length > 0) {
            $scope.report.events = $scope.report.filtered.events = _.filter($scope.events, function (event) {
                return _.contains(gameUUIDs, event.gameUUID);
            });

            $scope.report.faceoffEvents = _.findByValues($scope.report.events, 'name', ['W-L', 'W-W', 'L-L', 'L-W', 'T-L', 'T-W']);
        } else {
            $scope.report.events = [];
            $scope.report.faceoffEvents = [];
        }
    }

    /**
    * Gets a game's players through /api/reporting/teams endpoint and merges in the data retrieved
    * from /api/reporting/games/:id/players
    */
    function remap (callback) {

        var playersByName = {};

        // Refresh the report
        $scope.report.filtered.events = $scope.report.events;
        $scope.report.filtered.faceoffEvents = $scope.report.faceoffEvents;

        // Calculate which players participate in the game (or group of games)
        _.each($scope.report.games, function (game, gameIndex) {
            _.map($scope.report.players, function (player) { playersByName[player.name] = player; });
            _.map(game.players, function (player) {
                playersByName[player.name] = player;
                player.periods = [];
             });

        });

        // This query forms the API request
        var query = {
            filter: {
                where: {
                    or: $scope.report.gameUUIDs
                }
            }
        };

        if($scope.report.gameUUIDs && $scope.report.gameUUIDs.length > 0){

          if ( User.isAuthenticated() ) {

            ReportingGamePlayer.find(query, function (playerData) {
                getPlayerData(playerData);
            });
          }
          else{
            ReportingGamePlayer.public(query, function (playerData) {
                getPlayerData(playerData.result);
            });
          }
        }
        else{
          $("#table-report-Message").addClass('ng-hide');
          stage = new PIXI.Stage();
          for (var i = 0; i < NUM_FACEOFF_DOTS; i++) {
              new Chart(faceoffDisplays.team[i]).Pie( [], PIECHART_CONFIG );
              new Chart(faceoffDisplays.opposition[i]).Pie( [], PIECHART_CONFIG );
          }

          $scope.report = defaultReport();
          $('.table-events').addClass('ng-hide');
          $('.table-faceoffs').addClass('ng-hide');
          $('.btn-group-filter-rink-events').addClass('ng-hide');
          $("#table-events-Message").removeClass('ng-hide');
          $("#table-faceoffs-Message").removeClass('ng-hide');
          $("#table-report-Message").removeClass('ng-hide');
        }
        // Fetch all the player data for the selected games
        function getPlayerData(playerData){
            var sortedPlayerData = _.groupBy(playerData, 'name');
            // Iterate through the indexed object (use player name string as indexes in object instead of array)
            for (var playerName in playersByName) {
                if (sortedPlayerData.hasOwnProperty(playerName)) {
                    var periods             = [],
                        aggregatedPeriods   = [],
                        player              = playersByName[playerName];

                    player.events = [];

                    // Calculate the amount of games played out of the selected games in the report
                    player.gamesPlayed = sortedPlayerData[playerName].length;

                    if (player.gamesPlayed > 1) {
                        for (var j = 0; j < player.gamesPlayed; j++) {

                            // Select periods object depending on which specialty team filter is chosen
                            if ($scope.filters.specialtyTeam === 'all') {
                                periods = sortedPlayerData[playerName][j].periods;
                            } else if($scope.filters.specialtyTeam !== undefined) {
                                if (_.find(sortedPlayerData[playerName][j].specialteams, { 'name' : $scope.filters.specialtyTeam }) !== undefined) {
                                    periods = _.find(sortedPlayerData[playerName][j].specialteams, { 'name' : $scope.filters.specialtyTeam }).periods;
                                }
                            }

                            // If there are no aggregated periods in the array, it means we'll start by adding this game's data to it
                            if (!aggregatedPeriods.length) {
                                aggregatedPeriods = periods;
                            } else {
                                for (var k = 0; k < periods.length; k++) {
                                    var transformed = aggregatePeriods(
                                        [_.find(aggregatedPeriods, { period: periods[k].period}), periods[k]],
                                        false,
                                        periods[k].period
                                    );

                                    var periodIndex = _.findIndex(aggregatedPeriods, { period: periods[k].period });

                                    if (periodIndex >= 0) {
                                        aggregatedPeriods[ _.findIndex(aggregatedPeriods, { period: periods[k].period }) ] = transformed;
                                    } else {
                                        aggregatedPeriods.push(transformed);
                                    }
                                }
                            }

                            player.events = player.events.concat(sortedPlayerData[playerName][j].events);
                        }

                        periods = aggregatedPeriods;
                    } else {
                        // Get the totals from single game
                        if ($scope.filters.specialtyTeam === 'all') {
                            periods = sortedPlayerData[playerName][0].periods;
                        } else if($scope.filters.specialtyTeam !== undefined) {
                            if (_.find(sortedPlayerData[playerName][0].specialteams, { 'name' : $scope.filters.specialtyTeam }) !== undefined) {
                                periods = _.find(sortedPlayerData[playerName][0].specialteams, { 'name' : $scope.filters.specialtyTeam }).periods;
                            }
                        }

                        player.events = sortedPlayerData[playerName][0].events;
                    }

                    // Periods
                    if (player) {
                        player.periods = [];
                        player.periods[0] = _.find(periods, { period: 1 });
                        player.periods[1] = _.find(periods, { period: 2 });
                        player.periods[2] = _.find(periods, { period: 3 });
                        player.periods[3] = _.find(periods, { period: 4 });

                        // Per-period percentages
                        player.percentages = {};
                        player.percentages.periods = [];
                        player.percentages.periods[0] = calculatePercentages(_.find(periods, { period: 1}));
                        player.percentages.periods[1] = calculatePercentages(_.find(periods, { period: 2}));
                        player.percentages.periods[2] = calculatePercentages(_.find(periods, { period: 3}));
                        player.percentages.periods[3] = calculatePercentages(_.find(periods, { period: 4}));

                        // Aggregated 'all' periods
                        player.periods[4] = aggregatePeriods(periods);
                        player.percentages.periods[4] = aggregatePeriods(periods, true);
                    }

                    playersByName[playerName] = player;
                }
            }

            $scope.report.players = _.map(playersByName, function (player) {
                return player;
            });
            callback();
        }
    }

    /**
     * Ideally, this function would iterate through every property and aggregate it regardless of
     * the property name
     *
     * @param {[Array(Objects)]}    periods
     * @param {[Boolean]}           usePercent      Whether to return a value representing a
     *                                              percentage instead (e.g. 0.75)
     */
    function aggregatePeriods (periods, usePercent, periodNumber) {
        var aggregated = defaultEvent(),
            percent = defaultEvent();

        aggregated.period = periodNumber || 5;
        percent.period = periodNumber || 5;

        for (var k = 0; k < periods.length; k++) {
            for (var property in periods[k]) {
                if (periods[k].hasOwnProperty(property)) {
                    if (property === 'shots') {

                      var calculatedTotal = aggregated[property].total + (periods[k][property].total || 0);

                      // Manually substract 'miss/block' event aggregation from totals
                      // since totals are baked into the Database from the ETL layer
                      if ($scope.indexes.HP1) {
                        calculatedTotal -= (periods[k][property].miss || 0);
                        calculatedTotal -= (periods[k][property].block || 0);
                      }

                        aggregated[property] = {
                            block: aggregated[property].block + (periods[k][property].block || 0),
                            shot: aggregated[property].shot + (periods[k][property].shot || 0),
                            miss: aggregated[property].miss + (periods[k][property].miss || 0),
                            goal: aggregated[property].goal + (periods[k][property].goal || 0),
                            total: calculatedTotal
                        };

                        if (usePercent) {
                            percent[property] = {
                                block: aggregated[property].block / aggregated[property].total,
                                shot: aggregated[property].shot / aggregated[property].total,
                                miss: aggregated[property].miss / aggregated[property].total,
                                goal: aggregated[property].goal / aggregated[property].total
                            };
                        }

                    } else if (property === 'points') {

                        aggregated[property] = {
                            assists: aggregated[property].assists + periods[k][property].assists,
                            goal: aggregated[property].goal + periods[k][property].goal,
                            total: aggregated[property].total + periods[k][property].total
                        };

                    } else if (property === 'faceoff') {

                        aggregated[property] = {
                            win: aggregated[property].win + periods[k][property].win,
                            loss: aggregated[property].loss + periods[k][property].loss,
                            tie: aggregated[property].tie + periods[k][property].tie,
                            possession: aggregated[property].possession + periods[k][property].possession,
                            total: aggregated[property].total + periods[k][property].total
                        };

                        if (usePercent) {
                            percent[property] = {
                                win: aggregated[property].win / aggregated[property].total,
                                loss: aggregated[property].loss / aggregated[property].total,
                                tie: aggregated[property].tie / aggregated[property].total,
                                possession: aggregated[property].possession / aggregated[property].total
                            };
                        }

                    } else if (property === 'custom') {

                        aggregated[property]['Give-Away'] = aggregated[property]['Give-Away'] + (periods[k][property]['Give-Away'] || 0);
                        aggregated[property]['Take-Away'] = aggregated[property]['Take-Away'] + (periods[k][property]['Take-Away'] || 0);
                        aggregated[property]['Hit'] = aggregated[property]['Hit'] + (periods[k][property]['Hit'] || 0);
                        aggregated[property]['Fly-By'] = aggregated[property]['Fly-By'] + (periods[k][property]['Fly-By'] || 0);
                        aggregated[property]['Mid-Lane Drive'] = aggregated[property]['Mid-Lane Drive'] + (periods[k][property]['Mid-Lane Drive'] || 0);
                        aggregated[property]['Defensive Block'] = aggregated[property]['Defensive Block'] + (periods[k][property]['Defensive Block'] || 0);
                        aggregated[property]['Battle (+)'] = aggregated[property]['Battle (+)'] + (periods[k][property]['Battle (+)'] || 0);
                        aggregated[property]['Battle (-)'] = aggregated[property]['Battle (-)'] + (periods[k][property]['Battle (-)'] || 0);
                        aggregated[property]['2ndPuck(+)'] = aggregated[property]['2ndPuck(+)'] + (periods[k][property]['2ndPuck(+)'] || 0);
                        aggregated[property]['2ndPuck(-)'] = aggregated[property]['2ndPuck(-)'] + (periods[k][property]['2ndPuck(-)'] || 0);
                        aggregated[property]['Blueline (+)'] = aggregated[property]['Blueline (+)'] + (periods[k][property]['Blueline (+)'] || 0);
                        aggregated[property]['Blueline (-)'] = aggregated[property]['Blueline (-)'] + (periods[k][property]['Blueline (-)'] || 0);
                        aggregated[property]['Pass (+)'] = aggregated[property]['Pass (+)'] + (periods[k][property]['Pass (+)'] || 0);
                        aggregated[property]['Pass (-)'] = aggregated[property]['Pass (-)'] + (periods[k][property]['Pass (-)'] || 0);
                        aggregated[property]['Penalty (taken)'] = aggregated[property]['Penalty (taken)'] + (periods[k][property]['Penalty (taken)'] || 0);
                        aggregated[property]['Penalty (drawn)'] = aggregated[property]['Penalty (drawn)'] + (periods[k][property]['Penalty (drawn)'] || 0);

                    } else if ($.isNumeric(periods[k][property]) && property !== 'period') {

                        aggregated[property] = aggregated[property] + periods[k][property];

                    }
                }
            }
        }

        if (!usePercent) {
            return aggregated;
        } else {
            return percent;
        }
    }

    function aggregateFaceoffs () {
        // Empty faceoff aggregates and legend for dots 1-9
        var aggregates = [],
            numDots = 9;

        for(var i = 0; i <  numDots; i++) {
            aggregates.push({
                team: defaultFaceoffDot(),
                opposition: defaultFaceoffDot()
            });
        }

        // Loop through the events in each dot and check the position of W, L, or T letters to determine
        // which team won, lost, or tied the faceoff
        for (var j = 1; j <= NUM_FACEOFF_DOTS; j++) {

            var dot = $scope.report.faceoffDots[j];

            if (dot) {
                var keys = Object.keys(dot);

                for (var k = 0; k < keys.length; k++) {

                    var teamPoint = keys[k].charAt(0);
                    var oppositionPoint = keys[k].charAt(2);

                    for (var m = 0; m < dot[keys[k]]; m++) {
                        switch (teamPoint) {
                            case 'W':
                                aggregates[j - 1].team.wins++;
                                break;
                            case 'L':
                                aggregates[j - 1].team.losses++;
                                break;
                            case 'T':
                                aggregates[j - 1].team.ties++;
                                break;
                        }

                        switch (oppositionPoint) {
                            case 'W':
                                aggregates[j - 1].opposition.wins++;
                                break;
                            case 'L':
                                aggregates[j - 1].opposition.losses++;
                                break;
                            case 'T':
                                aggregates[j - 1].opposition.ties++;
                                break;
                        }
                    }

                }
            }
        }

        $scope.report.faceoffAggregates = aggregates;
    }

    var socket = io();
    socket.on('syncInProgress', function (teamSeason) {
      User.teamSeasons({
          id: User.getCurrentId()
      }, function (teamSeasons) {

        teamSeasons = $.grep(teamSeasons,
                   function(o,i) { return o.id !== teamSeason.id; },
                   true);

        if(teamSeasons.length !== 0){
          $scope.indexes = {team: 0,games: []};
          $scope.report = defaultReport();
          fetchData(teamSeasons, 'private')
        }
      });
    });

    /**
    * Fetch reporting team and event data based on what the current user has access to
    */
    function fetchData (teamSeasons, access) {

        teamSeasons = $.grep(teamSeasons,
                   function(o,i) { return o.syncInProgress === 'yes'; },
                   true);

        var query = [];

        for (var i = 0; i < teamSeasons.length; i++) {
            query.push({ teamSeasonUUID: teamSeasons[i].uuid });
        }

        if(access === 'private'){
          // Teams
          ReportingTeam.find({ filter: {
              where: {
                  or: query
              }
          }}, function (data) {
                $scope.teams = [];
                $scope.teams = _.sortBy(data, ['name']);
                processTeamData($scope.teams, teamSeasons)
          });

          // Game events
          $scope.events = GameEvent.find({ filter: {
              where: {
                  or: query
              }
          }}, function (data) {
              var isGameSelected = $scope.indexes.games !== null &&
                                typeof $scope.indexes.games !== undefined &&
                                $scope.indexes.games.length !== 0;

              if (isGameSelected) {
                $scope.refreshData();
              }
          });

          // Games
          $scope.games = ReportingGame.find();

        }
        if(access === 'public'){
          // Teams
          ReportingTeam.public({}, function (data) {
                $scope.teams = data.result.teams;
                $scope.events = data.result.events;
                $scope.games = data.result.games;
                processTeamData(data.result.teams, teamSeasons)
          });
        }

        // TeamSeasonName
        $scope.teamSeasons = teamSeasons;

        _.defer(function(){$scope.$apply();});

        function processTeamData(data, teamSeasons){
          for(var i = 0; i < data.length; i++) {
              if (data[i].uuid === data[i].teamSeasonUUID) {
                  $scope.indexes.team = i;
                  $scope.indexes.uuid = data[i].uuid;
              }
              for(var j = 0; j < teamSeasons.length; j++) {
                  if (teamSeasons[j].uuid === data[i].teamSeasonUUID) {
                      data[i].teamSeasonName = (teamSeasons[j].name);
                  }
              }
              var gamesArray = data[i].games;
              gamesArray.sort(function(a,b){
                return new Date(b.date) - new Date(a.date);
              });
           }
        }
    }

    if ( User.isAuthenticated() ) {
      User.teamSeasons({
          id: User.getCurrentId()
      }, function (teamSeasons, err) {
          if (!teamSeasons.length) {
            $state.go('teams');
          } else {
            var sorted = _.sortBy(teamSeasons, ['name']);
            fetchData(sorted, 'private');
          }
      });
    }
    else {

        TeamSeason.public({}, function (result, err) {
            fetchData(result.teamSeasons, 'public');
        });
    }

}]);
