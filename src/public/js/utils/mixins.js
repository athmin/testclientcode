'use strict';

/**
 * Filter a collection based on multiple values for a property
 */
_.mixin({
    'findByValues': function(collection, property, values) {
        return _.filter(collection, function(item) {
            return _.contains(values, item[property]);
        });
    }
});
