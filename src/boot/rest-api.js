'use strict';
var session = require('express-session');

module.exports = function mountRestApi(app) {

    var restApiRoot = app.get('restApiRoot');
    var path = require('path');

    // setup express sessions with mongo storage session
    var mongoSettings = app.datasources.Mongodb.settings;

    app.use(session({
        secret: 'alongithiamsecretpassphrase',
        resave: false,
        saveUninitialized: true
    }));

    app.use(require('connect-busboy')());
    app.use(require('../middleware/log-activity'));
    app.use(path.join(restApiRoot, 'team'), require('../middleware/backup'));
    app.use(path.join(restApiRoot, 'commands'), require('../middleware/commands'));
    app.use(path.join(restApiRoot, 'register'), require('../middleware/register'));
    app.use(path.join(restApiRoot, 'payment'), require('../middleware/payment'));
    app.use(path.join(restApiRoot, 'countries'), require('../middleware/countries'));

    app.use(restApiRoot, app.loopback.rest());
};
