'use strict';

var debug = require('debug')('boot:roles');

module.exports = function Roles (app) {

    var User        = app.models.user,
        Role        = app.models.role,
        RoleMapping = app.models.roleMapping;

    // Ensure we don't create duplicate roles
    Role.validatesUniquenessOf('name');

    /**
     * Create the Administrator role users. We only care about the e-mail since we're doing third-party
     * authentication. Username and password are added as a precaution. Once the user signs in
     * using OAuth, the rest of the instance's properties will be populated
     */
    Role.create({
        id: 1,
        name: 'ISA'
    }, function (err, role) {

        if (err) {
            return debug(err);
        }

        // Add principals to this role
        app.get('administrators').forEach(function (admin) {
            User.create({
                username:   admin.username,
                password:   admin.password,
                email:      admin.email
            }, function (err, user) {
                if (err) {
                    return debug(err);
                }

                role.principals.create({
                    principalType: RoleMapping.USER,
                    principalId: user.id
                }, function (err) {
                    if (err) {
                        return debug(err);
                    }
                });
            });
        });

    });

    // User role
    Role.create({
        id: 11,
        name: 'User'
    }, function (err) {
        if (err) {
            return debug(err);
        }
    });

    // Coach role
    Role.create({
        id: 21,
        name: 'Coach'
    }, function (err) {
        if (err) {
            return debug(err);
        }
    });

    // Statistician role
    Role.create({
        id: 31,
        name: 'Statistician'
    }, function (err) {
        if (err) {
            return debug(err);
        }
    });

    // Owner role
    Role.create({
        id: 41,
        name: 'Owner'
    }, function (err) {
        if (err) {
            return debug(err);
        }
    });
};
