var winston = require('winston');
var _ = ('lodash');

/**
 * Returns an object with version and product information from the client (used
 * for iPad version checking)
 */
function getUserAgentClient (agent) {
  var agent = agent.trim().toLowerCase(),
      parts = agent.split(' ');

  var product, version, isWeb = false;

  if (parts[0] === 'statsguy') {
    // Handle case where user-agent: StatsGuy HPX/XXX (...)
    product = (parts[1].split('/')[0]);
    version = parseInt(parts[1].split('/')[1]);
  } else if (parts[0].split('/').length) {
    // Handle older versions where user-agent: Statsguy/XXX (...)
    if (parts[0].split('/')[0] === 'statsguy') {
      product = 'hp3';
      version = parseInt(parts[0].split('/')[1]);
    } else {
      isWeb = true;
    }
  }

  return {
    isWeb: isWeb,
    version: version,
    product: product
  };
}

/**
 * Checks the user agent for version info. Returns true if check passes.
 *
 * e.g. user-agent: StatsGuy HP1/473 (iPad; iOS 9.1; Scale/1.00)
 */
function validateUserAgent (agent, context) {
  var client = getUserAgentClient(agent);

  if (client.isWeb) {
    return true;
  } else {
    return (client.version >= context.app.get('appVersion')[client.product]);
  }
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * Notes: leave check for version > 439 otherwise the app will return a 409 error
 * and previous versions are not built to handle that response code
 */
function syncInProgress (data, req) {
  var agent = (req.headers['user-agent'] || '').trim().toLowerCase(),
      inProgress = data.syncInProgress === 'yes';

  winston.info('sync in progress check - user agent: %s', agent, req.app.models.User.normalize(req.user));

  //StatsGuy/439 (iPad; iOS 9.1; Scale/1.00)
  var before = '/',
      after = '(ipad',
      start = agent.indexOf(before),
      end = agent.indexOf(after);
  if (start != -1 && end != -1) {
    var version = parseInt(agent.substring(start + before.length, end).trim());
    var canHandle = isNumeric(version) && version > 439;
    inProgress = inProgress && canHandle;
  }
  return inProgress;
}

module.exports = {
  syncInProgress: syncInProgress,
  getUserAgentClient: getUserAgentClient,
  validateUserAgent: validateUserAgent
};
