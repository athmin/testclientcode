'use strict';
var sqlite3 = require('sqlite3'),
    _ = require('lodash');

module.exports = {
    create: function (db) {

        if (_.isObject(db)) {
            return db;
        }
        else {
            return new sqlite3.Database(db);
        }

    }
};