'use strict';

var util = require('util'),
    _ = require('lodash'),
    app = require('../../server');

function Validator () {

}

// validates that there are no possible duplicate gameUUID or gameeventUUID's that exist in the mongo database
// for other team seasons
Validator.prototype.validate = function (db, teamSeasonUUID, done) {

    db.serialize(function () {

        var errors = [];

        db.all('select distinct(ZUUID) from ZGAME', function (err, rows) {
            if (err) {
                return done(err);
            }

            app.models.ReportingGame.find({
                fields: { id: true, uuid: true, teamSeasonUUID: true },
                where: { teamSeasonUUID: { neq: teamSeasonUUID }}
            }, function (e, results) {
                if (e) return done(e);

                var intersection = _.intersection(_.pluck(rows, 'ZUUID'), _.pluck(results, 'uuid'));
                if (intersection.length > 0) {
                    errors.push(new Error(util.format('game uuid(s) found in existing team seasons. %s', JSON.stringify(intersection))));
                }
            });

        });

        db.all('select distinct(ZUUID) as gameEventUUID from ZGAMEEVENT', function (err, rows) {
            if (err) {
                return done(err);
            }

            app.models.GameEvent.find({
                fields: { id: true, teamSeasonUUID: true, uuid: true},
                where: { teamSeasonUUID: { neq: teamSeasonUUID }}
            }, function (e, results) {
                if (e) return done(e);

                var intersection = _.intersection(_.pluck(rows, 'gameEventUUID'), _.pluck(results, 'uuid'));
                if (intersection.length > 0) {
                    errors.push(new Error(util.format('game event uuid(s) found in existing team seasons. %s', JSON.stringify(intersection))));
                }
                done(null, errors);
            });
        });
    });

};

module.exports = new Validator();
