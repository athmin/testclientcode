'use strict';

var util = require('util');

function Validator () {

}

Validator.prototype.validate = function (db, teamSeasonUUID, done) {

    db.serialize(function () {

        var errors = [];

        db.each('select count(*) as uuidcount from ZTEAM where ZPRIMARYTEAM = 1', function (err, row) {
            if (err) {
                return done(err);
            }

            if (row.uuidcount === 0) {
                errors.push(new Error('teamseason uuid not found in database'));
                done(null, errors);
            }

        });

        db.each('select ZUUID from ZTEAM where ZPRIMARYTEAM = 1', function (err, row) {
            if (err) {
                return done(err);
            }

            if (row.ZUUID !== teamSeasonUUID) {
                errors.push(new Error(util.format('teamseason uuid mismatch found in database. expected: %s, actual: %s', teamSeasonUUID, row.ZUUID)));
            }
            done(null, errors);
        });
    });

};

module.exports = new Validator();
