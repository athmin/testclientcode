'use strict';

var _ = require('lodash'),
    connectionFactory = require('./connection'),
    async = require('async'),
    winston = require('winston');

function DatabaseValidations () {
    this.validations = [ require('./teamseason'), require('./game') ];
}

DatabaseValidations.prototype.validate = function (db, teamSeason, normalizedUser, done) {
    var cn = connectionFactory.create(db);

    async.map(this.validations, function (validation, cb) {
        validation.validate(cn, teamSeason, cb);
    }, function (err, results) {

        var flattened = _.flatten(results);
        if (err) {
            winston.error(err.message, normalizedUser);
        }
        else if (flattened.length > 0) {
            flattened.forEach(function (e) {
                winston.error(e.message, normalizedUser);
            });
        }
        cn.close();
        done(err, flattened);
    });
};

module.exports = DatabaseValidations;