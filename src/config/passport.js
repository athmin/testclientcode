'use strict';

var loopbackPassport = require('loopback-component-passport');

module.exports = {

    /**
     * Hooks all our LoopBack models to the passport configuration and fetches
     * passport.json config values
     */
    initialize: function (app) {
        var passportProviders = {};
        var PassportConfigurator = loopbackPassport.PassportConfigurator;
        var passportConfigurator = new PassportConfigurator(app);

        // attempt to load providers configuration
        try {
            passportProviders = require('../passport-providers.json');
        } catch (err) {
            console.trace(err);
            process.exit(1); // fatal
        }

        // init passport after boot (ensuring models exist)
        passportConfigurator.init();
        passportConfigurator.setupModels({
            userModel: app.models.user,
            userIdentityModel: app.models.userIdentity,
            userCredentialModel: app.models.userCredential
        });

        // map and configure passport providers
        for (var i in passportProviders) {
            var provider = passportProviders[i];
            provider.session = provider.session !== false;
            passportConfigurator.configureProvider(i, provider);
        }
    }

};
