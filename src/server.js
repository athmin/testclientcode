'use strict';

var path 			= require('path'),
	loopback 		= require('loopback'),
	boot 			= require('loopback-boot'),
  bodyParser = require('body-parser'),
  compress = require('compression');
var auth 			= require('./config/passport');
var app = module.exports = loopback();

var winston = require('winston');
require('winston-mongodb').MongoDB;
var WORKERS = process.env.WEB_CONCURRENCY || 1;

var io;





// -- Add your pre-processing middleware here --


  app.use(compress());

  // config to make URL and JSON encoded form submissions accessible through req.body
  app.use( bodyParser.json() );
  app.use( bodyParser.urlencoded({
    extended: true
  }));
  boot(app, __dirname, function () {

    // setup the winston mongo logger
    var mongoSettings = app.datasources.Mongodb.settings;
    if (mongoSettings && mongoSettings.url) {
        winston.add(winston.transports.MongoDB, {
            dbUri: mongoSettings.url
        });
        winston.info('setup mongodb logging');
    }

    // set up authentication middleware
    auth.initialize(app);

    // set the application's accessToken (only accessible after boot)
    app.use(loopback.token({
        model: app.models.accessToken
    }));


    // HTML sink (to use as a success and failure redirects for our Passport providers)
    app.use('/loginSuccess', function (req, res) {

				//get user cookie info
				var list = {},
				rc = req.headers.cookie;
				rc && rc.split(';').forEach(function( cookie ) {
						var parts = cookie.split('=');
						list[parts.shift().trim()] = decodeURI(parts.join('='));
				});

				//find user email from the cookie userId
				var User =  app.models.User;
				User.findOne({ where: { id : list.userId }
				}, function(err, user){
					winston.info('successful login to application:', user);
				});

        res.status(200).send();
    });
    app.use('/loginError', function (req, res) {
        winston.error('unsuccessful login to application');
        res.status(401).send();
    });

    app.use( loopback.static(path.join(__dirname, '/public')) );

    if (app.get('env') === 'development') {
        app.use( loopback.static(path.join(__dirname, '../.tmp')) );
    }

    // Requests that get this far won't be handled by any middleware
    // Convert them to a 404 error that will be handled later down the chain
    app.use(loopback.urlNotFound());

    // The ultimate error handler
    app.use(loopback.errorHandler());

    // start the web server
    app.start = function() {

        var server = app.listen(process.env.PORT || app.get('port'), function() {
            app.emit('started');
            winston.info('Web server listening on port: %s', app.get('port'));
        });

				io = require('socket.io')(server);
				module.exports.socketio = io;
				return server;
    };

    // start the server if `$ node server.js`
    if (require.main === module) {
        // app.start();
      app.start();

    }
});
// throng(start);
