'use strict';

var loopback = require('loopback'),
  router = loopback.Router(),
  s3 = require('s3'),
  fs = require('fs'),
  path = require('path'),
  _ = require('lodash'),
  async = require('async'),
    etl = require('../etl'),
    server = require('../server'),
    moment = require('moment'),
    authorize = require('./auth').authorize,
    winston = require('winston'),
    DbValidations = require('../validations').database,
    MiddlewareValidations = require('../validations/middleware'),
    sqlite3 = require('sqlite3'),
    cookieParser = require('cookie-parser'),
    mailer = require('../utils/mailer');

var validations = new DbValidations();
var TeamSeason = server.models.TeamSeason;
var User = server.models.user;

module.exports = router;

function uploadToS3(req, localFile, teamSeason, remoteFile, done, validationError) {

  var bucket = server.get('S3').bucket;
  winston.info('uploading file [%s] to S3 [%s]', localFile, path.join(bucket, teamSeason.toString(), remoteFile), User.normalize(req.user));
  var upload = req.s3.uploadFile({
    localFile: localFile,
    s3Params: {
      Bucket: bucket,
      Key: path.join(teamSeason.toString(), remoteFile)
    }
  });

  upload.on('error', function(err) {
    winston.error('error while uploading to S3: %s', err.message, User.normalize(req.user));
    done(err);
  });

  upload.on('end', function() {
    if (validationError !== true) {
      winston.info('finished uploading %s to S3', localFile, User.normalize(req.user));
      done(null);
    }
  });

}

function notifyAdmin(teamSeasonUUID) {
  var email = {
    from: 'StatsGuy <' + server.get('mailer').auth.user + '>',
    to: server.get('mailer').recipient,
    subject: 'Database upload failed',
    html: '<p>A database push for the teamseason ' + teamSeasonUUID +
      ' failed on ' + new Date().toISOString() + '.</p>'
  };

  mailer.sendMail(email, function(error, data) {
    if (error) {
      next(null, error);
    } else {
      console.info('Email sent: ' + data.response);
    }
  });

}

function processUpload(req, teamSeasonUUID, tmpFile, calculatedFileName, callback) {

  var activities = [];
  activities.push(function(callback) {
    uploadToS3(req, tmpFile, teamSeasonUUID, calculatedFileName, callback, null);
  });

  // validate the database, only run the import if it passes
  validations.validate(tmpFile, teamSeasonUUID, User.normalize(req.user), function(err, results) {
    if (err || results.length > 0) {
      uploadToS3(req, tmpFile, 'error/' + teamSeasonUUID, calculatedFileName, null, true);
      updateTeamSeason(req, teamSeasonUUID, 'syncInProgress', 'failed', true);
      notifyAdmin(teamSeasonUUID);
      return callback(new Error('Error while validating the database, please review the logs.'));
    }
    async.parallel(activities, function(err) {

      etl.process(tmpFile, teamSeasonUUID, req.user, function(err) {

        if (err) {

          updateTeamSeason(req, teamSeasonUUID, 'syncInProgress', 'failed', true);
          notifyAdmin(teamSeasonUUID);

          // upload the file to s3 when there's an error in etl processing
          uploadToS3(req, tmpFile, 'error/' + teamSeasonUUID, calculatedFileName, function(s3err) {
            if (s3err) {
              winston.error('error uploading to s3 after etl err: %s', s3err, User.normalize(req.user));
            }

            fs.unlinkSync(tmpFile);
            callback(new Error('Error while importing the database, please review the logs.'));
          }, true);

        } else {
          updateTeamSeason(req, teamSeasonUUID, 'syncInProgress', 'no', true);

          // TeamSeason.findOne({ where: { uuid: teamSeasonUUID } }, function(err, teamSeason) {
          //   TeamSeason.update({ id: teamSeason.id }, obj, function(err) {
            
          fs.unlinkSync(tmpFile);
        }

      });

      if (err) {
        return callback(err, null);
      }

      callback();
    });
  });
}

function updateTeamSeason(req, teamSeasonUUID, field, value, alert) {
  var io = server.socketio;
  var obj = {};
  obj[field] = value;

  TeamSeason.findOne({ where: { uuid: teamSeasonUUID } }, function(err, teamSeason) {
    TeamSeason.update({ id: teamSeason.id }, obj, function(err) {

      if (err) {
        winston.error('error updating team season: %s', err, User.normalize(req.user));
      } else {

        if (field === 'syncInProgress' && value === 'failed') {
          winston.error('setting teamseason (' + teamSeasonUUID + ') syncInProgress to failed', User.normalize(req.user));
        } else {
          winston.info('setting teamseason (' + teamSeasonUUID + ') ' + field + ' to ' + value);
        }
        if (alert === true) {
          io.emit(field, teamSeason);
        }
      }

    });
  });
}

router.use(cookieParser());
router.use(authorize);

router.use(function(req, res, next) {
  var config = server.get('S3');
  req.s3 = s3.createClient({
    s3Options: {
      accessKeyId: config.key,
      secretAccessKey: config.secret
    }
  });
  next();
});

router.get('/backup/last', function(req, res) {

  function getTeamSeasonKey() {
    // allow for a teamId and seasonId to support the existing ios code
    // ios passes the teamSeason param in the seasonId

    if (req.query.teamId) {
      return req.query.seasonId;
    }

    return req.query.teamSeason;
  }

  winston.info('database requested for teamseason %s', getTeamSeasonKey(), User.normalize(req.user));

  var teamSeason = {};
  var items = req.s3.listObjects({ s3Params: { Bucket: server.get('S3').bucket, Prefix: getTeamSeasonKey() + '/' } });
  items.on('data', function(data) {
    teamSeason =
      _(data.Contents)
      .filter(function(item) {
        return item.Key !== getTeamSeasonKey() + '/';
      })
      .map(function(item) {
        return { Key: item.Key, LastModified: item.LastModified, Size: item.Size };
      })
      .sortBy('LastModified')
      .last();
  });

  items.on('end', function() {

    var agent = req.headers['user-agent'] || '';

    TeamSeason.findOne({
      "uuid": getTeamSeasonKey()
    }, {}, function(err, data) {
      if (data !== null) {

        if (MiddlewareValidations.syncInProgress(data, req)) {
          winston.error('sync already in progress! Cannot initialize pull for teamseason %s', getTeamSeasonKey(), User.normalize(req.user));
          res.sendStatus(409);
        } else if (!MiddlewareValidations.validateUserAgent(agent, req)) {
          winston.error('invalid client version! Cannot initialize pull for teamseason %s', getTeamSeasonKey(), User.normalize(req.user));
          res.sendStatus(403);
        } else {
          if (teamSeason) {

            var download = req.s3.downloadBuffer({
              Bucket: server.get('S3').bucket,
              Key: teamSeason.Key
            });
            download.on('end', function(buf) {
              winston.info('sending database for teamseason %s from S3', teamSeason.Key, User.normalize(req.user));
              res.set('Content-Type', 'application/octet-stream');
              res.attachment('statsguy.sqlite');
              res.send(buf);
            });
          } else {
            winston.error('no database found for teamseason %s in S3', getTeamSeasonKey(), User.normalize(req.user));
            res.sendStatus(404);
          }
        }

      } else {
        winston.error('no database found for teamseason %s in S3', getTeamSeasonKey(), User.normalize(req.user));
        res.sendStatus(404);
      }
    });

  });
});

router.post('/backup', function(req, res, next) {
  var teamSeasonUUID;
  req.busboy.on('field', function(key, value) {
    if (key === 'season') {
      teamSeasonUUID = value;
    }
    if (key === 'teamSeason') {
      teamSeasonUUID = value;
    }

    winston.info('database backup for teamseason %s started', teamSeasonUUID, User.normalize(req.user));
  });

  req.busboy.on('file', function(fieldname, file, filename) {
    if (!fs.existsSync('storage')) {
      fs.mkdirSync('storage');
    }

    var calculatedFileName = moment().format() + '.' + filename;
    var tmpFile = path.join('storage', calculatedFileName);
    var fstream = fs.createWriteStream(tmpFile, { flags: 'w' });

    file.pipe(fstream);

    // Check if a sync is already in progress
    TeamSeason.findOne({ where: { "uuid": teamSeasonUUID } }, {}, function(err, teamSeason) {

      if (MiddlewareValidations.syncInProgress(teamSeason, req)) {
        winston.error('sync already in progress! Cannot initialize backup for teamseason %s', teamSeasonUUID, User.normalize(req.user));

        fstream.on('close', function() {
          res.status(409).end('Another backup is currently in progress.');
        });
      } else if (!MiddlewareValidations.validateUserAgent(req.headers['user-agent'] || '', req)) {
        winston.error('invalid client version! Cannot initialize backup for teamseason %s', teamSeasonUUID, User.normalize(req.user));

        fstream.on('close', function() {
          res.status(403).end('Client version is not supported.');
        });
      } else {
        updateTeamSeason(req, teamSeasonUUID, 'syncInProgress', 'yes', true);

        fstream.on('close', function() {

          processUpload(req, teamSeasonUUID, tmpFile, calculatedFileName, function(err) {
            if (err) {
              winston.error(err.message, User.normalize(req.user));
              res.status(500).end(err.message);
            } else {
              res.status(200).end();
              winston.info('backup finished for teamseason %s', teamSeasonUUID, User.normalize(req.user));
              updateTeamSeason(req, teamSeasonUUID, 'lastSynchronizeDate', moment.utc().format(), false);
            }
          });

        });
      }
    });

  });

  req.pipe(req.busboy);
});

module.exports.newTeamDatabase = function newTeamDatabase(req, res, team, tSeason, season) {

  var config = server.get('S3');
  req.s3 = s3.createClient({ s3Options: { accessKeyId: config.key, secretAccessKey: config.secret } });

  var Sport = 'Meta Library (Hockey)';

  winston.info('new database created from - %s', Sport, User.normalize(req.user));

  var teamSeason = {};
  var items = req.s3.listObjects({ s3Params: { Bucket: server.get('S3').bucket, Prefix: Sport } });

  items.on('data', function(data) {
    teamSeason =
      _(data.Contents)
      .sortBy('LastModified')
      .last();
  });

  items.on('end', function() {

    if (teamSeason) {

      var download = req.s3.downloadBuffer({
        Bucket: server.get('S3').bucket,
        Key: teamSeason.Key
      });

      download.on('end', function(buf) {

        if (!fs.existsSync('storage')) {
          fs.mkdirSync('storage');
        }

        var tmpFile = path.join('storage', 'meta.sqlite');
        var fstream = fs.createWriteStream(tmpFile, { flags: 'w' });

        fstream.write(buf, function() {

          var db = new sqlite3.Database('storage/meta.sqlite');

          db.run('INSERT INTO `ZTEAM` VALUES (1,29,7,1,NULL,NULL,NULL,NULL,NULL,NULL,"' + season.name + '","' + season.endDate + '","' + season.startDate + '","' + team.name + '","' + tSeason.uuid + '");');
          db.run("UPDATE `Z_PRIMARYKEY` SET Z_MAX = 1 WHERE Z_NAME = 'Team';");

          /*for (var i in req.body.data.players) {

              var player = req.body.data.players[i];
              var j = +i + +1;

              db.run('INSERT INTO `ZPERSON` VALUES ('+j+',9,2,NULL,"'+player.firstName+'","'+player.lastName+'","");');
              db.run('INSERT INTO `ZMEMBER` VALUES ('+j+',13,1,"'+player.zJerseyNumber+'",1,'+j+',(SELECT Z_PK FROM ZPOSITION WHERE UPPER(ZPOSITIONNAME) = UPPER("'+player.position+'")),(SELECT Z_PK FROM ZROLE WHERE UPPER(ZROLENAME) = UPPER("'+player.role+'")),NULL,CURRENT_TIMESTAMP);');
              db.run("UPDATE `Z_PRIMARYKEY` SET Z_MAX = (SELECT MAX(Z_PK) FROM ZPERSON) WHERE Z_NAME = 'Person';");
              db.run("UPDATE `Z_PRIMARYKEY` SET Z_MAX = (SELECT MAX(Z_PK) FROM ZMEMBER) WHERE Z_NAME = 'Member';");
          }*/

          db.close();

          db.on('close', function() {

            var calculatedFileName = moment().format() + '.' + tSeason.name + '.sqlite';

            processUpload(req, tSeason.uuid, tmpFile, calculatedFileName, function(err) {

              if (err) {
                winston.error(err.message, User.normalize(req.user));
                res.status(500).end(err.message);
              }
              res.end();
            });
          });
        });
      });
    } else {
      winston.error('could not create new team database for', User.normalize(req.user));
      res.sendStatus(404);
    }

  });
};
