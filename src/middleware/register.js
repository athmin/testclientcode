'use strict';

var loopback = require('loopback'),
    router = loopback.Router(),
    authorize = require('./auth'),
    Validations = require('../validations/middleware'),
    _ = require('lodash'),
    winston = require('winston');

module.exports = router;

function transformUser (req, cb) {
  var user = req.user;

  var agent = req.headers['user-agent'] || '';
  var agentClient = Validations.getUserAgentClient(agent);

  var whereQuery = {};

  // Only return team-seasons associated with the tag passed in the user-agent
  // (e.g. if user-agent contains HP1 or HP3, use that as the appTag filter)
  if (agentClient.product) {
    whereQuery["appTag"] = agentClient.product.toUpperCase();
  }

  user.teamSeasons({
      where: whereQuery,
      include: [{
          relation: 'team'
      },{
          relation: 'season'
      },{
          relation: 'userRoles',
          scope: {
            fields: ['roleId'],
            where: {
              userId: user.id
            },
          },
      }]},function (err, teamSeasons) {

        var person = {
            id: user.id,
            uuid: user.uuid,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName
        };

        var teams = [];
        teamSeasons.forEach(function (teamSeason) {
            var team = teamSeason.team();
            var season = teamSeason.season();

            function mapteam () {

                return {
                    id: team.id,
                    uuid: team.uuid,
                    name: team.name,
                    seasons: [
                        {
                            endDate: season.endDate,
                            id: teamSeason.id,
                            uuid: teamSeason.uuid,
                            seasonDescription: season.name,
                            startDate: season.startDate,
                            dbType: teamSeason.dbType,
                            creationDate: teamSeason.creationDate,
                            expiryDate: teamSeason.expiryDate,
                            effectiveDate: teamSeason.effectiveDate,
                            lastSynchronizeDate: teamSeason.lastSynchronizeDate,
                            syncInProgress: teamSeason.syncInProgress
                        }
                    ]
                };
            }

            function mapseason () {
                return {
                            endDate: season.endDate,
                            id: teamSeason.id,
                            uuid: teamSeason.uuid,
                            seasonDescription: season.name,
                            startDate: season.startDate,
                            dbType: teamSeason.dbType,
                            creationDate: teamSeason.creationDate,
                            expiryDate: teamSeason.expiryDate,
                            effectiveDate: teamSeason.effectiveDate,
                            lastSynchronizeDate: teamSeason.lastSynchronizeDate,
                            syncInProgress: teamSeason.syncInProgress
                };
            }

            if(teamSeason.userRoles()[0].roleId === 31 || teamSeason.userRoles()[0].roleId === 41){
              var found = _.find(teams, {uuid: team.uuid});
              if (found) {
                  found.seasons.push(mapseason());
              }
              else {
                  teams.push(mapteam());
              }
            }

        });

        cb(err, { person: person, teams: teams});

    });
}

router.use(authorize.authorizeGoogle);

router.get('/:provider', function (req, res) {

    transformUser(req, function (err, user) {
        if (err) {
            return res.error(err);
        }

        winston.info('user: %s registered from their iPad.', req.user.email);
        res.send(user);
    });
});
