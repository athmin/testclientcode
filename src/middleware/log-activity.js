var loopback = require('loopback'),
    router = loopback.Router(),
    authorize = require('./auth').authorize,
    winston = require('winston'),
    server = require('../server'),
    User = server.models.user;

router.get('/api/reporting/teams', function(req, res, next) {
  // console.log(req);
  // console.log(res);
  // console.log(next);
  authorize(req, res, function() {
    winston.info('getting teams for reports', User.normalize(req.user));
    next();
  });
  // next();
});

module.exports = router;
