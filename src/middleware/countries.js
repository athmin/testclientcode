'use strict';

var loopback = require('loopback'),
    router = loopback.Router(),
    authorize = require('./auth'),
    _ = require('lodash'),
    winston = require('winston'),
    countries = require('./countries.json');

module.exports = router;

// router.use(authorize);

router.get('/all', function (req, res, next) {

    res.json(countries.map(function (item) {
        return {
            name: item.name,
            alpha2Code: item.alpha2Code,
            alpha3Code: item.alpha3Code
        }
    }));

});

router.get('/CAN/regions', function (req, res, next) {

    var provinces = [
        {name: 'Alberta', alpha2Code: 'AB'},
        {name: 'British Columbia', alpha2Code: 'BC'},
        {name: 'Manitoba', alpha2Code: 'MB'},
        {name: 'New Brunswick', alpha2Code: 'NB'},
        {name: 'Newfoundland and Labrador', alpha2Code: 'NL'},
        {name: 'Nova Scotia', alpha2Code: 'NS'},
        {name: 'Northwest Territories', alpha2Code: 'NT'},
        {name: 'Nunavut', alpha2Code: 'NU'},
        {name: 'Ontario', alpha2Code: 'ON'},
        {name: 'Prince Edward Island', alpha2Code: 'PE'},
        {name: 'Quebec', alpha2Code: 'QC'},
        {name: 'Saskatchewan', alpha2Code: 'SK'},
        {name: 'Yukon', alpha2Code: 'YT'}
    ];

    res.json(provinces);

});