'use strict';

var loopback = require('loopback'),
  authorize = require('./auth').authorize,
  PayPal = require('paypal-ec'),
  winston = require('winston'),
  router = loopback.Router(),
  server = require('../server'),
  backup = require('./backup'),
  _ = require('lodash'),
  url = require('url'),
  cookieParser = require('cookie-parser'),
  models = server.models;

module.exports = router;

router.use(cookieParser());
router.use(authorize);

var cred = server.get('PayPalCred');
var currency = server.get('currency');
var PayPalSandBox = server.get('PayPalSandBox');

var opts = {
  sandbox: PayPalSandBox,
  version: '92.0'
};

var taxTable = [
  {region: 'AB', rate: 0.05, label: 'GST'},
  {region: 'BC', rate: 0.12, label: 'BC GST + PST'},
  {region: 'MB', rate: 0.13, label: 'MB GST + PST'},
  {region: 'NB', rate: 0.13, label: 'NB HST'},
  {region: 'NL', rate: 0.13, label: 'NL HST'},
  {region: 'NT', rate: 0.05, label: 'GST'},
  {region: 'NS', rate: 0.15, label: 'NS HST'},
  {region: 'NU', rate: 0.05, label: 'GST'},
  {region: 'ON', rate: 0.13, label: 'ON HST'},
  {region: 'PE', rate: 0.14, label: 'GST'},
  {region: 'QC', rate: 0.14975, label: 'QC GST + QST'},
  {region: 'SK', rate: 0.10, label: 'SK GST + PST'},
  {region: 'YT', rate: 0.05, label: 'GST'}

];

var paypal = new PayPal(cred, opts);

/**
 * POST /payment/
 *
 * Handle payment for regular subscription purchase
 */
router.post('/', function (req, res, next) {

  models.role.getRoles({principalType: models.roleMapping.USER, principalId: req.user.id}, function(err, roles) {

    if (req.body.demo === 'on') {

      // Create demo team-season
      winston.info('Creating demo TeamSeason: %s', req.body);

      models.Season.findOne({where: {id: req.body.season}}, {}, function (err, season) {
        models.Team.create(req.body, {}, function (err, team) {
          models.TeamSeason.create({
            seasonId: req.body.season,
            teamId: team.id,
            dbType: 'demo'
          }, {}, function (err, teamSeason) {
            models.TeamSeason.scopes.roles.modelTo.create({
              teamSeasonId: teamSeason.id,
              userId: req.user.id,
              roleId: 41
            });
            backup.newTeamDatabase(req, res, team, teamSeason, season);
          });
        });
      });

      res.redirect('/#/subscriptions');

    } else {

      // Redirect to payment page
      models.Season.findOne({where: {id: req.body.season}}, {}, function (err, season) {

        var discountedPrice = req.app.models.Season.applyDiscounts(season);

        var host = req.headers.origin || req.headers.referer;
        var ctx = req.session;
        var params = {
          returnUrl: url.resolve(host, '/api/payment/confirm'),
          cancelUrl: url.resolve(host, '/api/payment/cancel'),
          SOLUTIONTYPE: 'sole',
          PAYMENTREQUEST_0_AMT: discountedPrice,
          PAYMENTREQUEST_0_ITEMAMT: discountedPrice,
          PAYMENTREQUEST_0_DESC: 'One-time Purchase',
          PAYMENTREQUEST_0_CURRENCYCODE: currency,
          PAYMENTREQUEST_0_PAYMENTACTION: 'Sale',
          L_PAYMENTREQUEST_0_ITEMCATEGORY0: 'Digital',
          L_PAYMENTREQUEST_0_NAME0: req.body.name + ' (' + season.name + ')',
          L_PAYMENTREQUEST_0_AMT0: discountedPrice,
          L_PAYMENTREQUEST_0_QTY0: '1'
        };

        // adjust the params for taxes
        if (req.body.country === 'CAN') {
          var tax = _.find(taxTable, function (item) {
            return item.region === req.body.region;
          });

          var taxAmount = Number((discountedPrice * tax.rate).toFixed(2));

          params.PAYMENTREQUEST_0_AMT = Number(discountedPrice) + taxAmount;
          params.PAYMENTREQUEST_0_TAXAMT = taxAmount;
          params.L_PAYMENTREQUEST_0_TAXAMT0 = taxAmount;
        }

        winston.info('request to create team %s', JSON.stringify(req.body));

        params.PAYMENTREQUEST_0_CUSTOM = JSON.stringify(req.body);
        ctx.paypalParams = params;

        paypal.set(params, function (err, data) {
          if (err) {
            console.dir(params);
            return next(err);
          }

          res.redirect(data.PAYMENTURL);
        });
      });

    }

  });
});

router.get('/confirm', function (req, res, next) {

  var params = req.session.paypalParams;
  params.TOKEN = req.query.token;
  params.PAYERID = req.query.PayerID;

  paypal.do_payment(params, function (err, data) {
    if (err) return next(err);

    paypal.get_details({token: params.TOKEN}, function (err, data) {
      if (err) return next(err);

      winston.info('PayPal success transaction id: %s, teamseason %s', data.PAYMENTREQUEST_0_TRANSACTIONID, data.PAYMENTREQUEST_0_CUSTOM);

      var newTeam = JSON.parse(data.PAYMENTREQUEST_0_CUSTOM);

      models.Season.findOne({where: {id: newTeam.season}}, {}, function (err, season) {
        models.Team.create(newTeam, {}, function (err, team) {
          models.TeamSeason.create({
            seasonId: newTeam.season,
            teamId: team.id
          }, {}, function (err, teamSeason) {
            models.TeamSeason.scopes.roles.modelTo.create({
              teamSeasonId: teamSeason.id,
              userId: req.user.id,
              roleId: 41
            });
            backup.newTeamDatabase(req, res, team, teamSeason, season);
          });
        });
      });

      res.redirect('/#/subscriptions');

    });
  });
});

router.get('/cancel', function (req, res, next) {
  paypal.get_details({token: req.query.token}, function (err, data) {
    if (err) return next(err);

    winston.log('PayPal transaction cancellation: %s, teamseason %s', data.PAYMENTREQUEST_0_TRANSACTIONID, data.PAYMENTREQUEST_0_CUSTOM);
  });

  res.redirect('/#/subscriptions');

});

/**
 * POST /payment/upgrade
 *
 * Process payment for a Free Trial upgrade and redirect the user to the PayPal
 * payment screen.
 */
router.post('/upgrade', function (req, res, next) {

  // Validate the request
  if (!req.body.teamSeason) {
    return res.status(403).json({ message: 'teamSeason field can\'t be empty' });
  } else if (!req.body.country) {
    return res.status(403).json({ message: 'country field can\'t be empty' });
  }
  if (req.body.country === 'CAN' && !req.body.region) {
    return res.status(403).json({ message: 'region field can\'t be empty' });
  }

  winston.info('Upgrading demo TeamSeason: %s', req.body.teamSeason);

  models.TeamSeason.findById(req.body.teamSeason, {}, function (error, teamSeason) {
    if (error) return next(error);

    models.Team.findById(teamSeason.teamId, {}, function (error, team) {
      if (error) return next(error);

      models.Season.findById(teamSeason.seasonId, {}, function (error, season) {
        if (error) return next(error);

        var discountedPrice = req.app.models.Season.applyDiscounts(season);

        var host = req.headers.origin || req.headers.referer;
        var ctx = req.session;
        var params = {
          returnUrl: url.resolve(host, '/api/payment/upgrade/confirm'),
          cancelUrl: url.resolve(host, '/api/payment/cancel'),
          SOLUTIONTYPE: 'sole',
          PAYMENTREQUEST_0_AMT: discountedPrice,
          PAYMENTREQUEST_0_ITEMAMT: discountedPrice,
          PAYMENTREQUEST_0_DESC: 'One-time Purchase',
          PAYMENTREQUEST_0_CURRENCYCODE: currency,
          PAYMENTREQUEST_0_PAYMENTACTION: 'Sale',
          L_PAYMENTREQUEST_0_ITEMCATEGORY0: 'Digital',
          L_PAYMENTREQUEST_0_NAME0: team.name + ' (' + season.name + ')',
          L_PAYMENTREQUEST_0_AMT0: discountedPrice,
          L_PAYMENTREQUEST_0_QTY0: '1'
        };

        // adjust the params for taxes
        if (req.body.country === 'CAN') {
          var tax = _.find(taxTable, function (item) {
            return item.region === req.body.region;
          });

          var taxAmount = Number((discountedPrice * tax.rate).toFixed(2));

          params.PAYMENTREQUEST_0_AMT = Number(discountedPrice) + taxAmount;
          params.PAYMENTREQUEST_0_TAXAMT = taxAmount;
          params.L_PAYMENTREQUEST_0_TAXAMT0 = taxAmount;
        }

        params.PAYMENTREQUEST_0_CUSTOM = teamSeason.id;
        ctx.paypalParams = params;

        paypal.set(params, function (error, data) {
          if (error) return next(error);
          res.redirect(data.PAYMENTURL);
        });
      });
    });
  });

});

/**
 * GET /upgrade/confirm
 *
 * Ensures that the PayPal transaction was successful before updating the
 * team-season model instance and redirecting the user
 */
router.get('/upgrade/confirm', function (req, res, next) {

  var params = req.session.paypalParams;

  params.TOKEN = req.query.token;
  params.PAYERID = req.query.PayerID;

  paypal.do_payment(params, function (error, data) {
    if (error) return next(error);

    paypal.get_details({ token: params.TOKEN }, function (error, data) {
      if (error) return next(error);

      winston.info('PayPal success upgrade transaction id: %s, teamseason %s', data.PAYMENTREQUEST_0_TRANSACTIONID, data.PAYMENTREQUEST_0_CUSTOM);

      var teamSeasonId = data.PAYMENTREQUEST_0_CUSTOM;

      models.TeamSeason.findById(teamSeasonId, {}, function (error, teamSeason) {
        if (error) return next(error);

        models.Season.findById(teamSeason.seasonId, {}, function (error, season) {
          if (error) return next(error);

          models.TeamSeason.upsert({
            id: teamSeason.id,
            dbType: 'real',
            effectiveDate: season.startDate > new Date() ? season.startDate : new Date(),
            expiryDate: season.endDate
          }, function (error, teamSeason) {
            if (error) return next(error);

            winston.info('Upgraded teamseason %s to \'real\'', data.PAYMENTREQUEST_0_CUSTOM);
            return res.redirect('/#/subscriptions');
          });
        });
      });
    });
  });

});
