'use strict';

var https = require('https'),
    _ = require('lodash'),
    server = require('../server'),
    models = server.models,
    winston = require('winston');

function authorizeInternal (req, res, next) {

    // load the user associated with the authorization token and validate it
    models.accessToken.findById(req.get('authorization') || req.cookies.access_token, function (err, token) {

        if (err) {
            winston.error(err);
            return res.status(500).end();
        }

        models.User.findById(token.userId, function (err, user) {
            req.user = user;
            winston.info('user identity for %s validated against internal database', user.email);
            return next();
        });

    });

}

function authorizeGoogle (req, res, next) {

    https.get('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' + req.get('X-AUTH-TOKEN'), function (googleRes) {

        googleRes.on('error', function (e) {
            winston.error(e);
            res.error(e);
        });

        googleRes.on('data', function (d) {

            var resp = JSON.parse(d.toString());
            // validate the token against the issuer and user
            if (_.contains(server.get('allowedGoogleClients'), resp.issued_to) &&
                resp.verified_email === true &&
                resp.expires_in > 0) {

                models.userIdentity.findOne({where: {externalId: resp.user_id}, include: {user: [{teamSeasons: ['season', 'team']}]}}, function (err, userIdentity) {

                    if (err) {
                        winston.error(err);
                        res.status(500).end();
                    }

                    if (! userIdentity) {
                        winston.error('Attempt to login with invalid user.');
                        res.status(401).end();
                    }
                    else {

                        userIdentity.user(function (err, user) {

                            winston.info('user identity for %s verified by google', user.email);
                            req.user = user;
                            return next();

                        });

                    }

                });

            }
            else {
                res.status(401).end();
            }

        });

    });
}


// check the authorization headers against g+
function authorize (req, res, next) {

    if (req.user) {
        return next();
    }

    if (req.get('X-AUTH-PROVIDER') !== 'google') {
        authorizeInternal(req, res, next);
    }
    else {
        authorizeGoogle(req, res, next);
    }
}

module.exports = {
    authorize: authorize,
    authorizeGoogle: authorizeGoogle
};