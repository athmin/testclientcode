'use strict';

var loopback = require('loopback'),
    router = loopback.Router(),
    authorize = require('./auth'),
    mailer      = require('../utils/mailer'),
    server      = require('../server'),
    winston     = require('winston');


    function NewUserRequestCommandHandler (mmand) {
    this.command = command;
}

// validates the request matches:
/*
    {
        firstname: "String",
        lastname: "String",
        requestedBy: {
            user: "String",
            team: "String",
            season: "String"
        }
    }
 */
NewUserRequestCommandHandler.prototype.validate = function () {

    try {
        var b = this.command;
        var pattern = /^(?!\s*$).+/;
        return (pattern.test(b.firstname) &&
                pattern.test(b.lastname) &&
                pattern.test(b.requestedBy.user) &&
                pattern.test(b.requestedBy.team) &&
                pattern.test(b.requestedBy.season));
    }
    catch (e) {
        return false;
    }

};

module.exports = router;

router.use(authorize.authorizeGoogle);

router.post('/newuserrequest', function (req, res) {

    var handler = new NewUserRequestCommandHandler(req.body);
    if (! handler.validate()) {
        res.status(400);
        return res.end('Bad command body.');
    }

    // send mail
    var email = {
        from: 'StatsGuy <' + server.get('mailer').auth.user + '>',
        to: server.get('mailer').recipient,
        subject: 'New User Account Requested',
        html: '<p>A new user account was requested:</p><br/>' +
        '<p>firstname: ' + req.body.firstname + '</p>' +
        '<p>lastname: ' + req.body.lastname + '</p>' +
        '<p>Requested By:</p>' +
        '<p>user: ' + req.body.requestedBy.user + '</p>' +
        '<p>team: ' + req.body.requestedBy.team + '</p>' +
        '<p>season: ' + req.body.requestedBy.season + '</p>'
    };

    mailer.sendMail(email, function(error, data){
        if (error) {
            winston.error(error);
        } else {
            winston.info('Email sent: ' + data.response);
        }
    });

    res.status(200);
    res.end();

});