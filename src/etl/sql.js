'use strict';

var fs = require('fs'),
    path = require('path');


function loadResource (name) {

    return fs.readFileSync(path.join(__dirname, 'resources', name)).toString();

}

module.exports = {
    teams: function () {
        return loadResource('Teams.sql');
    },
    teamGames: function () {
        return loadResource('Teams-Games.sql');
    },
    teamGamePlayers: function () {
        return loadResource('Teams-Games-Players.sql');
    },
    gameEvents: function () {
        return loadResource('GameEvents.sql');
    },
    gameEventsPlayers: function () {
        return loadResource('GameEvents-Players.sql');
    },
    associatedGameEvents: function () {
        return loadResource('GameEvents-AssociatedGameEvents.sql');
    }
};