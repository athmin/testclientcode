'use strict';

var sqlite = require('sqlite3'),
    mapping = require('./mapping'),
    _ = require('lodash'),
    server = require('../server'),
    sql = require('./sql'),
    aggregate = require('./aggregates'),
    async = require('async'),
    winston = require('winston');

var teamMapper = mapping.create('ReportingTeam');
var gameMapper = mapping.create('ReportingTeamGame');
var playerMapper = mapping.create('ReportingTeamPlayer');

var gameEventMapper = mapping.create('GameEvent');
var gameEventPlayerMapper = mapping.create('GameEventPlayer');
var associatedEventMapper = mapping.create('AssociatedEvent');
var User = server.models.user;

module.exports = {
    process: function (localFile, teamSeason, user, done) {


        // remove existing teamseason data
        async.applyEach([
            _.bind(server.models.ReportingTeam.destroyAll, server.models.ReportingTeam),
            _.bind(server.models.GameEvent.destroyAll, server.models.GameEvent),
            _.bind(server.models.ReportingGame.destroyAll, server.models.ReportingGame),
            _.bind(server.models.ReportingGamePlayer.destroyAll, server.models.ReportingGamePlayer)
        ], {teamSeasonUUID: teamSeason}, function (err) {

            if (err) {
                return done(err);
            }

            var errors = 0;

            var db = new sqlite.Database(localFile);
            db.serialize(function () {

                var teams = [];
                var events = [];

                // select teams
                db.each(sql.teams(), function (err, row) {
                    if (err) {
                        return err;
                    }

                    teams.push(teamMapper.map(row));
                });

                // select team games
                db.each(sql.teamGames(), function (err, row) {
                    if (err) {
                        return err;
                    }

                    var team = _.find(teams, {uuid: row.TEAM_UUID});

                    if (team === undefined) {
                        winston.error('Error! Failed to process team row (1) :', row, User.normalize(user));
                        errors++;
                    } else {
                        team.games.push(gameMapper.map(row));
                    }
                });

                // select players for games
                db.each(sql.teamGamePlayers(), function (err, row) {

                    if (err) {
                      console.log(err);
                        return err;
                    }

                    var team = _.find(teams, {uuid: row.TEAM_UUID});

                    if (team === undefined) {
                        winston.error('Error! Failed to process team row (2) :', row, User.normalize(user));
                        errors++;
                    } else {
                        var game = _.find(team.games, {uuid: row.GAME_UUID});
                        if (game === undefined) {
                            winston.error('Error! Failed to process team row (3) :', row, User.normalize(user));
                            errors++;
                        } else {
                            game.players.push(playerMapper.map(row));
                        }
                    }

                }, function (err) {
                    if (err) {
                        return done(err);
                    }

                    async.applyEach([_.bind(server.models.ReportingTeam.create, server.models.ReportingTeam)], teams, function (err) {
                        if (err) {
                            return done(err);
                        }

                        db.serialize(function () {
                            db.each(sql.gameEvents(), function (err, row) {
                                if (err) {
                                    return err;
                                }

                                events.push(gameEventMapper.map(row));

                            });

                            db.each(sql.gameEventsPlayers(), function (err, row) {
                                if (err) {
                                    return err;
                                }

                                var event = _.find(events, {uuid: row.GAMEEVENT_UUID});
                                if (event === undefined) {
                                    winston.error('Error! Failed to process event row:', row, User.normalize(user));
                                    errors++;
                                }
                                else {
                                    event.players.push(gameEventPlayerMapper.map(row));
                                }

                            });
                            db.each(sql.associatedGameEvents(), function (err, row) {
                                if (err) {
                                    return err;
                                }

                                var event = _.find(events, {uuid: row.GAMEEVENT_UUID});

                                if (event === undefined) {
                                    winston.error('Error! Failed to process event row', row, User.normalize(user));
                                    errors++;
                                } else {
                                    event.associatedEvents.push(associatedEventMapper.map(row));
                                    // map player of associated event

                                }

                            }, function (err) {
                                if (err) {
                                    return done(err);
                                }

                                if (errors > 0) {
                                    db.close();
                                    return done(new Error(errors + ' Errors found during import'));
                                }
                                aggregate.init();

                                async.eachSeries(events, function (event, cb)  {
                                        async.applyEach([_.bind(server.models.GameEvent.create, server.models.GameEvent), aggregate.process], event, cb);
                                }, function () {

                                    aggregate.save(function () {
                                        db.close();
                                        done(null);
                                    });

                                });

                            });
                        });
                    });

                });
            });

        });

    }
};
