'use strict';
var async = require('async'),
    models = require('../../server').models,
    _ = require('lodash');

var fns = [];
var cache = {};

function ensureGame (row, cb) {

    var cached = cache[row.gameUUID];
    if (cached) {
        return cb(null, cached.game);
    }

    models.ReportingGame.findOrCreate({where :{uuid: row.gameUUID}}, new models.ReportingGame({uuid: row.gameUUID, teamSeasonUUID: row.teamSeasonUUID, players: []}), function (err, game) {
        if (err) { return cb(err); }
        cache[row.gameUUID] = {};
        cache[row.gameUUID].game = game;
        cache[row.gameUUID].players = [];
        cb(null, game);
    });
}

function createPlayers (game, row, cb) {

    async.eachSeries(row.players, function (player, callback) {

        var cached = _.find(cache[row.gameUUID].players, {refId: player.refId});
        if (cached) {
            return callback(null, player);
        }

        models.ReportingGamePlayer.findOrCreate({where: {reportingGameId: game.id, refId: player.refId}}, {reportingGameId: game.id, refId: player.refId, personUUID: player.personUUID, teamSeasonUUID: row.teamSeasonUUID, name: player.name, jerseynumber: player.jerseynumber, role: player.role, periods: [], specialteams: []}, function (err, player) {
            if (err) { return callback(err); }
            cache[row.gameUUID].players.push(player);
            callback(null, player);
        });
    }, cb);

}

function createAssociatedPlayers (realGame, row, cb) {

    async.eachSeries(row.associatedEvents, function (associatedEvent, callback) {

        var cached = _.find(cache[row.gameUUID].players, {refId: associatedEvent.playerRefId});
        if (cached || associatedEvent.playerRefId == null) {
            return callback(null, cached);
        }

        models.ReportingTeam.findOne({where: {uuid: associatedEvent.teamUUID}}, function (err, team) {

            if (err) {
                return callback(err);
            }

            var game = _.find(team.games, function (game) {
                return game.uuid === row.gameUUID;
            });

            var player = _.find(game.players, function (player) {
                return player.refId === associatedEvent.playerRefId;
            });

            // no player found means bad data in an associated event?
            if (! player) {
                return callback(null, null);
            }

            models.ReportingGamePlayer.create({reportingGameId: realGame.id, refId: player.refId, personUUID: player.personUUID, teamSeasonUUID: row.teamSeasonUUID, name: player.name, jerseynumber: player.jerseynumber, role: player.role, periods: [], specialteams: []}, function (err, createdPlayer) {
                if (err) { return callback(err); }
                cache[row.gameUUID].players.push(createdPlayer);
                callback(null, createdPlayer);
            });
        });

    }, cb);

}

function ensureGameAndPlayer (row, cb) {

    // TODO: this code is missing players in associated events if they happen to occur for the first time in the associated event.
    ensureGame(row, function (err, game) {

        async.series([
            function (callback) {
                createPlayers(game, row, callback);
            },
            function (callback) {
                createAssociatedPlayers (game, row, callback);
            }
        ], cb);
    });
}

function shouldProcessEvent(event, fn) {

    if (_.isArray(fn.matchEvent)) {
        return _.contains(fn.matchEvent, event.name);
    }
    else if (_.isFunction(fn.matchEvent)) {
        return fn.matchEvent(event);
    }
    return false;
}

function aggregateEvent (event, fn) {

    if (shouldProcessEvent(event, fn)) {

        var players = _.where(cache[event.gameUUID].players, function (player) {
            return (fn.playerSelector) ?  fn.playerSelector(event, player) : _.any(event.players, {refId: player.refId});
        });

        // logging around potentially missing players
        if (players.length == 0 && event.playerRefId && (fn.playerSelector || event.players.length != 0)) {
            console.error('event %s had a player referenced that could not be found', JSON.stringify(event));
        }
        players.forEach(function (player) {
            fn.aggregate(event, player);
        });

    }

}

module.exports = {

    process: function (row, cb) {

        ensureGameAndPlayer(row, function (err) {
            if (err) { return cb(err); }
            fns.forEach(function (fn) {
                aggregateEvent(row, fn);
            });

            if (row.associatedEvents) {
                _(row.associatedEvents).map(function (ae) {
                    return {
                        name: ae.name,
                        gameUUID: row.gameUUID,
                        playerRefId: ae.playerRefId,
                        period: row.period,
                        players: []
                    };
                }).each(function (ae) {
                    fns.forEach(function (fn) {
                        aggregateEvent(ae, fn);
                    });
                });
            }

            cb();
        });
    },
    register: function (fn) {
        fns.push(fn);
    },
    init: function () {
        cache = {};
    },
    save: function (cb) {
        var players = _(cache).map(function (item) { return item.players;}).flatten().value();
        async.each(players, function (player, callback) {
            player.save(callback);
        }, cb);

    }

};

require('./functions/points');
require('./functions/faceoffs');
require('./functions/shots');
require('./functions/custom');
require('./functions/rawevent');
