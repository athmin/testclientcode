'use strict';

var aggregate = require('../'),
    _ = require('lodash');

var rawEvent = {

    matchEvent: function () {
        return true;
    },
    aggregate: function (event, player) {

        if (! player.events) {
            player.events = [];
        }

        var cloned = _.clone(event);
        delete cloned.playerRefId;
        delete cloned.associatedEvents;

        player.events.push(cloned);
    }
};

aggregate.register(rawEvent);
