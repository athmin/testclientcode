'use strict';

var aggregate = require('../');

var assist = {

    matchEvent: ['Assist', 'ASSIST1', 'ASSIST2'],
    aggregate: function (event, player) {
        var points = player.period(event.period).points;
        points.assists++;
        points.total++;
        if (event.specialtyTeam) {
            var specialTeamPoints = player.specialteamperiod(event.specialtyTeam, event.period).points;
            specialTeamPoints.assists++;
            specialTeamPoints.total++;
        }
    },
    playerSelector: function (event, player) {
        return event.playerRefId === player.refId;
    }
};

var plus = {

    matchEvent: ['Plus'],
    aggregate: function (event, player) {
        player.period(event.period).plusminus++;
        if (event.specialtyTeam) {
            player.specialteamperiod(event.specialtyTeam, event.period).plusminus++;
        }
    },
    playerSelector: function (event, player) {
        return event.playerRefId === player.refId;
    }
};

var minus = {

    matchEvent: ['Minus'],
    aggregate: function (event, player) {
        player.period(event.period).plusminus--;
        if (event.specialtyTeam) {
            player.specialteamperiod(event.specialtyTeam, event.period).plusminus--;
        }
    },
    playerSelector: function (event, player) {
        return event.playerRefId === player.refId;
    }
};


var goal = {

    matchEvent: ['Goal'],
    aggregate: function (event, player) {
        var points = player.period(event.period).points;
        points.goal++;
        points.total++;

        if (event.specialtyTeam) {
            var specialTeamPoints = player.specialteamperiod(event.specialtyTeam, event.period).points;
            specialTeamPoints.goal++;
            specialTeamPoints.total++;
        }
    }

};

aggregate.register(goal);
aggregate.register(minus);
aggregate.register(plus);
aggregate.register(assist);