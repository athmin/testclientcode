'use strict';

var aggregate = require('../');

function processEvent (eventName, prop) {

    return {
        matchEvent: [eventName],
        aggregate: function (event, player) {
            var val = player.period(event.period).shots;
            val[prop]++;
            val.total++;

            if (event.specialtyTeam) {
                var specialTeamVal = player.specialteamperiod(event.specialtyTeam, event.period).shots;
                specialTeamVal[prop]++;
                specialTeamVal.total++;
            }
        }
    };
}

aggregate.register(processEvent('Goal', 'goal'));
aggregate.register(processEvent('Save', 'shot'));
aggregate.register(processEvent('Blocked', 'block'));
aggregate.register(processEvent('Miss', 'miss'));
