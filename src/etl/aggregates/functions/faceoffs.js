'use strict';

var aggregate = require('../');

var win = {

    matchEvent: ['W-W', 'W-L'],
    aggregate: function (event, player) {
        var faceoff = player.period(event.period).faceoff;
        faceoff.win++;
        faceoff.total++;
        if (event.specialtyTeam) {
            var specialtyFaceoff = player.specialteamperiod(event.specialtyTeam, event.period).faceoff;
            specialtyFaceoff.win++;
            specialtyFaceoff.total++;
        }
    }
};

var loss = {

    matchEvent: ['L-W', 'L-L'],
    aggregate: function (event,player) {
        var faceoff = player.period(event.period).faceoff;
        faceoff.loss++;
        faceoff.total++;
        if (event.specialtyTeam) {
            var specialtyFaceoff = player.specialteamperiod(event.specialtyTeam, event.period).faceoff;
            specialtyFaceoff.loss++;
            specialtyFaceoff.total++;
        }

    }
};

var tie = {
    matchEvent: ['T-W', 'T-L'],
    aggregate: function (event,player) {
        var faceoff = player.period(event.period).faceoff;
        faceoff.tie++;
        faceoff.total++;
        if (event.specialtyTeam) {
            var specialtyFaceoff = player.specialteamperiod(event.specialtyTeam, event.period).faceoff;
            specialtyFaceoff.tie++;
            specialtyFaceoff.total++;
        }
    }
};

var possession = {

    matchEvent: ['W-W', 'T-W', 'L-W'],
    aggregate: function (event,player) {
        var faceoff = player.period(event.period).faceoff;
        faceoff.possession++;
        if (event.specialtyTeam) {
            var specialtyFaceoff = player.specialteamperiod(event.specialtyTeam, event.period).faceoff;
            specialtyFaceoff.possession++;
        }
    }
};

aggregate.register(win);
aggregate.register(loss);
aggregate.register(tie);
aggregate.register(possession);