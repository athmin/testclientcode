'use strict';

var aggregate = require('../');

var processEvent = {

    matchEvent: function (e) { return e.customFlag === 1; },
    aggregate: function (event, player) {
        var custom = player.period(event.period).custom;
        custom[event.name] = (custom[event.name] || 0) + 1;
        if (event.specialtyTeam) {
            var specialTeamCustom = player.specialteamperiod(event.specialtyTeam, event.period).custom;
            specialTeamCustom[event.name] = (specialTeamCustom[event.name] || 0) + 1;
        }
    }
};

aggregate.register(processEvent);