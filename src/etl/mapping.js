'use strict';

var moment = require('moment');

var mappers = [];

function mapDate(date, time) {

    var parsedDate = moment.unix(parseInt(date) + 978307200);
    var parsedTime = moment.unix(parseInt(time) + 978307200 - 2032);

    return parsedDate.hours(parsedTime.hours()).minutes(parsedTime.minutes()).toDate();

    //return moment(date + ' ' + time).toDate();

}

function mapTeam(row) {

    return {
        name: row.TEAMNAME,
        uuid: row.TEAM_UUID,
        teamSeasonUUID: row.TEAMSEASON_UUID,
        games: []
    };
}

function mapGame(row) {

    return {
        uuid: row.GAME_UUID,
        date: mapDate(row.DATE, row.STARTTIME),
        city: row.CITY,
        rival: row.OPPOSITION_TEAM_NAME,
        type: row.GAMETYPE,
        ozoneLeftFlag: row.OZONE_LEFT_FLAG,
        players: []
    };

}

function mapTeamPlayer(row) {

    return {
        personUUID: row.PERSON_UUID,
        refId: row.PERSON_PK,
        name: row.FIRSTNAME + ' ' + row.LASTNAME,
        jerseynumber: row.JERSEYNUMBER,
        position: row.POSITION_NAME
    };

}

function mapGameEvent(row) {
    return {
        uuid: row.GAMEEVENT_UUID,
        teamSeasonUUID: row.TEAMSEASON_UUID,
        gameUUID: row.GAME_UUID,
        order: row.EVENT_ORDER,
        name: row.EVENT_NAME,
        description: row.DESCRIPTION,
        period: row.PERIOD,
        customFlag: row.EVENT_CUSTOM_FLAG,
        specialtyTeam: row.SPECIALTYTEAM,
        faceOffPosition: {
            dot: row.FACEOFFDOT,
            dotAll: row.FACEOFFDOT_ALL,
            dotTranspose: row.FACEOFFDOT_TRANSPOSE,
            dotTransposeAll: row.FACEOFFDOT_TRANSPOSE_ALL
        },
        position: {
            x: row.X,
            y: row.Y,
            xAll: row.X_ALL,
            yAll: row.Y_ALL,
            xTranspose: row.X_TRANSPOSE,
            yTranspose: row.Y_TRANSPOSE,
            xTransposeAll: row.X_TRANSPOSE_ALL,
            yTransposeAll: row.Y_TRANSPOSE_ALL
        },
        players: [],
        associatedEvents: []
    };
}

function mapGameEventPlayer(row) {
    return {
        name: row.FIRSTNAME + ' ' + row.LASTNAME,
        role: row.PLAYER_ROLE,
        teamSeasonUUID: row.TEAMSEASON_UUID,
        teamUUID: row.TEAM_UUID,
        refId: row.PERSON_PK,
        personUUID: row.PERSON_UUID
    };
}

function mapAssociatedEvent(row) {
    return {
        name: row.EVENT_NAME,
        gameUUID: row.GAME_UUID,
        playerRefId: row.PERSON_PK,
        teamUUID: row.TEAM_UUID,
        period: row.PERIOD,
        players: []
    }
}

var RowMapper = function (mapFn) {
    let self = this;
    self.map = mapFn;
};

mappers.ReportingTeam = new RowMapper(mapTeam);
mappers.ReportingTeamGame = new RowMapper(mapGame);
mappers.ReportingTeamPlayer = new RowMapper(mapTeamPlayer);
mappers.GameEvent = new RowMapper(mapGameEvent);
mappers.GameEventPlayer = new RowMapper(mapGameEventPlayer);
mappers.AssociatedEvent = new RowMapper(mapAssociatedEvent);

module.exports = {
    create: function (model) {
        return mappers[model];
    }
};
