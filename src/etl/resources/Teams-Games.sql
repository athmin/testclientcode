select T.z_pk as TEAM_PK, T.ZUUID as TEAM_UUID, G.Z_PK as GAME_PK, G.ZUUID as GAME_UUID, "Home" as TEAM_ROLE, (select O.ZTEAMNAME from ZTEAM as O where O.Z_PK = G.ZVISITORTEAM) as OPPOSITION_TEAM_NAME, 1 as OZONE_LEFT_FLAG, G.ZSCOREKEEPER as SCOREKEEPER, G.ZTIMEKEEPER as TIMEKEEPER, G.ZDATE as DATE, G.ZSTARTTIME as STARTTIME, G.ZENDTIME as ENDTIME, G.ZARENA as ARENA, G.ZCITY as CITY, G.ZCOMMENTS as COMMENTS, G.ZCOUNTRY as COUNTRY, G.ZDIVISION as DIVISION, G.ZGAMETYPE as GAMETYPE, G.ZLEAGUE as LEAGUE, G.ZLEVEL as LEVEL, G.ZPROVINCE as PROVINCE, G.ZREMARKS as REMARKS, (select ZUUID from ZTEAM where ZPRIMARYTEAM = 1) as TEAMSEASON_UUID
from zteam as T join zgame as G on T.z_pk = G.zhometeam
where 
((select avg(zlocationx) from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam = t.z_pk and ge.zhomeplayer is not null)) < 0.5) 
or
((select avg(zlocationx) from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam <> t.z_pk and ge.zvisitorplayer is not null)) >= 0.5)
union
select T.z_pk as TEAM_PK, T.ZUUID as TEAM_UUID, G.Z_PK as GAME_PK, G.ZUUID as GAME_UUID, "Home" as TEAM_ROLE, (select O.ZTEAMNAME from ZTEAM as O where O.Z_PK = G.ZVISITORTEAM) as OPPOSITION_TEAM_NAME, 0 as OZONE_LEFT_FLAG, G.ZSCOREKEEPER as SCOREKEEPER, G.ZTIMEKEEPER as TIMEKEEPER, G.ZDATE as DATE, G.ZSTARTTIME as STARTTIME, G.ZENDTIME as ENDTIME, G.ZARENA as ARENA, G.ZCITY as CITY, G.ZCOMMENTS as COMMENTS, G.ZCOUNTRY as COUNTRY, G.ZDIVISION as DIVISION, G.ZGAMETYPE as GAMETYPE, G.ZLEAGUE as LEAGUE, G.ZLEVEL as LEVEL, G.ZPROVINCE as PROVINCE, G.ZREMARKS as REMARKS, (select ZUUID from ZTEAM where ZPRIMARYTEAM = 1) as TEAMSEASON_UUID
from zteam as T join zgame as G on T.z_pk = G.zhometeam
where 
((select avg(zlocationx) from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam = t.z_pk and ge.zhomeplayer is not null)) >= 0.5) 
or
((select avg(zlocationx) from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam <> t.z_pk and ge.zvisitorplayer is not null)) < 0.5)
union
select T.z_pk as TEAM_PK, T.ZUUID as TEAM_UUID, G.Z_PK as GAME_PK, G.ZUUID as GAME_UUID, "Visitor" as TEAM_ROLE, (select O.ZTEAMNAME from ZTEAM as O where O.Z_PK = G.ZHOMETEAM) as OPPOSITION_TEAM_NAME, 1 as OZONE_LEFT_FLAG, G.ZSCOREKEEPER as SCOREKEEPER, G.ZTIMEKEEPER as TIMEKEEPER, G.ZDATE as DATE, G.ZSTARTTIME as STARTTIME, G.ZENDTIME as ENDTIME, G.ZARENA as ARENA, G.ZCITY as CITY, G.ZCOMMENTS as COMMENTS, G.ZCOUNTRY as COUNTRY, G.ZDIVISION as DIVISION, G.ZGAMETYPE as GAMETYPE, G.ZLEAGUE as LEAGUE, G.ZLEVEL as LEVEL, G.ZPROVINCE as PROVINCE, G.ZREMARKS as REMARKS, (select ZUUID from ZTEAM where ZPRIMARYTEAM = 1) as TEAMSEASON_UUID
from zteam as T join zgame as G on T.z_pk = G.zvisitorteam
where 
((select avg(zlocationx) from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam = t.z_pk and ge.zvisitorplayer is not null)) < 0.5) 
or
((select avg(zlocationx) from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam <> t.z_pk and ge.zhomeplayer is not null)) >= 0.5)
union
select T.z_pk as TEAM_PK, T.ZUUID as TEAM_UUID, G.Z_PK as GAME_PK, G.ZUUID as GAME_UUID, "Visitor" as TEAM_ROLE, (select O.ZTEAMNAME from ZTEAM as O where O.Z_PK = G.ZHOMETEAM) as OPPOSITION_TEAM_NAME, 0 as OZONE_LEFT_FLAG, G.ZSCOREKEEPER as SCOREKEEPER, G.ZTIMEKEEPER as TIMEKEEPER, G.ZDATE as DATE, G.ZSTARTTIME as STARTTIME, G.ZENDTIME as ENDTIME, G.ZARENA as ARENA, G.ZCITY as CITY, G.ZCOMMENTS as COMMENTS, G.ZCOUNTRY as COUNTRY, G.ZDIVISION as DIVISION, G.ZGAMETYPE as GAMETYPE, G.ZLEAGUE as LEAGUE, G.ZLEVEL as LEVEL, G.ZPROVINCE as PROVINCE, G.ZREMARKS as REMARKS, (select ZUUID from ZTEAM where ZPRIMARYTEAM = 1) as TEAMSEASON_UUID
from zteam as T join zgame as G on T.z_pk = G.zvisitorteam
where 
((select avg(zlocationx) from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam = t.z_pk and ge.zvisitorplayer is not null)) >= 0.5) 
or
((select avg(zlocationx) from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam <> t.z_pk and ge.zhomeplayer is not null)) < 0.5)
union
select T.z_pk as TEAM_PK, T.ZUUID as TEAM_UUID, G.Z_PK as GAME_PK, G.ZUUID as GAME_UUID, "Home" as TEAM_ROLE, (select O.ZTEAMNAME from ZTEAM as O where O.Z_PK = G.ZVISITORTEAM) as OPPOSITION_TEAM_NAME, 1 as OZONE_LEFT_FLAG, G.ZSCOREKEEPER as SCOREKEEPER, G.ZTIMEKEEPER as TIMEKEEPER, G.ZDATE as DATE, G.ZSTARTTIME as STARTTIME, G.ZENDTIME as ENDTIME, G.ZARENA as ARENA, G.ZCITY as CITY, G.ZCOMMENTS as COMMENTS, G.ZCOUNTRY as COUNTRY, G.ZDIVISION as DIVISION, G.ZGAMETYPE as GAMETYPE, G.ZLEAGUE as LEAGUE, G.ZLEVEL as LEVEL, G.ZPROVINCE as PROVINCE, G.ZREMARKS as REMARKS, (select ZUUID from ZTEAM where ZPRIMARYTEAM = 1) as TEAMSEASON_UUID
from zteam as T join zgame as G on T.z_pk = G.zhometeam
where 
not exists(select 1 from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam = t.z_pk and ge.zhomeplayer is not null)) 
and
not exists (select 1 from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam <> t.z_pk and ge.zvisitorplayer is not null))
union
select T.z_pk as TEAM_PK, T.ZUUID as TEAM_UUID, G.Z_PK as GAME_PK, G.ZUUID as GAME_UUID, "Visitor" as TEAM_ROLE, (select O.ZTEAMNAME from ZTEAM as O where O.Z_PK = G.ZHOMETEAM) as OPPOSITION_TEAM_NAME, 0 as OZONE_LEFT_FLAG, G.ZSCOREKEEPER as SCOREKEEPER, G.ZTIMEKEEPER as TIMEKEEPER, G.ZDATE as DATE, G.ZSTARTTIME as STARTTIME, G.ZENDTIME as ENDTIME, G.ZARENA as ARENA, G.ZCITY as CITY, G.ZCOMMENTS as COMMENTS, G.ZCOUNTRY as COUNTRY, G.ZDIVISION as DIVISION, G.ZGAMETYPE as GAMETYPE, G.ZLEAGUE as LEAGUE, G.ZLEVEL as LEVEL, G.ZPROVINCE as PROVINCE, G.ZREMARKS as REMARKS, (select ZUUID from ZTEAM where ZPRIMARYTEAM = 1) as TEAMSEASON_UUID
from zteam as T join zgame as G on T.z_pk = G.zvisitorteam
where 
not exists (select 1 from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam = t.z_pk and ge.zvisitorplayer is not null)) 
and
not exists (select 1 from zgameevent as ge where ge.zgame = g.z_pk and ge.zperiod in (1, 3, 4) and ge.zevent in (select z_pk from zevent where zeventname in ("Goal", "Save", "Miss", "Blocked")) and (ge.zrecordedbyteam <> t.z_pk and ge.zhomeplayer is not null))
order by  TEAM_PK, GAME_PK;