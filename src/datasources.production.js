'use strict';

module.exports = {
    statsguy: {
        name: 'statsguy',
        connector: 'mysql',
        host: process.env.MYSQL_HOST, // 'us-cdbr-iron-east-01.cleardb.net',
        port: process.env.MYSQL_PORT, // 3306,
        database: process.env.MYSQL_DB, //'heroku_95040b092ce30ce',
        username: process.env.MYSQL_USERNAME, //'b24cb549f7c6a6',
        password: process.env.MYSQL_PASSWORD //'72372f15'
    },
    mongodb: {
        name: 'mongodb',
        connector: 'mongodb',
        url: process.env.MONGO_URI
    }
};