var nodemailer = require('nodemailer'),
    smtpTransport = require('nodemailer-smtp-transport'),
    server = require('../server.js');

var mailTransport = nodemailer.createTransport(smtpTransport({
    port: server.get('mailer').port,
    host: server.get('mailer').host,
    secure: server.get('mailer').secure,
    auth: {
        user: server.get('mailer').auth.user,
        pass: server.get('mailer').auth.pass
    }
}));

module.exports = mailTransport;
