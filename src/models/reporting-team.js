'use strict';

var server = require('../server');

module.exports = function (ReportingTeam) {

  ReportingTeam.public = function(callback) {

    var TeamSeason = server.models.TeamSeason;
    var GameEvent = server.models.GameEvent;
    var ReportingGame = server.models.ReportingGame;

    TeamSeason.find({
          where: {
              dbType: 'public'
          }
      }, function (err, teamSeasons) {

          var query = [];

          for (var i = 0; i < teamSeasons.length; i++) {
              query.push({ teamSeasonUUID: teamSeasons[i].uuid });
          }

          // Teams
          ReportingTeam.find({
              where: {
                  or: query
              }
          }, function (err, teams) {
              // Game events
              GameEvent.find({
                  where: {
                      or: query
                  }
              }, function (err, events) {
                  // Games
                  ReportingGame.find({}, function(err, games){
                    var result = {
                      teams: teams,
                      events: events,
                      games: games
                    };
                    callback(null, result);
                  });
              });
          });
      });
    }

    ReportingTeam.remoteMethod(
        'public',
        {
          returns: {arg: 'result', type: 'object', required: true},
        }
    );
};
