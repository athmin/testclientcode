'use strict';

module.exports = function (UserIdentity) {

    /**
     * Map the blob { profile: ... } data brought in from third-party providers
     * into their respective columns
     */
    UserIdentity.beforeSave = function (next, identity) {

        if (identity.profile !== undefined) {
            if (identity.profile.provider === 'google') {
                if (identity.user) {
                    identity.user(function (err, user) {
                        if (err) { return next(err); }

                        user.updateAttributes({
                            firstName: user.firstName || identity.profile.name.givenName,
                            lastName: user.lastName || identity.profile.name.familyName
                        }, function (err, user) {
                            if (err) { return next(err); }

                            return next();
                        });
                    });
                } else {
                    return next();
                }
            }
        }

    };

};
