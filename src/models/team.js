'use strict';

var uuid        = require('node-uuid');

module.exports = function (Team) {

    //Team.validatesUniquenessOf('name');

    /**
     * Generate a UUID for every new team
     */
    Team.beforeSave = function (next, team) {
        team.uuid = uuid.v4();
        next();
    };

};
