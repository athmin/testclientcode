'use strict';

var uuid        = require('node-uuid');

module.exports = function (Season) {

  /**
   * Calculates the difference in months between a date vector. If rolled over,
   * end dates will flip to the next month
   *
   * @param {Date} startDate
   * @param {Date} endDate
   */
  function monthDifference (startDate, endDate) {
    var months;

    months = (endDate.getFullYear() - startDate.getFullYear()) * 12;
    months -= startDate.getMonth();
    months += endDate.getMonth();

    return months <= 0 ? 0 : months;
  }

  /**
   * Applies a discount to a season purchase depending on the season's duration
   * and the purchase date
   *
   * [Notes]
   *
   * Customers will receive discounts based on how far into the
   * season they're buying access
   *
   * Seasons can be 2 months, 4 months, or 8 months in duration.
   * Pro-ration works as follows:
   *
   * full price: 1st Qtr of season (e.g. Sep - Oct inclusive)
   * 3/4 price: 2nd Qtr of season (e.g. Nov - Dec inclusive)
   * 1/2 price: 3rd Qtr of season (e.g. Jan - Feb inclusive)
   * 1/4 price: last Qtr of season (e.g. Mar - Apr inclusive)
   *
   * If a season is only 2 months, a 50% discount will be applied
   * after the first month
   */
  function applyDiscounts (season) {
    var today = new Date();
    var price = parseInt(season.price);

    if (today > season.startDate) {
      var purchaseEndDate = new Date();
      purchaseEndDate.setMonth(season.endDate.getMonth() + 1);
      var duration = monthDifference(season.startDate, purchaseEndDate);
      var monthsLeft = monthDifference(today, purchaseEndDate);

      // 2 Mo. Season
      if (duration === 2) {
        if (monthsLeft === 1) {
          // 1 Month Left, 50% Off
          price = price * 0.5;
        }
      // 4 Mo. Season
      } else if (duration === 4) {
        if (monthsLeft === 1) {
          // 1 Month Left, 75% Off
          price = price * 0.25;
        } else if (monthsLeft === 2) {
          // 2 Months Left, 50% Off
          price = price * 0.5;
        } else if (monthsLeft === 3) {
          // 3 Months Left, 25% Off
          price = price * 0.75;
        }
      // 8 Mo. season
      } else if (duration === 8) {
        if (monthsLeft <= 2) {
          // 2 Month Left, 75% Off
          price = price * 0.25;
        } else if (monthsLeft > 2 && monthsLeft <= 4) {
          // 4 Months Left, 50% Off
          price = price * 0.5;
        } else if (monthsLeft > 4 && monthsLeft <= 6) {
          // 6 Months Left, 25% Off
          price = price * 0.75;
        }
      }
    }

    return price;
  }

  Season.applyDiscounts = applyDiscounts;

  /**
   * Generate a UUID for every new team
   */
  Season.beforeSave = function (next, season) {
    season.uuid = uuid.v4();
    next();
  };

  /**
   * Remote method for returning discounted prices according to today's date
   */
  Season.price = function(seasonId, callback) {
    var response = {};

    Season.findById(seasonId, {}, function (error, result) {
      if (error) return callback(error);

      response.fullPrice = parseInt(result.price);
      response.payable = applyDiscounts(result);
      response.discountedAmount = response.fullPrice - response.payable;
      response.discountedPercentage = (1 - (response.payable / response.fullPrice)) * 100;

      return callback(null, response);
    });
  };

  Season.remoteMethod('price', {
    accepts: [{ arg: 'seasonId', type: 'Number' }],
    returns: { type: 'Object', root: true },
    http: { verb: 'get', status: 200 }
  });

  /**
   * Get the trial expiry date
   */
  Season.trialExpiry = function (seasonId, callback) {
    var response = {};

    Season.findById(seasonId, {}, function (error, result) {
      if (error) return callback(error);

      var trialExpiry = new Date( +new Date() + (1000 * 60 * 60 * 24 * Season.app.get('trialLength')) );
      response.trialExpiry =  trialExpiry;

      return callback(null, response);
    });
  }

  Season.remoteMethod('trialExpiry', {
    accepts: [{ arg: 'seasonId', type: 'Number' }],
    returns: { type: 'Object', root: true },
    http: { verb: 'get', status: 200 }
  });

  /**
   * Get the length (in days) of the trial period
   */
  Season.trialLength = function (callback) {
    var trialLength = Season.app.get('trialLength');

    if (trialLength) {
      return callback(null, { days: trialLength });
    } else {
      return callback(true)
    }
  }

  Season.remoteMethod('trialLength', {
    accepts: [],
    returns: { type: 'Object', root: true },
    http: { verb: 'get', status: 200 }
  });

};
