'use strict';

var _ = require('lodash');

function createAggregatePeriod(period) {
    return {period: period,
        points: {
            goal: 0,
            assists: 0,
            total: 0
        },
        plusminus: 0,
        faceoff: {
            win: 0,
            loss: 0,
            tie: 0,
            total: 0,
            possession: 0
        },
        shots: {
            goal: 0,
            shot: 0,
            block: 0,
            miss: 0,
            total: 0
        },
        custom: {

        }
    };
}

module.exports = function (ReportingTeamPlayer) {

    ReportingTeamPlayer.prototype.period = function (num) {

        var p = _.find(this.periods, {period: num});
        if (! p) {
            p = createAggregatePeriod(num);
            this.periods.push(p);
        }

        return p;

    };

    ReportingTeamPlayer.prototype.specialteamperiod = function (name, period) {

        var st = _.find(this.specialteams, {name: name});
        if (! st) {
            st = {
                name: name,
                periods: [
                    createAggregatePeriod(1),
                    createAggregatePeriod(2),
                    createAggregatePeriod(3),
                    createAggregatePeriod(4)
                ]
            };
            this.specialteams.push(st);
        }

        return _.find(st.periods, { 'period': period });
    };

    ReportingTeamPlayer.public = function(query, callback) {
      ReportingTeamPlayer.find(query, function (err, result) {
          callback(null, result);
      });
    }

    ReportingTeamPlayer.remoteMethod(
        'public', {
          accepts: [
            { arg: 'filter', type: 'object'}
          ],
          returns: {arg: 'result', type: 'object', required: true},
        }
    );

};
