'use strict';

var uuid        = require('node-uuid'),
    mailer      = require('../utils/mailer'),
    server      = require('../server');
    // UserVoice   = require('../utils/uservoice');

module.exports = function (User) {

    /**
     * Generate a UUID for every new user
     */
    User.beforeSave = function (next, user) {
        user.uuid = uuid.v4();
        next();
    };

    /**
     * Notify the mailer recipient that a user has been deleted
     */
    User.beforeDestroy = function (next, user) {
        var email = {
            from: 'StatsGuy <' + server.get('mailer').auth.user + '>',
            to: server.get('mailer').recipient,
            subject: 'User Account Deleted',
            html: '<strong>Account Deleted</strong><br/><p>The account for ' + user.email +
                ' was deleted from the system on ' + new Date().toISOString() + '.</p>'
        };

        user.teamSeasons.destroyAll(function (error, data) {
            if (error) {
                console.error(error);
                return error;
            }

            mailer.sendMail(email, function(error, data){
                if (error) {
                    console.error(error);
                    next(null, error);
                } else {
                    console.info('Email sent: ' + data.response);
                    next();
                }
            });
        });
    };

    /**
     * Fetches a user and generates an SSO token from their unique credentials
     *
     * [Indefinitely Disabled]
     *
     * [Notes]
     *
     * The following ACL must be added to the model configuration in order to
     * make this work:
     *
     * {
     *   "accessType": "EXECUTE",
     *   "principalType": "ROLE",
     *   "principalId": "$authenticated",
     *   "permission": "ALLOW",
     *   "property": "userVoiceSSO"
     * }
     *
     * @param {Integer}     id          A User's ID
     * @param {Function}    callback
     */
    // User.userVoiceSSO = function (id, callback) {
    //     User.findById(id, function (err, user) {
    //         if (err) { return callback(err); }
    //
    //         if (user === null) {
    //             callback(null, null);
    //             return;
    //         }
    //
    //         // Fetch the user identity first
    //         user.userIdentities(function (err, identity) {
    //             if (err) { return callback(err); }
    //
    //             var name = identity[0].profile.displayName;
    //
    //             var sso = new UserVoice(server.get('userVoiceDomain'), server.get('userVoiceKey'));
    //             var token = sso.createToken({
    //                 guid: user.uuid,
    //                 email: user.email,
    //                 display_name: name,
    //                 comment_updates: true
    //             });
    //
    //             callback(null, token);
    //         });
    //     });
    // };

    /**
     * Returns a user's profile info (i.e. identity, roles, etc...)
     */
    User.profile = function (id, callback) {
        User.findById(id, function (err, user) {

            if (err) { return callback(err); }
            var profile = {
                user: user
            };

            user.roles(function (err, roles) {
                if (err) { return callback(err); }
                profile.roles = roles;

                user.userIdentities(function (err, identities) {
                    if (err) { return callback(err); }
                    profile.identities = identities;

                    callback(null, profile);
                });
            });

        });
    };

    /**
     * GET /users/:id/profile
     */
    User.remoteMethod('profile', {
        accepts: [
            { arg: 'id', type: 'string'}
        ],
        returns: {arg: 'profile', type: 'object', required: true},
        http: {path:'/profile', verb: 'get'}
    });

    /**
     * GET /users/:id/uservoice/sso
     *
     * [Indefinitely Disabled]
     */
    // User.remoteMethod('userVoiceSSO', {
    //     accepts: [
    //         { arg: 'id', type: 'string'}
    //     ],
    //     returns: {arg: 'SSO', type: 'string', required: true},
    //     http: {path:'/uservoice/sso', verb: 'get'}
    // });

    /**
     * Normalizes a User from a request object. This should be used when passing
     * a user object to a winston log entry.
     *
     * Since iPad client requests are full of cached relational data, we create
     * a minimal user object from the passed object to store through our Winston
     * transport. This prevents the log message from skipping the DB storage step
     * and eventually timing out
     */
    User.normalize = function (object) {
      var user = {};

      user['id'] = object['id'];
      user['uuid'] = object['uuid'];
      user['fistName'] = object['firstName'];
      user['lastName'] = object['lastName'];
      user['contactNumber'] = object['contactNumber'];
      user['username'] = object['username'];
      user['email'] = object['email'];
      // console.log(user);

      return user;
    }

};
