'use strict';

module.exports = function (Log) {

    Log.clear = function (cb) {
        Log.destroyAll(cb);
    };

    Log.remoteMethod('clear', {});

};