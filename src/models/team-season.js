'use strict';

var uuid   = require('node-uuid'),
    moment = require('moment');

var currentDate = moment.utc().format();

module.exports = function (TeamSeason) {

    /**
     * Generate a UUID and Team-Season name (if a user with that Team-Season doesn't already exist)
     */
    TeamSeason.beforeSave = function (next, teamSeason) {

        TeamSeason.findOne({
            where: {
                seasonId: teamSeason.seasonId,
                teamId: teamSeason.teamId
            }
        }, function (err, result) {
            if (err) { return next(err); }

            if (result === null) {
                teamSeason.team(function (err, team) {
                    teamSeason.season(function (err, season) {

                        teamSeason.uuid = uuid.v4();
                        teamSeason.name = team.name + ' - ' + season.name;
                        teamSeason.dbType = teamSeason.dbType || 'real';
                        teamSeason.creationDate = currentDate;

                        // Set the effective date to today if the season has already started,
                        // otherwise, just set the effective date to the season's end date
                        teamSeason.effectiveDate = season.startDate > new Date() ? season.startDate : new Date();

                        if (teamSeason.dbType === 'demo') {
                          var trialExpiry = new Date( +new Date() + (1000 * 60 * 60 * 24 * TeamSeason.app.get('trialLength')) );
                          teamSeason.expiryDate =  trialExpiry;
                          teamSeason.effectiveDate = new Date();
                        } else {
                          teamSeason.expiryDate = season.endDate;
                        }

                        teamSeason.price = TeamSeason.app.models.Season.applyDiscounts(season);

                        TeamSeason.app.models.App.findById(season.appId, function (err, app) {
                          if (err) return next(err);

                          teamSeason.appTag = app.appTag;
                          return next();
                        });
                    });
                });
            } else {
                return next(new Error('That Team Season already exists!'));
            }

        });
    };

    TeamSeason.public = function(callback) {
      TeamSeason.find({
        where: {
            dbType: 'public'
        }
      }, function (err, result) {
        callback(null, result);
      });
    };

    TeamSeason.remoteMethod('public', {
      returns: {
        arg: 'teamSeasons',
        type: 'object',
        required: true
      }
    });

};
