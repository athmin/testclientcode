// Karma configuration
// Generated on Mon Oct 27 2014 13:39:28 GMT-0600 (MDT)

module.exports = function(config) {
  config.set({
    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    plugins: [
        "karma-chrome-launcher",
        "karma-jasmine",
        "karma-phantomjs-launcher",
        "karma-spec-reporter"
    ],

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
        'src/public/bower_components/ng-file-upload/angular-file-upload-shim.js',
        'src/public/bower_components/jquery/dist/jquery.js',
        'src/public/bower_components/lodash/dist/lodash.js',
        'src/public/bower_components/angular/angular.js',
        'src/public/bower_components/angular-*/angular-*.js',
        'src/public/bower_components/angular-bootstrap/ui-bootstrap.js',
        'src/public/bower_components/angular-ui-router/release/angular-ui-router.js',
        'src/public/bower_components/angular-ui-select/dist/select.js',
        'src/public/bower_components/ng-file-upload/angular-file-upload.js',
        'src/public/bower_components/angular-sanitize/angular-sanitize.js',
        'src/public/bower_components/ng-csv/build/ng-csv.js',
        'node_modules/socket.io/node_modules/socket.io-client/socket.io.js',
        // 'src/public/bower_components/angular-socket-io/socket.js',
        'src/public/js/**/*.js',
        'test/client/**/*.js'
    ],

    // list of files to exclude
    exclude: [
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['spec'],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
